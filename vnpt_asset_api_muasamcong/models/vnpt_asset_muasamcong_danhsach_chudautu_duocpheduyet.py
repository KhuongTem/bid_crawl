# -*- coding:utf-8 -*-
_author_ = 'VNPT.IT-ERP'

# -*- coding:utf-8 -*-
import os

import requests
import json
from odoo import models, fields, api

import xml.etree.ElementTree as ET

from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

from odoo.tools import OrderedSet, pycompat


class VnptAssetMuaSamCongListChuDauTuDuocPheDuyet(models.Model):
    _name = 'vnpt_asset_muasamcong_danhsach_chudautu_duocpheduyet'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tendaydu'
    _description = 'Lấy danh sách chủ đầu tư được phê duyệt'

    # nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin chung
    tendaydu = fields.Char(string="Tên đầy đủ", required=False, )
    tentienganh = fields.Char(string="Tên tiếng anh", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    loaihinhphaply = fields.Char(string="Loại hình pháp lý", required=False, )
    masothue = fields.Char(string="Mã số thuế", required=False, )
    ngaycap_chudautu = fields.Date(string="Ngày cấp", required=False, )
    quocgiacap_chudautu = fields.Char(string="Quốc gia cấp", required=False, )

    #Tình trạng hoạt động
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )
    trangthai_vaitro = fields.Char(string="Trạng thái vai trò", required=False, )

    #Cơ quan chủ quản
    tencoquan = fields.Char(string="Tên cơ quan chủ quản", required=False, )
    maquanhe = fields.Char(string="Mã quan hệ ngân sách", required=False, )

    #Địa chỉ trụ sở
    tinh = fields.Char(string="Tỉnh/thành phố", required=False, )
    diachi = fields.Char(string="Địa chỉ", required=False, )
    sdt = fields.Char(string="Số điện thoại", required=False, )
    web = fields.Char(string="Web", required=False, )

    #Người đại diện pháp luật
    hovaten = fields.Char(string="Họ và tên người đại diện pháp luật", required=False, )
    chucvu = fields.Char(string="Chức vụ người đại diện pháp luật", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )
