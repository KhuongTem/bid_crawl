# -*- coding:utf-8 -*-
import json

import requests
from selenium.webdriver import Keys

from odoo.tools import float_compare

__author__ = 'VNPT-IT'

import os
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError, Warning
import urllib3
import random
import string
from datetime import datetime
from datetime import timezone
import dateutil.parser

import asyncio

import time
import pathlib
import json

from datetime import datetime, timedelta
from datetime import timezone

from pdf2docx import Converter

import requests
import random
import string

# import nest_asyncio
# nest_asyncio.apply()

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from chromedriver_py import binary_path
# from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException, ElementClickInterceptedException, \
    StaleElementReferenceException
from urllib.parse import urlparse, parse_qs

import logging
from telegram import Bot
import telegram.ext
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackContext

from odoo import tools
import psutil

class vnpt_muasamcong_sync(models.Model):
    _name = 'vnpt_muasamcong_sync'
    _inherit = ['mail.thread']
    _rec_name = 'name'
    _description = u'Đồng bộ dữ liệu mua sắm công'

    name = fields.Char(string=u"Tiêu đề", required=True, )
    notes = fields.Text(string=u"Ghi chú", required=False, )

    token = fields.Text(string=u"Mã token", required=False, )

    page_size_nhathau = fields.Integer(string=u"Số bản ghi nhà thầu trên 1 trang", required=False, )
    page_number_nhathau = fields.Integer(string=u"Số trang bắt đầu nhà thầu", required=False, )

    state = fields.Selection(string=u"Trạng thái", selection=[('draft', u'Dự thảo'), ('approved', u'Xác nhận'), ],
                             required=False, default='draft')

    date_start = fields.Date(string="Ngày bắt đầu", required=False, )
    date_end = fields.Date(string="Ngày kết thúc", required=False, )

    def downloadPdfMuaSamCong(self, url):
        options = webdriver.ChromeOptions();
        options.headless = True;
        # local
        prefs = {"download.default_directory": tools.config['crawl_bid_prefs_data']};
        # prefs = {"download.default_directory": "C:\Program Files\BID_CRAWL\odoo-15.0-crawl\data"};
        # prefs = {"download.default_directory": "/home/vnpt/odoo/odoo15/source/vnpt_asset_api_muasamcong/data"};
        options.add_experimental_option("prefs", prefs);
        chromedriver = "chromedriver.exe"
        # chromedriver = "/usr/local/bin/chromedriver"
        service = Service(chromedriver)
        driver = webdriver.Chrome(service=service, options=options)
        driver.get(url);
        driver.implicitly_wait(60)
        downloadcsv = driver.find_element(By.CSS_SELECTOR, '.btn-primary');
        downloadcsv.click();
        time.sleep(60)
        driver.close()

        # options = webdriver.ChromeOptions();
        #
        # options.add_argument("--headless")  # or use pyvirtualdiplay
        # options.add_argument("--no-sandbox")  # needed, because colab runs as root
        #
        # chromedriver = "/home/vnpt/odoo/chromedriver"
        # service = Service(chromedriver)
        # options.headless = True
        #
        # driver = webdriver.Chrome(service=service, options=options)
        #
        # driver.get("https://github.com/kaliiiiiiiiii/Selenium-Profiles")
        # print(driver.title)
        # driver.quit()

    def get_random_string(self, length):
        letters = string.ascii_letters
        result_str = ''.join(random.choice(letters) for i in range(length))

        return result_str

    def convertEffRoleDate(self, date):
        if (date and len(date) > 0):
            year = str(date[0])
            month = str(date[1]) if date[1] > 9 else '0' + str(date[1])
            day = str(date[2]) if date[2] > 9 else '0' + str(date[2])
            return year + '-' + month + '-' + day
            # return str(date[0]) + '-' + str(date[1]) if date[1] > 9 else '0'+str(date[1]) + '-' + str(date[2]) if date[2] > 9 else '0'+str(date[2])
        return '';

    def convertDateMuaSamCong(self, date):
        if date is not None:
            return dateutil.parser.parse(date).strftime('%Y-%m-%d')
        return None;

    def convertDateNgayCapChuDauTu(self, date):
        if date is not None:
            if isinstance(date, int) == True:
                date_timestamp_in_secs = date / 1000
                if isinstance(date_timestamp_in_secs, int) == True:
                    return datetime.fromtimestamp(int(date_timestamp_in_secs)).strftime('%Y-%m-%d')
                return None;
            return None;
        return None;

    def converStatusChuDauTu(self, status):
        if status == 1:
            textStatus = 'Đang hoạt động'
        elif status == 2:
            textStatus = 'Tạm ngừng'
        else:
            textStatus = 'Chấm dứt'
        return textStatus

    def getNameStatus(self, statusForNotify, bidCloseDate, isInternet):
        bidCloseDate_format = datetime.fromisoformat(bidCloseDate).astimezone(timezone.utc)
        now = datetime.now()
        now_format = datetime.fromisoformat(str(now)).astimezone(timezone.utc)
        if (statusForNotify == 'DHTBMT'):
            return "Đã hủy TBMT"
        if (statusForNotify == 'KCNTTT'):
            return "Không có nhà thầu trúng thầu"
        if (statusForNotify == 'CNTTT'):
            return "Có nhà thầu trúng thầu"
        if (statusForNotify == 'DHT'):
            return "Đã huỷ thầu"
        if (statusForNotify == 'DXT'):
            return "Đang xét thầu"
        if (statusForNotify == ""):
            if bidCloseDate and bidCloseDate_format > now_format:
                return "Chưa đóng thầu"
            if bidCloseDate and bidCloseDate_format <= now_format:
                if (isInternet == 0):
                    return "Đang xét thầu"
                else:
                    return "Chưa mở thầu"

    # async def downloadPdfMuaSamCong(self, url):
    #     browser = await launch(headless = True)
    #     page = await browser.newPage()
    #
    #     await page.goto(url)
    #     await page.waitForSelector('.btn-primary')
    #     await page._client.send('Page.setDownloadBehavior', {
    #             'behavior': 'allow',
    #             'downloadPath': 'D:\VNPT\BID_ODOO\odoo-15.0-crawl\data'
    #     })
    #
    #     await page.click(".btn-primary")
    #     time.sleep(10)

    # async def downloadPdfMuaSamCong(self, url):
    #     browser =  launch(headless = True)
    #     page =  await browser.newPage()
    #
    #     await page.goto(url)
    #     await page.waitForSelector('.btn-primary')
    #     await page._client.send('Page.setDownloadBehavior', {
    #             'behavior': 'allow',
    #             'downloadPath': 'D:\VNPT\BID_ODOO\odoo-15.0-crawl\data'
    #     })
    #
    #     await page.click(".btn-primary")
    #     time.sleep(10)

    def btn_syns_nhathau_duocpheduyet(self):

        self = self.sudo()
        _api_link = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-list'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        _count_write = 0
        _count_create = 0
        ls_data_nhathau = []
        ls_data_thanhvien_gopvon = []
        ls_data_nganhnghe_kinhdoanh = []

        # Update page_index về 0 nếu vượt quá limit page cron job
        self.env.cr.execute("""select * from fn_get_cauhinh_crawl(%s)""", ('btn_syns_nhathau_duocpheduyet',))
        result = self.env.cr.dictfetchall()
        cauhinh = result[0]['json_cauhinh_crawl'][0]
        page_index_next = cauhinh['page_index'] + cauhinh['step'] + 1
        for pageNumber in range(cauhinh['page_index'], cauhinh['page_index'] + cauhinh['step']):
            params = {
                "pageSize": cauhinh['page_size'],
                "pageNumber": pageNumber,
                "queryParams": {
                    "officePro": {
                        "contains": ""
                    },
                    "effRoleDate": {
                        "greaterThanOrEqual": "",
                        "lessThanOrEqual": ""
                    },
                    "isForeignInvestor": {
                        "equals": ""
                    },
                    "roleType": {
                        "equals": "NT"
                    },
                    "decNo": {
                        "contains": ""
                    },
                    "orgName": {
                        "contains": ""
                    },
                    "taxCode": {
                        "contains": ""
                    },
                    "orgNameOrOrgCode": {
                        "contains": ""
                    },
                    "agencyName": {
                        "in": ""
                    }
                }
            }

            r = requests.post(_api_link, json=params, verify=False, headers=headers)
            jsondata = r.json()

            if jsondata:
                datas = jsondata['content']
                for _item in datas:
                    params_orgCode = {"orgCode": _item['orgCode']}
                    # lấy chi tiết nhà thầu
                    _api_get_detail_approve_bidder = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-detail-approve-bidder'
                    r2 = requests.post(_api_get_detail_approve_bidder, json=params_orgCode, headers=headers,
                                       verify=False, timeout=3000)
                    detail_approve_bidder = r2.json()

                    # chi tiết khác
                    urlLoadOrgInforSPM = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-org-infor-spm'
                    params_LoadOrgInforSPM = {"body": _item['orgCode']}
                    # params_LoadOrgInforSPM = {"body": 'vn8053651835'}
                    request_orgInforSPM = requests.post(urlLoadOrgInforSPM, json=params_LoadOrgInforSPM,
                                                        headers=headers, verify=False,
                                                        timeout=3000)
                    data_org_info_spm = []
                    if len(json.loads(request_orgInforSPM.content)) > 0:
                        a_list = json.loads(request_orgInforSPM.content)

                        filtered_list = [
                            dictionary for dictionary in a_list
                            if dictionary['status'] == "PUBLICIZED"
                        ]

                        data_org_info_spm = filtered_list[len(filtered_list) - 1]

                    # lấy chi tiết địa chỉ
                    param_tinh = {
                        "queryParams": {
                            "code": {
                                "equals": detail_approve_bidder['officePro']
                            }
                        }
                    }
                    _api_get_area_by_code = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-area-by-code'
                    request_get_tinh = requests.post(_api_get_area_by_code, json=param_tinh, headers=headers,
                                                     verify=False,
                                                     timeout=3000)
                    if len(json.loads(request_get_tinh.content)) > 0:
                        detail_tinh = json.loads(request_get_tinh.content)[0]
                    else:
                        detail_tinh['name'] = None

                    param_quan = {
                        "queryParams": {
                            "code": {
                                "equals": detail_approve_bidder['officeDis']
                            }
                        }
                    }
                    _api_get_area_by_code = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-area-by-code'
                    request_get_quan = requests.post(_api_get_area_by_code, json=param_quan, headers=headers,
                                                     verify=False,
                                                     timeout=3000)
                    # detail_quan = json.loads(request_get_quan.content)[0]
                    if len(json.loads(request_get_quan.content)) > 0:
                        detail_quan = json.loads(request_get_quan.content)[0]
                    else:
                        detail_quan['name'] = None

                    param_xa = {
                        "queryParams": {
                            "code": {
                                "equals": detail_approve_bidder['officeWar']
                            }
                        }
                    }
                    _api_get_area_by_code = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-area-by-code'
                    request_get_xa = requests.post(_api_get_area_by_code, json=param_xa, headers=headers, verify=False,
                                                   timeout=3000)
                    # detail_xa = json.loads(request_get_xa.content)[0]
                    if len(json.loads(request_get_xa.content)) > 0:
                        detail_xa = json.loads(request_get_xa.content)[0]
                    else:
                        detail_xa['name'] = None

                    _var_data_nhathau = {
                        'tendaydu': detail_approve_bidder['orgFullName'],
                        'tentienganh': detail_approve_bidder['orgEnName'],
                        'madinhdanh': detail_approve_bidder['orgCode'],
                        'ngaypheduyet': self.convertEffRoleDate(_item['effRoleDate']),
                        'loaihinhphaply': detail_approve_bidder['businessType'],
                        'masothue': detail_approve_bidder['taxCode'],
                        'ngaycap_nhathau': self.convertDateMuaSamCong(detail_approve_bidder['taxDate']),
                        'quocgiacap_nhathau': detail_approve_bidder['taxNation'],

                        # Quyết định thành lập
                        'ngaythanhlap': self.convertDateMuaSamCong(
                            data_org_info_spm['orgDecisionDate']) if 'orgDecisionDate' in data_org_info_spm else None,
                        'coquanbanhanh': data_org_info_spm[
                            'orgDecisionAgency'] if 'orgDecisionAgency' in data_org_info_spm else None,
                        'quocgiabanhanh': data_org_info_spm[
                            'orgDecisionNation'] if 'orgDecisionNation' in data_org_info_spm else None,
                        'quyetdinhthanhlap': data_org_info_spm[
                            'orgDecisionFileName'] if 'orgDecisionFileName' in data_org_info_spm else None,

                        # Địa chỉ trụ sở
                        'tinh': detail_tinh['name'],
                        'quan': detail_quan['name'],
                        'xa': detail_xa['name'],
                        'sonha': detail_approve_bidder['officeAdd'],
                        'web': detail_approve_bidder['officeWeb'],

                        # Người đại diện pháp luật
                        'hovaten': detail_approve_bidder['repName'],
                        'chucvu': detail_approve_bidder['repPosition'],

                        # Đăng ký kinh doanh
                        'sodangky': data_org_info_spm['orgDetail'][
                            'taxCode'] if 'orgDetail' in data_org_info_spm else None,
                        'ngaycap_dangky_kinhdoanh': self.convertDateMuaSamCong(data_org_info_spm['orgDetail'][
                                                                                   'businessDate']) if 'orgDetail' in data_org_info_spm else None,
                        'quocgiacap_dangky_kinhdoanh': data_org_info_spm['orgDetail'][
                            'businessNation'] if 'orgDetail' in data_org_info_spm else None,
                        'giayphepkinhdoanh': data_org_info_spm['orgDetail'][
                            'businessFileName'] if 'orgDetail' in data_org_info_spm else None,

                        # Thông tin hoạt động
                        'dieulehoatdong': data_org_info_spm['orgDetail'][
                            'compCharterFileName'] if 'orgDetail' in data_org_info_spm else None,
                        'sodotochuc': data_org_info_spm['orgDetail'][
                            'orgChartFileName'] if 'orgDetail' in data_org_info_spm else None,
                        'sonhanvien': data_org_info_spm['empCount'] if 'empCount' in data_org_info_spm else None,
                        'linhvucthamgia': data_org_info_spm['bidFields'][
                            'lcnt'] if 'bidFields' in data_org_info_spm else None,
                        'quymodoanhnghiep': data_org_info_spm['orgScale'] if 'orgScale' in data_org_info_spm else None
                    }
                    ls_data_nhathau.append(_var_data_nhathau)

                    # Thành viên góp vốn
                    if 'shareholderList' in data_org_info_spm and len(data_org_info_spm['shareholderList']) > 0:
                        for _item_gopvon in data_org_info_spm['shareholderList']:
                            _var_data_thanhvien_gopvon = {
                                'madinhdanh_nhathau': detail_approve_bidder['orgCode'],
                                'hovaten': _item_gopvon['name'],
                                'sogiaychungthuc': _item_gopvon['idNo'],
                                'ngaycap': _item_gopvon['issuedDate'],
                                'noicap': _item_gopvon['issuedBy'],
                                'diachithuongtru': _item_gopvon['address'],
                                'quoctich': _item_gopvon['nationality'],
                                'tile': _item_gopvon['percentage']
                            }
                            ls_data_thanhvien_gopvon.append(_var_data_thanhvien_gopvon)

                    # Ngành nghề kinh doanh
                    if len(detail_approve_bidder['businesses']) > 0:
                        for _item_nganhnghe_kinhdoanh in detail_approve_bidder['businesses']:
                            _var_data_nganhnghe_kinhdoanh = {
                                'madinhdanh_nhathau': detail_approve_bidder['orgCode'],
                                'manganh': _item_nganhnghe_kinhdoanh['code'],
                                'tennganh': _item_nganhnghe_kinhdoanh['name'],
                                'nganhchinh': _item_nganhnghe_kinhdoanh['main'],
                            }
                            ls_data_nganhnghe_kinhdoanh.append(_var_data_nganhnghe_kinhdoanh)

        self.env.cr.execute(
            """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_nhathau_duocpheduyet' """,
            (page_index_next, len(ls_data_nhathau) + cauhinh['quantity']))

        if len(ls_data_nhathau) > 0:
            b_json_data = json.dumps(ls_data_nhathau)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_sync_nhathau_duocpheduyet(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        if len(ls_data_thanhvien_gopvon) > 0:
            b_json_data_thanhvien_gopvon = json.dumps(ls_data_thanhvien_gopvon)
            string_sql_insert_json_thanhvien_gopvon = '''SELECT * FROM fn_vnpt_asset_hrm_sync_thanhvien_gopvon(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json_thanhvien_gopvon,
                                (b_json_data_thanhvien_gopvon, self._uid, b_random_code))
            self.env.cr.commit()

        if len(ls_data_nganhnghe_kinhdoanh) > 0:
            b_json_data_nganhnghe_kinhdoanh = json.dumps(ls_data_nganhnghe_kinhdoanh)
            string_sql_insert_json_nganhnghe_kinhdoanh = '''SELECT * FROM fn_vnpt_asset_hrm_sync_nganhnghe_kinhdoanh(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json_nganhnghe_kinhdoanh,
                                (b_json_data_nganhnghe_kinhdoanh, self._uid, b_random_code))
            self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi nhà thầu được phê duyệt trả về: %s" % (
                              'Nhà thầu được phê duyệt', len(ls_data_nhathau)))

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi ngành nghề kinh doanh trả về: %s" % (
                              'Ngành nghề kinh doanh', len(ls_data_nganhnghe_kinhdoanh)))

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi thành viên góp vốn trả về: %s" % (
                              'Thành viên góp vốn', len(ls_data_thanhvien_gopvon)))

    def btn_syns_chudautu_duocpheduyet(self):

        self = self.sudo()
        _api_link = 'https://muasamcong.mpi.gov.vn/o/egp-portal-investor-approved-v2/services/um/lookup-orgInfo'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        ls_data_chudautu = []

        # Update page_index về 0 nếu vượt quá limit page cron job
        self.env.cr.execute("""select * from fn_get_cauhinh_crawl(%s)""", ('btn_syns_chudautu_duocpheduyet',))
        result = self.env.cr.dictfetchall()
        cauhinh = result[0]['json_cauhinh_crawl'][0]
        page_index_next = cauhinh['page_index'] + cauhinh['step'] + 1
        for pageNumber in range(cauhinh['page_index'], cauhinh['page_index'] + cauhinh['step']):
            params = {
                "pageSize": cauhinh['page_size'],
                "pageNumber": pageNumber,
                "queryParams": {
                    "roleType": {
                        "equals": "CDT"
                    },
                    "orgName": {
                        "contains": ""
                    },
                    "orgCode": {
                        "contains": ""
                    },
                    "orgNameOrOrgCode": {
                        "contains": ""
                    },
                    "agencyName": {
                        "contains": ""
                    },
                    "effRoleDate": {
                        "greaterThanOrEqual": "",
                        "lessThanOrEqual": ""
                    }
                }
            }

            r = requests.post(_api_link, json=params, verify=False, headers=headers, timeout=15000)
            jsondata = r.json()

            if jsondata:
                datas = jsondata['ebidOrgInfos']['content']
                for _item in datas:

                    params_orgCode = {"orgCode": _item['orgCode']}
                    # lấy chi tiết chủ đầu tư
                    _api_get_detail_investor_approved = 'https://muasamcong.mpi.gov.vn/o/egp-portal-investor-approved-v2/services/um/org/get-detail-info'
                    r2 = requests.post(_api_get_detail_investor_approved, json=params_orgCode, headers=headers,
                                       verify=False,
                                       timeout=3000)
                    detail_investor_approved = r2.json()['orgInfo']

                    # lấy chi tiết địa chỉ
                    param_tinh = {
                        "queryParams": {
                            "code": {
                                "equals": _item['officePro']
                            }
                        }
                    }
                    _api_get_area_by_code = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractors-approved/services/get-area-by-code'
                    request_get_tinh = requests.post(_api_get_area_by_code, json=param_tinh, headers=headers,
                                                     verify=False,
                                                     timeout=3000)
                    # detail_tinh = json.loads(request_get_tinh.content)[0]
                    if len(json.loads(request_get_tinh.content)) > 0:
                        detail_tinh = json.loads(request_get_tinh.content)[0]
                    else:
                        detail_tinh['name'] = None

                    # lấy chi tiết tên cơ quan chủ quản
                    param_chuquan = {
                        "queryParams": {
                            "category_type_code": {
                                "equals": "classification_dependent"
                            },
                            "code": {
                                "equals": detail_investor_approved['agencyName']
                            }
                        }
                    }
                    # _api_get_detail_chuquan = 'https://muasamcong.mpi.gov.vn/o/egp-portal-investor-approved-v2/services/cat/categories-by-cat-type'
                    # request_get_detail_chuquan = requests.post(_api_get_detail_chuquan, json=param_chuquan, headers=headers, verify=False,
                    #                                  timeout=3000)
                    # detail_chuquan = json.loads(request_get_detail_chuquan.content)[0]

                    _var_data_chudautu = {
                        # Thông tin thành lập
                        'tendaydu': _item['orgFullname'],
                        'tentienganh': _item['orgEnName'],
                        'madinhdanh': _item['orgCode'],
                        'loaihinhphaply': _item['businessType'],
                        'masothue': _item['taxCode'],
                        'ngaycap_chudautu': self.convertDateNgayCapChuDauTu(detail_investor_approved['taxDate']),
                        'quocgiacap_chudautu': _item['taxNation'],

                        # Tình trạng hoạt động
                        'ngaypheduyet': self.convertEffRoleDate(_item['effRoleDate']),
                        'trangthai_vaitro': self.converStatusChuDauTu(int(_item['status'])),

                        # Cơ quan chủ quản

                        # 'tencoquan' : detail_chuquan['name'],
                        'tencoquan': "",
                        'maquanhe': detail_investor_approved['budgetCode'],

                        # Địa chỉ trụ sở
                        'tinh': detail_tinh['name'],
                        'diachi': _item['officeAdd'],
                        'sdt': _item['officePhone'],
                        'web': _item['officeWeb'],

                        # Người đại diện pháp luật
                        'hovaten': detail_investor_approved['repName'],
                        'chucvu': detail_investor_approved['repPosition'],
                    }
                    ls_data_chudautu.append(_var_data_chudautu)
        self.env.cr.execute(
            """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_chudautu_duocpheduyet' """,
            (page_index_next, len(ls_data_chudautu) + cauhinh['quantity']))
        if len(ls_data_chudautu) > 0:
            b_json_data = json.dumps(ls_data_chudautu)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_chudautu_duocpheduyet(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi chủ đầu tư được phê duyệt trả về: %s" % (
                              'Chủ đầu tư được phê duyệt', len(ls_data_chudautu)))

    # Bên mời thầu
    def btn_syns_benmoithau(self):

        self = self.sudo()
        _api_link = "https://muasamcong.mpi.gov.vn/o/egp-portal-bid-solicitor-approved/services/um/lookup-orgInfo"
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        ls_data_benmoithau = []

        # Update page_index về 0 nếu vượt quá limit page cron job
        self.env.cr.execute("""select * from fn_get_cauhinh_crawl(%s)""", ('btn_syns_benmoithau',))
        result = self.env.cr.dictfetchall()
        cauhinh = result[0]['json_cauhinh_crawl'][0]
        page_index_next = cauhinh['page_index'] + cauhinh['step'] + 1
        for pageNumber in range(cauhinh['page_index'], cauhinh['page_index'] + cauhinh['step']):
            params = {
                "pageSize": cauhinh['page_size'],
                "pageNumber": pageNumber,
                "queryParams": {
                    "roleType": {
                        "equals": "BMT"
                    },
                    "orgName": {
                        "contains": ""
                    },
                    "orgCode": {
                        "contains": ""
                    },
                    "orgNameOrOrgCode": {
                        "contains": ""
                    },
                    "agencyName": {
                        "contains": ""
                    },
                    "effRoleDate": {
                        "greaterThanOrEqual": "",
                        "lessThanOrEqual": ""
                    }
                }
            }

            r = requests.post(_api_link, json=params, verify=False, headers=headers)
            jsondata = r.json()

            if jsondata:
                datas = jsondata['ebidOrgInfos']['content']
                for _item in datas:

                    params_orgCode = {"orgCode": _item['orgCode']}
                    # lấy chi tiết bên mời thầu
                    _api_get_detail_investor_approved = 'https://muasamcong.mpi.gov.vn/o/egp-portal-bid-solicitor-approved/services/um/org/get-detail-info'
                    r2 = requests.post(_api_get_detail_investor_approved, json=params_orgCode, headers=headers,
                                       verify=False,
                                       timeout=3000)
                    if len(r2.json()) > 0:
                        detail_investor_approved = r2.json()['orgInfo']

                        # lấy chi tiết địa chỉ
                        param_tinh = {
                            "queryParams": {
                                "code": {
                                    "equals": _item['officePro']
                                }
                            }
                        }
                        _api_get_area_by_code = 'https://muasamcong.mpi.gov.vn/o/egp-portal-bid-solicitor-approved/services/get-area-by-code'
                        request_get_tinh = requests.post(_api_get_area_by_code, json=param_tinh, headers=headers,
                                                         verify=False,
                                                         timeout=3000)
                        # detail_tinh = json.loads(request_get_tinh.content)[0]
                        if len(json.loads(request_get_tinh.content)) > 0:
                            detail_tinh = json.loads(request_get_tinh.content)[0]
                        else:
                            detail_tinh['name'] = None

                        # lấy chi tiết tên cơ quan chủ quản
                        # param_chuquan = {
                        #   "queryParams": {
                        #     "category_type_code": {
                        #       "equals": "classification_dependent"
                        #     },
                        #     "code": {
                        #       "equals": detail_investor_approved['agencyName']
                        #     }
                        #   }
                        # }
                        # _api_get_detail_chuquan = 'https://muasamcong.mpi.gov.vn/o/egp-portal-bid-solicitor-approved/services/cat/categories-by-cat-type'
                        # request_get_detail_chuquan = requests.post(_api_get_detail_chuquan, json=param_chuquan, headers=headers, verify=False,
                        #                                  timeout=3000)
                        # detail_chuquan = json.loads(request_get_detail_chuquan.content)[0]

                        _var_data_benmoithau = {
                            # Thông tin thành lập
                            'tendaydu': _item['orgFullname'],
                            'tentienganh': _item['orgEnName'],
                            'madinhdanh': _item['orgCode'],
                            'loaihinhphaply': _item['businessType'],
                            'masothue': _item['taxCode'],
                            'ngaycap': self.convertDateNgayCapChuDauTu(detail_investor_approved['taxDate']) if
                            detail_investor_approved['taxDate'] is not None else None,
                            'quocgiacap': _item['taxNation'],

                            # Tình trạng hoạt động
                            'ngaypheduyet': self.convertEffRoleDate(_item['effRoleDate']),
                            'trangthaivaitro': self.converStatusChuDauTu(int(_item['status'])),

                            # Cơ quan chủ quản
                            # 'tencoquan' : detail_chuquan['name'],
                            'tencoquan': "",
                            'maquanhe': detail_investor_approved['budgetCode'],

                            # Địa chỉ trụ sở
                            'tinh': detail_tinh['name'],
                            'diachi': _item['officeAdd'],
                            'sdt': _item['officePhone'],
                            'web': _item['officeWeb'],

                            # Người đại diện pháp luật
                            'hovaten': detail_investor_approved['repName'],
                            'chucvu': detail_investor_approved['repPosition']
                        }
                        ls_data_benmoithau.append(_var_data_benmoithau)
        self.env.cr.execute(
            """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_benmoithau' """,
            (page_index_next, len(ls_data_benmoithau) + cauhinh['quantity']))
        if len(ls_data_benmoithau) > 0:
            b_json_data = json.dumps(ls_data_benmoithau)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_benmoithau(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi bên mời thầu trả về: %s" % (
                              'Bên mời thầu', len(ls_data_benmoithau)))

    # Gói thầu

    def getNameCatByCodeCat(self, code, listCat):
        if not listCat:
            return code;
        # temp = listCat.filter(el= > el.code == code).pop();
        # return temp ? temp.name: code;

    def getNameOfPeriod(self, code):
        if code == 'D':
            return 'ngày';
        if code == 'T':
            return 'tháng';
        if code == 'M':
            return 'tháng';
        if code == 'Q':
            return 'quý';
        if code == 'N':
            return 'năm';
        if code == 'Y':
            return 'năm';
        return '';

    # Gói thầu
    def btn_syns_goithau(self):

        self = self.sudo()
        _api_link = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/smart/search'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        _ls_goithau = []
        _ls_nhathau_thamgia = []
        # list nhà thầu trúng thầu
        _ls_ketqua_dauthau = []
        _ls_sanpham = []
        url_data = tools.config['crawl_bid_url_data']

        # get danh sách từ khóa
        self.env.cr.execute("""select code,name from bid_muasamcong_danhsach_tukhoa_cntt;""")
        result = self.env.cr.dictfetchall()
        for pageNumber in range(0, len(result) - 1):
            params = [
                {
                    "pageSize": 10000,
                    "pageNumber": 0,
                    "query": [
                        {
                            "index": "es-contractor-selection",
                            "keyWord": result[pageNumber]['name'],
                            "matchType": "all-1",
                            "matchFields": [
                                "notifyNo",
                                "bidName"
                            ],
                            "filters": [
                                {
                                    "fieldName": "type",
                                    "searchType": "in",
                                    "fieldValues": [
                                        "es-notify-contractor"
                                    ]
                                },
                                {
                                    "fieldName": "caseKHKQ",
                                    "searchType": "not_in",
                                    "fieldValues": [
                                        "1"
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]

            r = requests.post(_api_link, json=params, verify=False, headers=headers)
            jsondata = r.json()

            if jsondata['page']:
                datas = jsondata['page']['content']
                for _item in datas:
                    url_download = ''
                    params_detail = {"id": _item['id']}
                    _api_get_detail_bid = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/expose/lcnt/bid-notify-contractor-out/get-by-id'
                    r2 = requests.post(_api_get_detail_bid, json=params_detail, headers=headers, verify=False,
                                       timeout=3000)
                    if r2.status_code != 204:
                        jsondata2 = r2.json()
                        if jsondata2['bidoNotifyContractorP'] is None:
                            _api_get_detail_bid = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/expose/lcnt/bid-po-bido-notify-contractor-view/get-by-id'
                            r2 = requests.post(_api_get_detail_bid, json=params_detail, headers=headers, verify=False,
                                               timeout=3000)
                            if r2.status_code != 204:
                                jsondata2 = r2.json()
                    bidpPlanDetail = jsondata2['bidoNotifyContractorM'] if 'bidoNotifyContractorM' in jsondata2 else \
                        jsondata2['bidoNotifyContractorP']
                    trangthai_thau = self.getNameStatus(_item['statusForNotify'], _item['bidCloseDate'],
                                                        _item['isInternet'])

                    if trangthai_thau in ('Chưa đóng thầu'):
                        # Chương 2
                        url_download = 'https://muasamcong.mpi.gov.vn/egp/contractorfe/viewer?formCode=P_C4&id=' + \
                                       _item['id'] + '&fileName=' + _item['id']
                    if trangthai_thau in ('Chưa mở thầu', 'Đang xét thầu'):
                        url_download = 'https://muasamcong.mpi.gov.vn/egp/contractorfe/viewer?formCode=P_C4&id=' + \
                                       _item['id'] + '&fileName=' + _item['id']
                    # self.downloadPdfMuaSamCong(url_download)
                    text_diadiemthuchiengoithau = ''
                    text_diadiemthuchiengoithaus = ''
                    if 'bidpBidLocationList' in jsondata2 and jsondata2['bidpBidLocationList'] is not None and len(
                            jsondata2['bidpBidLocationList']) > 0:
                        for item_diadiem_thuchien_goithau in jsondata2['bidpBidLocationList']:
                            xa = item_diadiem_thuchien_goithau['wardName'] + ', ' if item_diadiem_thuchien_goithau[
                                                                                         'wardName'] is not None else ''
                            huyen = item_diadiem_thuchien_goithau['districtName'] + ', ' if \
                                item_diadiem_thuchien_goithau['districtName'] is not None else ''
                            tinh = item_diadiem_thuchien_goithau['provName'] if item_diadiem_thuchien_goithau[
                                                                                    'provName'] is not None else ''
                            text_diadiemthuchiengoithau = xa + huyen + tinh + ' \n'
                            text_diadiemthuchiengoithaus += text_diadiemthuchiengoithau

                    if 'lsBidpBidLocationDTO' in jsondata2 and jsondata2[
                        'lsBidpBidLocationDTO'] is not None and len(jsondata2['lsBidpBidLocationDTO']) > 0:
                        for item_diadiem_thuchien_goithau in jsondata2['lsBidpBidLocationDTO']:
                            xa = item_diadiem_thuchien_goithau['wardName'] + ', ' if item_diadiem_thuchien_goithau[
                                                                                         'wardName'] is not None else ''
                            huyen = item_diadiem_thuchien_goithau['districtName'] + ', ' if \
                                item_diadiem_thuchien_goithau['districtName'] is not None else ''
                            tinh = item_diadiem_thuchien_goithau['provName'] if item_diadiem_thuchien_goithau[
                                                                                    'provName'] is not None else ''
                            text_diadiemthuchiengoithau = xa + huyen + tinh + ' \n'
                            text_diadiemthuchiengoithaus += text_diadiemthuchiengoithau
                    if bidpPlanDetail is not None:
                        _var_goithau = {
                            'goithauid': bidpPlanDetail['bidId'] if bidpPlanDetail is not None else '',
                            'magoithau': bidpPlanDetail['bidNo'],
                            'matbmt': bidpPlanDetail['notifyNo'],
                            'makhlcnt': bidpPlanDetail['planNo'],
                            'tenchudautu': bidpPlanDetail['investorCode'],
                            'mabenmoithau': bidpPlanDetail['procuringEntityCode'],
                            'tengoithau': bidpPlanDetail['bidName'],
                            'tendutoanmuasam': bidpPlanDetail['projectName'] if 'projectName' in bidpPlanDetail else
                            bidpPlanDetail['pName'],
                            'nguonvon': bidpPlanDetail['capitalDetail'],
                            'loaihopdong': bidpPlanDetail['contractType'] if 'contractType' in bidpPlanDetail else
                            bidpPlanDetail['cType'],
                            'hinhthuclcnt': bidpPlanDetail['bidForm'],
                            'phuongthuclcnt': bidpPlanDetail['bidMode'],
                            'linhvuc': bidpPlanDetail['investField'],
                            'thoigianthuchienhd': str(
                                bidpPlanDetail['contractPeriod'] if 'contractPeriod' in bidpPlanDetail else
                                bidpPlanDetail['cPeriod']) + ' ' + self.getNameOfPeriod(
                                bidpPlanDetail['contractPeriodUnit'] if 'contractPeriodUnit' in bidpPlanDetail else
                                bidpPlanDetail['cPeriodUnit']),
                            'giagoithau': bidpPlanDetail['bidPrice'],
                            'hinhthucduthau': 'Qua mạng' if bidpPlanDetail['isInternet'] == 1 else 'Không qua mạng',
                            'thoigiannhanhsdt_tungay': self.convertDateMuaSamCong(bidpPlanDetail['publicDate']),
                            'thoigiannhanhsdt_denngay': self.convertDateMuaSamCong(bidpPlanDetail['bidCloseDate']),
                            'diadiemphathanhhsmt': bidpPlanDetail['issueLocation'],
                            'thoigianhieuluc': str(
                                bidpPlanDetail['bidValidityPeriod']) + ' ' + self.getNameOfPeriod(
                                bidpPlanDetail['bidValidityPeriodUnit']),
                            'diadiemnhanhsdt': bidpPlanDetail['receiveLocation'],
                            'diadiemthuchiengoithau': text_diadiemthuchiengoithau,
                            'thoidiemdongthau': self.convertDateMuaSamCong(bidpPlanDetail['bidCloseDate']),
                            'thoidiemmothau': self.convertDateMuaSamCong(bidpPlanDetail['bidOpenDate']),
                            'diadiemmothau': bidpPlanDetail['bidOpenLocation'],
                            'sotiendambaoduthau': bidpPlanDetail[
                                'guaranteeValue'] if 'guaranteeValue' in bidpPlanDetail else bidpPlanDetail[
                                'bidGuaranteeValue'],
                            'hinhthucbaodamduthau': bidpPlanDetail[
                                'guaranteeForm'] if 'guaranteeForm' in bidpPlanDetail else bidpPlanDetail[
                                'bidGuaranteeForm'],
                            'filehosomoithau': '',
                        }
                        _ls_goithau.append(_var_goithau)

                    # Check file tải về, lấy dữ liệu trong bảng
                    # pdf_file = _item['id'] + '.pdf'
                    # cv = Converter(url_data+pdf_file)
                    # tables = cv.extract_tables(start=0, end=1)
                    # cv.close()
                    #
                    # for table in tables:
                    #     for record in range(1, len(table), 1):
                    #         if len(table[record]) >= 2:
                    #             _var_sanpham = {
                    #                 'sotbmt': bidpPlanDetail['notifyNo'],
                    #                 'goithauid': bidpPlanDetail['bidId'],
                    #                 'sanpham': table[record][1] if table[record][1] else None,
                    #                 'khoiluong': table[record][2].replace(".", "").replace(",", ".") if
                    #                 table[record][2] else None,
                    #                 'donvitinh': table[record][3] if table[record][3] else None,
                    #                 'random_code': b_random_code
                    #             }
                    #         else:
                    #             _var_sanpham = {
                    #                 'sotbmt': bidpPlanDetail['notifyNo'],
                    #                 'goithauid': bidpPlanDetail['bidId'],
                    #                 'sanpham': table[record][1] if table[record][1] else None,
                    #                 'khoiluong': None,
                    #                 'donvitinh': None,
                    #                 'random_code': b_random_code
                    #             }
                    #         _ls_sanpham.append(_var_sanpham)

                    # Thông tin nhà thầu tham gia
                    if bidpPlanDetail is not None:

                        _api_get_thongtin_nhathau_thamgia = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/expose/ldtkqmt/bid-notification-p/get-by-id'
                        params_get_thongtin_nhathau_thamgia = {
                            "notifyNo": bidpPlanDetail['notifyNo'],
                            # "notifyNo": "IB2300103908",
                            "type": "TBMT",
                            "packType": 0
                        }
                        r_thongtin_nhathau_thamgia = requests.post(_api_get_thongtin_nhathau_thamgia,
                                                                   json=params_get_thongtin_nhathau_thamgia,
                                                                   headers=headers, verify=False,
                                                                   timeout=3000)
                        if r_thongtin_nhathau_thamgia.status_code == 200:
                            jsondata_thongtin_nhathau_thamgia = r_thongtin_nhathau_thamgia.json()
                            detail_nhathau_thamgia = jsondata_thongtin_nhathau_thamgia[
                                'bidSubmissionByContractorViewResponse']
                            if detail_nhathau_thamgia is not None and 'bidSubmissionDTOList' in detail_nhathau_thamgia and \
                                    detail_nhathau_thamgia['bidSubmissionDTOList'] is not None:
                                if len(detail_nhathau_thamgia['bidSubmissionDTOList']) > 0:
                                    for item_nhathau_thamgia in detail_nhathau_thamgia['bidSubmissionDTOList']:
                                        _var_nhathau_thamgia = {
                                            'nhathau_thamgia_id': bidpPlanDetail[
                                                'id'] if bidpPlanDetail is not None else '',
                                            'matbmt': bidpPlanDetail['notifyNo'] if bidpPlanDetail is not None else '',
                                            'madinhdanh': item_nhathau_thamgia['contractorCode'],
                                            'tennhathau': item_nhathau_thamgia['contractorName'],
                                            'giaduthau': item_nhathau_thamgia['bidPrice'],
                                            'tile_giamgia': item_nhathau_thamgia['saleNumber'],
                                            'giaduthau_saugiamgia': item_nhathau_thamgia['bidFinalPrice'],
                                            'hieuluc_ehsdt': str(item_nhathau_thamgia['bidValidityNum']),
                                            'giatri_dambaoduthau': item_nhathau_thamgia[
                                                'bidGuarantee'] if 'bidGuarantee' in item_nhathau_thamgia else
                                            item_nhathau_thamgia['bidGuaranteeValue'],
                                            'hieuluc_dambaoduthau': str(item_nhathau_thamgia['bidGuaranteeValidity']),
                                            'thoigian_giaohang': str(
                                                item_nhathau_thamgia['contractPeriodDT']) + ' ' + self.getNameOfPeriod(
                                                item_nhathau_thamgia['contractPeriodDTUnit'])
                                        }
                                        _ls_nhathau_thamgia.append(_var_nhathau_thamgia)

                    # Kết quả chi tiết đấu thầu ( check nếu có key inputResultId trong kết quả trả về thì sẽ có kết quả đấu thầu )
                    if bidpPlanDetail is not None:
                        if 'inputResultId' in _item:
                            _api_get_thongtin_ketqua_dauthau = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/expose/contractor-input-result/get'
                            params_get_thongtin_ketqua_dauthau = {
                                # "id": "6ff60fc0-4e21-45f6-bd87-deb09a907660"
                                "id": _item['inputResultId']
                            }

                            r_thongtin_ketqua_dauthau = requests.post(_api_get_thongtin_ketqua_dauthau,
                                                                      json=params_get_thongtin_ketqua_dauthau,
                                                                      headers=headers, verify=False,
                                                                      timeout=3000)
                            if r_thongtin_ketqua_dauthau.status_code != 204:
                                jsondata_thongtin_ketqua_dauthau = r_thongtin_ketqua_dauthau.json()
                                bid_ketqua_dauthau = jsondata_thongtin_ketqua_dauthau['bideContractorInputResultDTO']
                                if len(bid_ketqua_dauthau['lotResultDTO']) > 0:
                                    for item_ketqua_dauthau in bid_ketqua_dauthau['lotResultDTO'][0]['contractorList']:
                                        _var_ketqua_dauthau = {
                                            'ketqua_id': item_ketqua_dauthau['id'],
                                            'matbmt': bidpPlanDetail['notifyNo'],
                                            'madinhdanh': item_ketqua_dauthau['orgCode'],
                                            'tennhathau': item_ketqua_dauthau['orgFullname'],
                                            'giaduthau': item_ketqua_dauthau['lotPrice'],
                                            'giatrungthau': item_ketqua_dauthau['lotFinalPrice'],
                                            'thoigian_giaohang': str(
                                                item_ketqua_dauthau['cperiod']) + ' ' + self.getNameOfPeriod(
                                                item_ketqua_dauthau['cperiodUnit'])
                                        }
                                        _ls_ketqua_dauthau.append(_var_ketqua_dauthau)

        # self.env.cr.execute(
        #     """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_goithau' """,
        #     (page_index_next, len(_ls_goithau) + cauhinh['quantity']))

        if len(_ls_goithau) > 0:
            b_json_data = json.dumps(_ls_goithau)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_goithau(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        if len(_ls_nhathau_thamgia) > 0:
            b_json_data = json.dumps(_ls_nhathau_thamgia)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_nhathau_thamgia(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        if len(_ls_ketqua_dauthau) > 0:
            b_json_data = json.dumps(_ls_ketqua_dauthau)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_ketqua_dauthau(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        if len(_ls_sanpham) > 0:
            b_json_data = json.dumps(_ls_sanpham)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_sanpham(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        # if len(_ls_sanpham) > 0:
        #     b_json_data = json.dumps(_ls_sanpham)
        #     string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_sanpham(%s,%s,%s);'''
        #     self.env.cr.execute(string_sql_insert_json,
        #                         (b_json_data, self._uid, b_random_code))
        #     self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi gói thầu trả về: %s" % (
                              'Gói thầu thầu', len(_ls_goithau)))

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi nhà thầu tham gia trả về: %s" % (
                              'Nhà thầu tham gia', len(_ls_nhathau_thamgia)))

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi kết quả đấu thầu trả về: %s" % (
                              'Kết quả đấu thầu', len(_ls_ketqua_dauthau)))

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi sản phẩm trả về: %s" % (
                              'Sản phẩm', len(_ls_sanpham)))

        # self.message_post(subject=u'Thông báo',
        #                   body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi sản phẩm trả về: %s" % (
        #                       'Sản phẩm', len(_ls_sanpham)))

    # Kế hoạch lựa chọn nhà thầu
    def btn_syns_kehoach_luachon_nhathau(self):

        self = self.sudo()
        _api_link = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/smart/search'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        ls_data_kehoach_luachon_nhathau = []

        # Update page_index về 0 nếu vượt quá limit page cron job
        self.env.cr.execute("""select * from fn_get_cauhinh_crawl(%s)""", ('btn_syns_kehoach_luachon_nhathau',))
        result = self.env.cr.dictfetchall()
        cauhinh = result[0]['json_cauhinh_crawl'][0]
        page_index_next = cauhinh['page_index'] + cauhinh['step'] + 1
        for pageNumber in range(cauhinh['page_index'], cauhinh['page_index'] + cauhinh['step']):
            params = [
                {
                    "pageSize": cauhinh['page_size'],
                    "pageNumber": pageNumber,
                    "query": [
                        {
                            "index": "es-contractor-selection",
                            "keyWord": "",
                            "matchType": "all-1",
                            "matchFields": [
                                "planNo",
                                "name"
                            ],
                            "filters": [
                                {
                                    "fieldName": "type",
                                    "searchType": "in",
                                    "fieldValues": [
                                        "es-plan-project-p"
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]

            r = requests.post(_api_link, json=params, verify=False, headers=headers)
            jsondata = r.json()

            if jsondata:
                datas = jsondata['page']['content']
                for _item in datas:
                    params_orgCode = {"id": _item['id']}
                    # lấy chi tiết KHLCNT
                    _api_get_detail_investor_approved = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/expose/lcnt/bid-po-bidp-plan-project-view/get-by-id'
                    r2 = requests.post(_api_get_detail_investor_approved, json=params_orgCode, headers=headers,
                                       verify=False,
                                       timeout=3000)
                    detail_investor_approved = r2.json()['bidPoBidpPlanProjectDetailView']
                    _var_data_kehoach_luachon_nhathau = {
                        # Thông tin thành lập
                        'makhlcnt': detail_investor_approved['planNo'],
                        'tenkhlcnt': detail_investor_approved['name'],
                        'phienbanthaydoi': detail_investor_approved['planVersion'],
                        'trangthaidangtai': detail_investor_approved['status'],
                        'tendutoanmuasam': detail_investor_approved['pname'],
                        'benmoithau': detail_investor_approved['investorName'],
                        'soluonggoithau': len(r2.json()['bidpPlanDetailToProjectList']),

                        # Tình trạng hoạt động
                        'dutoanmuasam': detail_investor_approved['investTotal'],
                        # 'sotienbangchu': self.converStatusChuDauTu(int(_item['status'])),
                        'sotienbangchu': '',
                        # Cơ quan chủ quản
                        'soquyetdinhpheduyet': detail_investor_approved['decisionNo'],
                        'ngaypheduyet': self.convertDateMuaSamCong(detail_investor_approved['decisionDate']),

                        # Địa chỉ trụ sở
                        'coquanbanhanhquyetdinh': detail_investor_approved['decisionAgency'],
                        'quyetdinhpheduyet': detail_investor_approved['decisionFileName']
                    }
                    ls_data_kehoach_luachon_nhathau.append(_var_data_kehoach_luachon_nhathau)

        self.env.cr.execute(
            """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_kehoach_luachon_nhathau' """,
            (page_index_next, len(ls_data_kehoach_luachon_nhathau) + cauhinh['quantity']))

        if len(ls_data_kehoach_luachon_nhathau) > 0:
            b_json_data = json.dumps(ls_data_kehoach_luachon_nhathau)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_kehoach_luachon_nhathau(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi kế hoạch lựa chọn nhà thầu trả về: %s" % (
                              'Kế hoạch lựa chọn nhà thầu', len(ls_data_kehoach_luachon_nhathau)))

    # Danh mục
    def btn_syns_danhmuc(self):

        self = self.sudo()
        _api_link = 'https://muasamcong.mpi.gov.vn/o/egp-portal-contractor-selection-v2/services/get/category'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }
        b_random_code = self.get_random_string(8)
        ls_data_danhmuc = []

        params = {
            "categoryTypeCodeLst": [
                "DM_TTPDDA",
                "DM_KHLCNT",
                "DM_LVLCNT",
                "DM_HTLCNT",
                "BID_INVEST_FORM",
                "BID_CONTRACT_TYPE",
                "DM_QT",
                "DM_PTLCNT",
                "FORM_OF_SPENDING",
                "BIDO_BID_STATUS",
                "BID_NOTIFY_STATUS",
                "DM_GROUP_DA",
                "DM_HTQLDA",
                "DM_HTDT",
                "BID_PLAN_STATUS",
                "DM_LHDLCNT",
                "DM_QT",
                "BIDO_BID_STATUS",
                "DM_TRANG_THAI",
                "DM_CQMS",
                "DM_CQMS_LV1",
                "DM_LYDO_HT_CPTPP",
                "DM_LYDO_HT",
                "CAT_COUNTRY_LIST"
            ]
        }

        r = requests.post(_api_link, json=params, verify=False, headers=headers)
        jsondata = r.json()

        if jsondata:
            datas = jsondata['categories']
            for item in datas['BIDO_BID_STATUS']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_HTQLDA']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_GROUP_DA']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['BID_NOTIFY_STATUS']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['CAT_COUNTRY_LIST']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_HTLCNT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_PTLCNT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['BID_INVEST_FORM']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_CQMS']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_TRANG_THAI']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_LHDLCNT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_LYDO_HT_CPTPP']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['FORM_OF_SPENDING']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_TTPDDA']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_CQMS_LV1']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['BID_CONTRACT_TYPE']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_LVLCNT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['BID_PLAN_STATUS']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_QT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_HTDT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_KHLCNT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

            for item in datas['DM_LYDO_HT']:
                _var_data_danhmuc = {
                    # Thông tin thành lập
                    'code': item['code'],
                    'name': item['name'],
                    'name_en': item['nameEn'],
                    'category_type_code': item['categoryTypeCode']
                }
                ls_data_danhmuc.append(_var_data_danhmuc)

        if len(ls_data_danhmuc) > 0:
            b_json_data = json.dumps(ls_data_danhmuc)
            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_danhmuc(%s,%s,%s);'''
            self.env.cr.execute(string_sql_insert_json,
                                (b_json_data, self._uid, b_random_code))
            self.env.cr.commit()

        self.message_post(subject=u'Thông báo',
                          body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi danh mục trả về: %s" % (
                              'Danh mục', len(ls_data_danhmuc)))

    # get token
    def get_token_spdv(self):

        self = self.sudo()
        _api_link = 'https://spdv.vnpt.vn/api/token'
        result = 0
        _pageIndex = 1
        headers = {
            'Content-Type': 'application/json',
        }

        params = {
            "UserName": "erp_bid",
            "Password": "erp_bid"
        }

        r = requests.post(_api_link, json=params, headers=headers, verify=False, timeout=3000)
        if r:
            return r.text

    def ams_btn_draft(self):
        # self.message_post(subject=u'Thông báo',
        #                   body=u"Bản ghi đã được chuyển trạng thái từ:  %s -> %s." % (self.state, 'draft'))
        self.state = 'draft'

    def ams_btn_confirm(self):
        # self.message_post(subject=u'Thông báo',
        #                   body=u"Bản ghi đã được chuyển trạng thái từ:  %s -> %s." % (self.state, 'approved'))
        self.state = 'approved'

    def unlink(self):
        if not self:
            return
        for _item in self:
            if _item.state not in ['reject', 'draft']:
                raise ValidationError(u"Bạn chỉ có thể xóa bản ghi ở trạng thái 'Từ chối' hoặc 'Dự thảo'.")
        res = super(vnpt_muasamcong_sync, self).unlink()
        return res

    # lấy giá trị từ tên label
    def get_value_by_label(self, label_name, driver):
        try:
            xpath = f"//div[contains(text(), '{label_name}')]/following-sibling::div[1]"
            text = driver.find_element(By.XPATH, xpath).text
        except NoSuchElementException:
            text = None
            pass

        return text

    def get_value_by_label_span(self, label_name, driver):
        try:
            xpath = f"//div/span[contains(text(), '{label_name}')]/../following-sibling::div[1]"
            text = driver.find_element(By.XPATH, xpath).text
        except NoSuchElementException:
            text = None
            pass

        return text

    # convert 15.000.000 VND -> 15000000
    def convert_money_vnd(self, string_value):
        if string_value is not None:
            # Xóa khoảng trắng dư thừa
            try:
                string_value = str(string_value).strip()
                # Kiểm tra định dạng chuỗi
                # if string_value.endswith(' VND') or string_value.count('.') == 2:
                # Xóa dấu chấm và ' VND', sau đó chuyển đổi thành số nguyên
                cleaned_string = string_value.replace('.', '').replace(' VND', '')
                try:
                    integer_value = int(cleaned_string)
                    return integer_value
                except ValueError:
                    return None
                # else:
                #     return None
            except ValueError:
                return None
        return None

    def convert_to_float(self, value):
        try:
            result = float(str(value).replace(",", "."))
        except (ValueError, TypeError):
            result = 0
        return result

    # 05/12/2023 15:50 -> 2023-12-05
    def convert_date_dd_mm_yyyy_hh_mm(self, date_time):
        if date_time is not None and date_time != '':
            return datetime.strptime(date_time, '%d/%m/%Y %H:%M').strftime('%Y-%m-%d')
        return None

    def convert_date_dd_mm_yyyy(self, date_time):
        if date_time is not None and date_time != '':
            return datetime.strptime(date_time, '%d/%m/%Y').strftime('%Y-%m-%d')
        return None

    # lấy giá trị từ url
    def get_value_from_url(self, url, key):
        # Phân tích URL để lấy ra query string
        parsed_url = urlparse(url)
        query_string = parsed_url.query

        # Trích xuất các tham số từ query string
        query_params = parse_qs(query_string)

        # Kiểm tra xem key có tồn tại trong query_params hay không
        if key in query_params:
            # Nếu key tồn tại, lấy value tương ứng
            value = query_params[key][0]
            return value
        else:
            # Nếu key không tồn tại, trả về None hoặc giá trị mặc định tùy ý
            return None

    def get_value_from_list_by_index(self, list, index):
        try:
            return list[index]
        except IndexError as e:
            return None

    def bot_tele_send_log(self, message):
        try:
            # local
            chat_id = tools.config['tele_chat_id']
            token = tools.config['tele_token']
            # chat_id = '-4009586818'
            # token = '6782774829:AAGkTtYqdomKmFoER_LIsheHAXxcSWaBZc8'
            url = f"https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}&text={message}"
            requests.get(url)
        except ConnectionError as e:
            pass
        except Exception as e:
            pass

    def rename_latest_pdf(self, directory_path, new_file_name):
        try:
            # Lấy danh sách tất cả các file trong thư mục chỉ định
            files = os.listdir(directory_path)

            # Lọc các file có phần mở rộng là '.pdf'
            pdf_files = [file for file in files if file.endswith('.pdf')]

            # Sắp xếp danh sách file PDF theo thời gian sửa đổi giảm dần
            pdf_files.sort(key=lambda x: os.path.getmtime(os.path.join(directory_path, x)), reverse=True)

            if len(pdf_files) > 0:
                latest_pdf_file = pdf_files[
                    0]  # Lấy file PDF mới nhất, nếu tên file = 'Chương_IV__Biểu_mẫu_mời_thầu_và_dự_thầu.pdf' thì đổi tên

                if ('Biểu_mẫu_mời_thầu_và_dự_thầu' in latest_pdf_file):
                    # Đường dẫn đến file PDF cần đổi tên
                    old_file_path = os.path.join(directory_path, latest_pdf_file)

                    # Tạo đường dẫn mới cho file PDF
                    new_file_path = os.path.join(directory_path, new_file_name + '.pdf')

                    # Kiểm tra xem tồn tại file đó trong folder không
                    if not os.path.exists(new_file_path):
                        # Đổi tên file PDF
                        os.rename(old_file_path, new_file_path)

                        print(f"Đổi tên file thành công: {latest_pdf_file} -> {new_file_name}.pdf")
                    else:
                        os.remove(old_file_path)
                        pass
            else:
                pass
                print("Không tìm thấy file PDF trong thư mục.")
        except FileExistsError as e:
            pass
        except OSError as e:
            pass

    # Gói thầu
    def btn_syns_goithau_webdriver(self):
        # local
        url_data = tools.config['crawl_bid_url_data']
        # url_data = "C:/Program Files/BID_CRAWL/odoo-15.0-crawl/data/"
        b_random_code = self.get_random_string(8)
        _ls_goithau = []
        _ls_nhathau_thamgia = []
        # list nhà thầu trúng thầu
        _ls_ketqua_dauthau = []
        _ls_sanpham = []
        options = webdriver.ChromeOptions();
        options.headless = False;
        options.add_argument('--disable-gpu')
        options.add_argument(
            "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
        # local
        prefs = {"download.default_directory": tools.config['crawl_bid_prefs_data']};
        # prefs = {"download.default_directory": "C:\Program Files\BID_CRAWL\odoo-15.0-crawl\data"};
        options.add_experimental_option("prefs", prefs);
        chromedriver = "chromedriver.exe"
        driver = webdriver.Chrome(service=Service(executable_path=binary_path), options=options)
        driver.maximize_window()
        driver.get('https://muasamcong.mpi.gov.vn/web/guest/contractor-selection?render=index')
        page_number = 1
        #check % ram bộ nhớ sử dụng
        memory_percent = psutil.virtual_memory().percent
        while True:
            try:
                _ls_goithau = []
                _ls_nhathau_thamgia = []
                # list nhà thầu trúng thầu
                _ls_ketqua_dauthau = []
                _ls_sanpham = []
                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                    "%d/%m/%Y %H:%M:%S") + " [Bot] : Truy cập page gói thầu, trang : " + str(page_number))
                driver.execute_script('window.scrollBy(0, 1000)')
                # time.sleep(5)

                # Gửi từ khóa vào ô tìm kiếm
                # search_box = driver.find_element(By.NAME, 'keyword')
                # search_box.send_keys('truyền dẫn')

                # Bấm nút tìm kiếm
                # search_button = driver.find_element(By.CLASS_NAME, 'search-button')
                # search_button.click()
                # search_button = WebDriverWait(driver, 20).until(
                #     EC.element_to_be_clickable((By.CLASS_NAME, 'button__search')))
                # search_button.click()
                # driver.implicitly_wait(10)

                a_elements = WebDriverWait(driver, 5).until(
                    EC.visibility_of_all_elements_located((By.CSS_SELECTOR, "div.content__body__left__item a[href]")))
                urls = []
                # In nội dung của các thẻ div
                for a in a_elements:
                    url = a.get_attribute('href')
                    urls.append(url)
                    # time.sleep(10)
                    # a.click()

                    # back page

                    # button_back = WebDriverWait(driver, 5).until(
                    # EC.element_to_be_clickable((By.CLASS_NAME, 'button-back')))
                    # button_back.click()
                    # time.sleep(5)
                    # driver.execute_script("window.history.go(-1)")
                    # time.sleep(10)

                # Duyệt qua danh sách các URL và thực hiện các hành động sau khi click vào từng trang
                for index, url in enumerate(urls):
                    # url2 = "https://muasamcong.mpi.gov.vn/web/guest/contractor-selection?p_p_id=egpportalcontractorselectionv2_WAR_egpportalcontractorselectionv2&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&_egpportalcontractorselectionv2_WAR_egpportalcontractorselectionv2_render=detail-v2&type=es-notify-contractor&stepCode=notify-contractor-step-4-kqlcnt&id=757ceaff-a246-4ca7-9174-670a358e3c43&notifyId=757ceaff-a246-4ca7-9174-670a358e3c43&inputResultId=6e2900da-7e90-4eef-b182-5326e38e2196&bidOpenId=45721c41-bf0a-48f8-a032-82b3e267691d&techReqId=c8dac08d-2484-438b-b4dd-44d6b3ee2979&bidPreNotifyResultId=undefined&bidPreOpenId=undefined&processApply=LDT&bidMode=1_HTHS&notifyNo=IB2300203684&planNo=PL2300003537&pno=undefined&step=tbmt&isInternet=1&caseKHKQ=undefined"
                    # driver.get(url2)
                    driver.get(url)

                    time.sleep(5)
                    driver.execute_script('window.scrollBy(0, 1000)')
                    # driver.implicitly_wait(10)
                    # Thực hiện các hành động trên trang con

                    # Thông tin cơ bản
                    text_goithau_id = self.get_value_from_url(url, 'id')

                    text_ketqua_goithau_id = self.get_value_from_url(url, 'inputResultId')
                    text_ma_tbmt = self.get_value_by_label('Mã TBMT', driver)

                    # check xem mã tbmt tồn tại trong db chưa
                    self.env.cr.execute(
                        """select count(*) from vnpt_asset_muasamcong_danhsach_goithau where matbmt = %s;""",
                        (text_ma_tbmt,))
                    check_exists_tbmt = self.env.cr.dictfetchall()

                    if (check_exists_tbmt[0]['count'] <= 0):
                        text_ngay_dangtai = self.get_value_by_label('Ngày đăng tải', driver)
                        # Thông tin chung của KHLCNT
                        self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                            "%d/%m/%Y %H:%M:%S") + f" [Bot] : Đang phân tích gói thầu số {index + 1} : {text_ma_tbmt} , trang : " + str(
                            page_number))

                        text_ma_khlcnt = self.get_value_by_label('Mã KHLCNT', driver)
                        text_phanloai_khlcnt = self.get_value_by_label('Phân loại KHLCNT', driver)
                        text_tendutoan_muasam = self.get_value_by_label('Tên dự toán mua sắm', driver)
                        text_giagoithau = self.get_value_by_label('Giá gói thầu', driver)

                        # Thông tin gói thầu
                        text_quytrinh_apdung = self.get_value_by_label('Quy trình áp dụng', driver)
                        text_ten_goithau = self.get_value_by_label('Tên gói thầu', driver)
                        text_ben_moithau = self.get_value_by_label('Bên mời thầu', driver)
                        text_chudautu = self.get_value_by_label('Chủ đầu tư', driver) if self.get_value_by_label(
                            'Chủ đầu tư', driver) != '' \
                            else self.get_value_by_label('Cơ quan ban hành quyết định', driver)
                        text_chitiet_nguonvon = self.get_value_by_label('Chi tiết nguồn vốn', driver)
                        text_linhvuc = self.get_value_by_label('Lĩnh vực', driver)
                        text_hinhthuc_lcnt = self.get_value_by_label('Hình thức lựa chọn nhà thầu', driver)
                        text_loai_hd = self.get_value_by_label('Loại hợp đồng', driver)
                        text_trongnuoc_quocte = self.get_value_by_label('Trong nước/ Quốc tế', driver)
                        text_phuongthuc_lcnt = self.get_value_by_label('Phương thức lựa chọn nhà thầu', driver)
                        text_thoigian_thuchien_hd = self.get_value_by_label('Thời gian thực hiện hợp đồng', driver)

                        # Cách thức dự thầu
                        text_hinhthuc_duthau = self.get_value_by_label('Hình thức dự thầu', driver)
                        text_diadiem_phathanh_e_hsmt = self.get_value_by_label_span('Địa điểm phát hành e-HSMT', driver)
                        text_chiphi_nop_e_hsdt = self.get_value_by_label_span('Chi phí nộp e-HSDT', driver)
                        text_diadiem_nhan_e_hsdt = self.get_value_by_label_span('Địa điểm nhận e-HSDT', driver)
                        text_diadiem_thuchien_goithau = self.get_value_by_label('Địa điểm thực hiện gói thầu', driver)

                        # Thông tin dự thầu
                        text_thoidiem_dongthau = self.get_value_by_label('Thời điểm đóng thầu', driver)
                        text_thoidiem_mothau = self.get_value_by_label('Thời điểm mở thầu', driver)
                        text_diadiem_mothau = self.get_value_by_label('Địa điểm mở thầu', driver)
                        text_hieuluc_hoso_duthau = self.get_value_by_label('Hiệu lực hồ sơ dự thầu', driver)
                        text_sotien_dambao_duthau = self.get_value_by_label('Số tiền đảm bảo dự thầu', driver)
                        text_hinhthuc_dambao_duthau = self.get_value_by_label('Hình thức đảm bảo dự thầu', driver)
                        text_loaicongtrinh = self.get_value_by_label('Loại công trình', driver)

                        # Thông tin quyết định phê duyệt
                        text_soquyetdinh_pheduyet = self.get_value_by_label('Số quyết định phê duyệt', driver)
                        text_ngaypheduyet = self.get_value_by_label('Ngày phê duyệt', driver)
                        text_coquan_banhanh_quyetdinh = self.get_value_by_label('Cơ quan ban hành quyết định', driver)
                        text_quyetdinh_pheduyet = self.get_value_by_label('Quyết định phê duyệt', driver)
                        text_noidung_suadoi_e_hsmt = self.get_value_by_label('Nội dung sửa đổi e-HSMT', driver)

                        # Xử lý bấm vào kế hoạch lựa chọn nhà thầu
                        try:
                            div_ma_khlcnts = WebDriverWait(driver, 5).until(
                            EC.visibility_of_all_elements_located((By.CSS_SELECTOR, "div.text-blue-4D7AE6")))

                            # WebDriverWait(driver, 5).until(
                            #     EC.presence_of_element_located((By.CSS_SELECTOR, 'div.text-blue-4D7AE6')))
                            button_ma_khlcnt = div_ma_khlcnts[0]
                            button_ma_khlcnt.click()

                            tab_thongtingoithau = WebDriverWait(driver, 5).until(
                                EC.presence_of_element_located((By.CSS_SELECTOR, "a[href='#tab2']")))
                            tab_thongtingoithau.click()

                            # Tìm đối tượng div bằng id
                            tr_elements = WebDriverWait(driver, 5).until(
                                EC.visibility_of_all_elements_located(
                                    (By.CSS_SELECTOR, "div#tab2 tbody tr")))
                            # Tìm tất cả các thẻ tr trong tbody của div

                            if len(tr_elements) > 0:

                                # Lặp qua danh sách các thẻ tr
                                for tr in tr_elements:
                                    try:
                                        # Tìm tất cả các thẻ td trong thẻ tr
                                        td_elements = WebDriverWait(tr, 10).until(
                                            EC.visibility_of_all_elements_located((By.TAG_NAME, 'td')))
                                        # Tab thông tin Nhà thầu trúng thầu có trên 3 cột
                                        if len(td_elements) > 3:
                                            # Lặp qua danh sách các thẻ td
                                            text_thongtin_goithau_ten_goithau = self.get_value_from_list_by_index(
                                                td_elements,
                                                1).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     1) is not None else None
                                            text_thongtin_goithau_gia_goithau = self.get_value_from_list_by_index(
                                                td_elements,
                                                3).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     3) is not None else None
                                            if text_thongtin_goithau_ten_goithau == text_ten_goithau:
                                                text_giagoithau = self.convert_money_vnd(
                                                    text_thongtin_goithau_gia_goithau)
                                                break

                                    except (TimeoutException, NoSuchElementException, StaleElementReferenceException,
                                            Exception):
                                        pass

                        except (TimeoutException, NoSuchElementException, StaleElementReferenceException, Exception):
                            time.sleep(2)
                            pass

                        # Nếu vẫn đang ở màn kế hoạch lcnt thì back về
                        try:
                            # Đợi tối đa 10 giây cho tab có id nhất định xuất hiện
                            tab_element = WebDriverWait(driver, 5).until(
                            EC.presence_of_element_located((By.CSS_SELECTOR, "a[href='#tab2']")))
                            # Nếu tìm thấy tab, thực hiện hành động back về trang trước đó
                            driver.back()
                        except:
                            # Nếu không tìm thấy tab, pass câu lệnh
                            pass

                        if text_ma_tbmt is not None and text_ma_tbmt != '':
                            _var_goithau = {
                                'goithauid': text_goithau_id,
                                'magoithau': text_goithau_id,
                                'matbmt': text_ma_tbmt,
                                'makhlcnt': text_ma_khlcnt,
                                'tenchudautu': text_chudautu,
                                'mabenmoithau': text_ben_moithau,
                                'tenbenmoithau': text_ben_moithau,
                                'tengoithau': text_ten_goithau,
                                'tendutoanmuasam': text_tendutoan_muasam,
                                'nguonvon': text_chitiet_nguonvon,
                                'loaihopdong': text_loai_hd,
                                'hinhthuclcnt': text_hinhthuc_lcnt,
                                'phuongthuclcnt': text_phuongthuc_lcnt,
                                'linhvuc': text_linhvuc,
                                'thoigianthuchienhd': text_thoigian_thuchien_hd,
                                'giagoithau': text_giagoithau,
                                'hinhthucduthau': text_hinhthuc_duthau,
                                'thoigiannhanhsdt_tungay': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_mothau),
                                'thoigiannhanhsdt_denngay': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_dongthau),
                                'diadiemphathanhhsmt': text_diadiem_phathanh_e_hsmt,
                                'thoigianhieuluc': text_hieuluc_hoso_duthau,
                                'diadiemnhanhsdt': text_diadiem_nhan_e_hsdt,
                                'diadiemthuchiengoithau': text_diadiem_thuchien_goithau,
                                'thoidiemdongthau': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_dongthau),
                                'thoidiemmothau': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_mothau),
                                'diadiemmothau': text_diadiem_mothau,
                                'sotiendambaoduthau': self.convert_money_vnd(text_sotien_dambao_duthau),
                                'hinhthucbaodamduthau': text_hinhthuc_dambao_duthau,
                                'filehosomoithau': '',
                                'url_goithau': url,
                                'ngay_dangtai': self.convert_date_dd_mm_yyyy_hh_mm(text_ngay_dangtai),
                                'phanloai_khlcnt': text_phanloai_khlcnt,
                                'quytrinh_apdung': text_quytrinh_apdung,
                                'trongnuoc_quocte': text_trongnuoc_quocte,
                                'chiphi_nop_e_hsdt': self.convert_money_vnd(text_chiphi_nop_e_hsdt),
                                'loaicongtrinh': text_loaicongtrinh,
                                'soquyetdinh_pheduyet': text_soquyetdinh_pheduyet,
                                'ngaypheduyet': self.convert_date_dd_mm_yyyy(text_ngaypheduyet),
                                'coquan_banhanh_quyetdinh': text_coquan_banhanh_quyetdinh,
                                'quyetdinh_pheduyet': text_quyetdinh_pheduyet,
                                'noidung_suadoi_e_hsmt': text_noidung_suadoi_e_hsmt
                            }
                            _ls_goithau.append(_var_goithau)

                            # Tải file hồ sơ
                            try:
                                button_hoso_moithau = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located(
                                        (By.CSS_SELECTOR, "a[href='#file-tender-invitation']")))
                                button_hoso_moithau.click()
                                driver.execute_script('window.scrollBy(0, 1000)')

                                button_bieumau_moithau = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located(
                                        (By.XPATH, "//span[contains(text(), 'Biểu mẫu mời thầu')]")))
                                button_bieumau_moithau.click()
                                # Lưu lại cửa sổ hiện tại của tab 1
                                window_tab_chitiet_goithau = driver.current_window_handle
                                # Lấy danh sách các cửa sổ hiện tại
                                window_handles = driver.window_handles
                                # Chuyển sang cửa sổ mới (tab 2)
                                for window_handle in window_handles:
                                    if window_handle != window_tab_chitiet_goithau:
                                        driver.switch_to.window(window_handle)
                                        break
                                # Xử lý các hoạt động trên tab 2
                                button_download_bieumau_moithau = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located((By.CLASS_NAME, "btn-primary")))
                                button_download_bieumau_moithau.click()
                                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                    "%d/%m/%Y %H:%M:%S") + f" [Bot] : Tải hồ sơ gói thầu  {text_ma_tbmt} , trang : {str(page_number)}")
                                # Đóng tab 2
                                driver.close()

                                # local
                                self.rename_latest_pdf(tools.config['crawl_bid_prefs_data'], text_ma_tbmt)
                                # self.rename_latest_pdf("C:\Program Files\BID_CRAWL\odoo-15.0-crawl\data", text_ma_tbmt)

                                # Chuyển về tab 1
                                driver.switch_to.window(window_tab_chitiet_goithau)

                                # Check file tải về, lấy dữ liệu trong bảng
                                try:
                                    pdf_file = text_ma_tbmt + '.pdf'
                                    cv = Converter(url_data + pdf_file)
                                    tables = cv.extract_tables(start=0, end=1)
                                    cv.close()

                                    if len(tables) > 0:
                                        for table in tables:
                                            for record in range(1, len(table), 1):
                                                if len(table[record]) >= 2:
                                                    _var_sanpham = {
                                                        'sotbmt': text_ma_tbmt,
                                                        'goithauid': text_goithau_id,
                                                        'sanpham': table[record][1] if table[record][1] else None,
                                                        'khoiluong': table[record][2].replace(".", "").replace(",",
                                                                                                               ".") if
                                                        table[record][2] else None,
                                                        'donvitinh': table[record][3] if table[record][3] else None,
                                                        'random_code': b_random_code
                                                    }
                                                else:
                                                    _var_sanpham = {
                                                        'sotbmt': text_ma_tbmt,
                                                        'goithauid': text_goithau_id,
                                                        'sanpham': table[record][1] if table[record][1] else None,
                                                        'khoiluong': None,
                                                        'donvitinh': None,
                                                        'random_code': b_random_code
                                                    }
                                                _ls_sanpham.append(_var_sanpham)
                                except Exception as e:
                                    self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                        "%d/%m/%Y %H:%M:%S") + f" [Bot] : Xảy ra lỗi khi phân tích gói thầu  {text_ma_tbmt} , trang : {str(page_number)}. \n Lỗi : {e}")
                                    pass
                            # except NoSuchElementException:
                            except TimeoutException:
                                pass
                            except ElementClickInterceptedException as e:
                                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                    "%d/%m/%Y %H:%M:%S") + f" [Bot] : Xảy ra lỗi khi phân tích gói thầu  {text_ma_tbmt} , trang : {str(page_number)}. \n Lỗi : {e}")
                                pass

                            # Biên bản mở thầu
                            try:
                                button_bienban_mothau = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located((By.CSS_SELECTOR, "a[href='#bidOpeningMinutes']")))
                                button_bienban_mothau.click()
                                driver.execute_script('window.scrollBy(0, 1000)')

                                # Tìm đối tượng div bằng id
                                tr_elements = WebDriverWait(driver, 5).until(
                                    EC.presence_of_all_elements_located(
                                        (By.CSS_SELECTOR, "div#bidOpeningMinutes tbody tr")))
                                # Tìm tất cả các thẻ tr trong tbody của div

                                if len(tr_elements) > 0:

                                    # Lặp qua danh sách các thẻ tr
                                    for tr in tr_elements:
                                        try:
                                            # Tìm tất cả các thẻ td trong thẻ tr
                                            td_elements = WebDriverWait(tr, 10).until(
                                                EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                            # tr.find_elements(By.TAG_NAME,'td')
                                            # Lặp qua danh sách các thẻ td
                                            text_bienban_mothau_madinhdanh = self.get_value_from_list_by_index(
                                                td_elements, 0).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     0) is not None else None
                                            text_bienban_mothau_tennhathau = self.get_value_from_list_by_index(
                                                td_elements, 1).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     1) is not None else None
                                            text_bienban_mothau_giaduthau = self.get_value_from_list_by_index(
                                                td_elements, 2).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     2) is not None else None
                                            text_bienban_mothau_tile_giamgia = self.get_value_from_list_by_index(
                                                td_elements, 3).text \
                                                if self.get_value_from_list_by_index(td_elements, 3) is not None else 0
                                            text_bienban_mothau_giaduthau_saugiamgia = self.get_value_from_list_by_index(
                                                td_elements, 4).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     4) is not None else None
                                            text_bienban_mothau_hieuluc_ehsdt = self.get_value_from_list_by_index(
                                                td_elements, 5).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     5) is not None else None
                                            text_bienban_mothau_giatri_dambaoduthau = self.get_value_from_list_by_index(
                                                td_elements, 6).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     6) is not None else None
                                            text_bienban_mothau_hieuluc_dambaoduthau = self.get_value_from_list_by_index(
                                                td_elements, 7).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     7) is not None else None
                                            text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                td_elements, 8).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     8) is not None else None

                                            _var_nhathau_thamgia = {
                                                'nhathau_thamgia_id': text_bienban_mothau_madinhdanh,
                                                'matbmt': text_ma_tbmt,
                                                'madinhdanh': text_bienban_mothau_madinhdanh,
                                                'tennhathau': text_bienban_mothau_tennhathau,
                                                'giaduthau': self.convert_money_vnd(text_bienban_mothau_giaduthau),
                                                'tile_giamgia': self.convert_to_float(text_bienban_mothau_tile_giamgia),
                                                'giaduthau_saugiamgia': self.convert_money_vnd(
                                                    text_bienban_mothau_giaduthau_saugiamgia),
                                                'hieuluc_ehsdt': text_bienban_mothau_hieuluc_ehsdt,
                                                'giatri_dambaoduthau': self.convert_money_vnd(
                                                    text_bienban_mothau_giatri_dambaoduthau),
                                                'hieuluc_dambaoduthau': text_bienban_mothau_hieuluc_dambaoduthau,
                                                'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                            }
                                            _ls_nhathau_thamgia.append(_var_nhathau_thamgia)

                                        except (NoSuchElementException, StaleElementReferenceException, Exception):
                                            pass
                            # except NoSuchElementException:
                            except TimeoutException:
                                pass

                            # Biên bản mở E-HSDXKT
                            try:
                                button_bienban_mothau = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located(
                                        (By.CSS_SELECTOR, "a[href='#hsdxkt']")))
                                button_bienban_mothau.click()
                                driver.execute_script('window.scrollBy(0, 1000)')

                                # Tìm đối tượng div bằng id
                                tr_elements = WebDriverWait(driver, 5).until(
                                    EC.presence_of_all_elements_located(
                                        (By.CSS_SELECTOR, "div#hsdxkt tbody tr")))
                                # Tìm tất cả các thẻ tr trong tbody của div

                                if len(tr_elements) > 0:

                                    # Lặp qua danh sách các thẻ tr
                                    for tr in tr_elements:
                                        try:
                                            # Tìm tất cả các thẻ td trong thẻ tr
                                            td_elements = WebDriverWait(tr, 10).until(
                                                EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                            # tr.find_elements(By.TAG_NAME,'td')
                                            # Lặp qua danh sách các thẻ td
                                            text_bienban_mothau_madinhdanh = self.get_value_from_list_by_index(
                                                td_elements, 0).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     0) is not None else None
                                            text_bienban_mothau_tennhathau = self.get_value_from_list_by_index(
                                                td_elements, 1).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     1) is not None else None
                                            text_bienban_mothau_giaduthau = 0
                                            text_bienban_mothau_tile_giamgia = 0
                                            text_bienban_mothau_giaduthau_saugiamgia = 0
                                            text_bienban_mothau_hieuluc_ehsdt = self.get_value_from_list_by_index(
                                                td_elements, 2).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     2) is not None else None
                                            text_bienban_mothau_giatri_dambaoduthau = self.get_value_from_list_by_index(
                                                td_elements, 3).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     3) is not None else None
                                            text_bienban_mothau_hieuluc_dambaoduthau = self.get_value_from_list_by_index(
                                                td_elements, 4).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     4) is not None else None
                                            text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                td_elements, 5).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     5) is not None else None

                                            _var_nhathau_thamgia = {
                                                'nhathau_thamgia_id': text_bienban_mothau_madinhdanh,
                                                'matbmt': text_ma_tbmt,
                                                'madinhdanh': text_bienban_mothau_madinhdanh,
                                                'tennhathau': text_bienban_mothau_tennhathau,
                                                'giaduthau': self.convert_money_vnd(text_bienban_mothau_giaduthau),
                                                'tile_giamgia': self.convert_to_float(
                                                    text_bienban_mothau_tile_giamgia),
                                                'giaduthau_saugiamgia': self.convert_money_vnd(
                                                    text_bienban_mothau_giaduthau_saugiamgia),
                                                'hieuluc_ehsdt': text_bienban_mothau_hieuluc_ehsdt,
                                                'giatri_dambaoduthau': self.convert_money_vnd(
                                                    text_bienban_mothau_giatri_dambaoduthau),
                                                'hieuluc_dambaoduthau': text_bienban_mothau_hieuluc_dambaoduthau,
                                                'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                            }
                                            _ls_nhathau_thamgia.append(_var_nhathau_thamgia)

                                        except (NoSuchElementException, StaleElementReferenceException, Exception):
                                            pass
                            # except NoSuchElementException:
                            except TimeoutException:
                                pass

                            # Kết quả lựa chọn nhà thầu
                            try:
                                button_ketqua_lcnt = WebDriverWait(driver, 5).until(
                                    EC.presence_of_element_located(
                                        (By.CSS_SELECTOR, "a[href='#contractorSelectionResults']")))
                                button_ketqua_lcnt.click()
                                driver.execute_script('window.scrollBy(0, 1000)')

                                # Tìm đối tượng div bằng id
                                # tr_elements = WebDriverWait(driver, 5).until(
                                #     EC.presence_of_all_elements_located(
                                #         (By.CSS_SELECTOR, "div#contractorSelectionResults tbody tr")))

                                tr_elements = WebDriverWait(driver, 5).until(
                                    EC.presence_of_all_elements_located(
                                        (By.CSS_SELECTOR, "#contractorSelectionResults > div:nth-child(2) > div.card-body.item-table > table > tbody tr")))
                                # Tìm tất cả các thẻ tr trong tbody của div

                                if len(tr_elements) > 0:

                                    # Lặp qua danh sách các thẻ tr
                                    for tr in tr_elements:
                                        try:
                                            # Tìm tất cả các thẻ td trong thẻ tr
                                            td_elements = WebDriverWait(tr, 10).until(
                                                EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                            # Tab thông tin Nhà thầu trúng thầu có trên 3 cột, nếu có cột liên danh là 8 cột
                                            text_ketqua_lcnt_madinhdanh = self.get_value_from_list_by_index(
                                                td_elements,
                                                1).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     1) is not None else None
                                            text_ketqua_lcnt_tennhathau = self.get_value_from_list_by_index(
                                                td_elements,
                                                2).text \
                                                if self.get_value_from_list_by_index(td_elements,
                                                                                     2) is not None else None
                                            if len(td_elements) > 3:
                                                # Lặp qua danh sách các thẻ td
                                                text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    3).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         3) is not None else None
                                                text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    4).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         4) is not None else 0
                                                text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                    td_elements, 5).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         5) is not None else None
                                            # Nếu có điểm kỹ thuật, tổng số cột là 8, các cột sẽ là : stt, mã định danh, tên nhà thầu, tên liên danh, giá dự thầu, điểm kỹ thuật, giá trúng thầu, thời gian giao hàng, thời gian giao hàng chi tiết
                                            if len(td_elements) > 7:
                                                # Lặp qua danh sách các thẻ td
                                                text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    3).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         3) is not None else None
                                                text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    5).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         5) is not None else 0
                                                text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                    td_elements, 6).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         6) is not None else None
                                            # Nếu có liên danh, tổng số cột là 9 các cột sẽ là : stt, mã định danh, tên nhà thầu, tên liên danh, giá dự thầu, điểm kỹ thuật, giá trúng thầu, thời gian giao hàng, thời gian giao hàng chi tiết
                                            if len(td_elements) > 8:
                                                # Lặp qua danh sách các thẻ td
                                                text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    4).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         4) is not None else None
                                                text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                    td_elements,
                                                    6).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         6) is not None else 0
                                                text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                    td_elements, 7).text \
                                                    if self.get_value_from_list_by_index(td_elements,
                                                                                         7) is not None else None

                                            _var_ketqua_dauthau = {
                                                'ketqua_id': text_ketqua_goithau_id,
                                                'goithau_id': text_goithau_id,
                                                'matbmt': text_ma_tbmt,
                                                'madinhdanh': text_ketqua_lcnt_madinhdanh,
                                                'tennhathau': text_ketqua_lcnt_tennhathau,
                                                'giaduthau': self.convert_money_vnd(text_ketqua_lcnt_giaduthau),
                                                'giatrungthau': self.convert_money_vnd(
                                                    text_ketqua_lcnt_giatrungthau),
                                                'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                            }
                                            _ls_ketqua_dauthau.append(_var_ketqua_dauthau)

                                        except NoSuchElementException:
                                            pass
                            except TimeoutException:
                                pass
                            # time.sleep(5)

                    # Quay lại trang trước đó
                    driver.back()
                    # time.sleep(5)

                time.sleep(5)
                if len(_ls_goithau) > 0:
                    b_json_data = json.dumps(_ls_goithau)
                    string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_goithau(%s,%s,%s);'''
                    self.env.cr.execute(string_sql_insert_json,
                                        (b_json_data, self._uid, b_random_code))
                    self.env.cr.commit()

                if len(_ls_nhathau_thamgia) > 0:
                    b_json_data = json.dumps(_ls_nhathau_thamgia)
                    string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_nhathau_thamgia(%s,%s,%s);'''
                    self.env.cr.execute(string_sql_insert_json,
                                        (b_json_data, self._uid, b_random_code))
                    self.env.cr.commit()

                if len(_ls_ketqua_dauthau) > 0:
                    b_json_data = json.dumps(_ls_ketqua_dauthau)
                    string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_ketqua_dauthau(%s,%s,%s);'''
                    self.env.cr.execute(string_sql_insert_json,
                                        (b_json_data, self._uid, b_random_code))
                    self.env.cr.commit()

                if len(_ls_sanpham) > 0:
                    b_json_data = json.dumps(_ls_sanpham)
                    string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_sanpham(%s,%s,%s);'''
                    self.env.cr.execute(string_sql_insert_json,
                                        (b_json_data, self._uid, b_random_code))
                    self.env.cr.commit()

                self.message_post(subject=u'Thông báo',
                                  body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi gói thầu trả về: %s" % (
                                      'Gói thầu thầu', len(_ls_goithau)))

                self.message_post(subject=u'Thông báo',
                                  body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi nhà thầu tham gia trả về: %s" % (
                                      'Nhà thầu tham gia', len(_ls_nhathau_thamgia)))

                self.message_post(subject=u'Thông báo',
                                  body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi kết quả đấu thầu trả về: %s" % (
                                      'Kết quả đấu thầu', len(_ls_ketqua_dauthau)))

                self.message_post(subject=u'Thông báo',
                                  body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi sản phẩm trả về: %s" % (
                                      'Sản phẩm', len(_ls_sanpham)))

                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                    "%d/%m/%Y %H:%M:%S") + " [Bot] : Thu thập được số gói thầu " + str(
                    len(_ls_goithau)) + " trang " + str(page_number))
                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                    "%d/%m/%Y %H:%M:%S") + " [Bot] : Thu thập được số sản phẩm " + str(
                    len(_ls_sanpham)) + " trang " + str(page_number))
                # Đi đến trang kế tiếp (Ví dụ: bấm vào nút phân trang)
                while True:
                    try:
                        next_button = WebDriverWait(driver, 5).until(
                            EC.presence_of_element_located((By.CSS_SELECTOR, "button.btn-next")))
                        # Thay đổi XPath để tìm nút phân trang
                        if next_button.is_enabled():
                            next_button.click()
                            page_number = page_number + 1
                            time.sleep(5)
                            self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                "%d/%m/%Y %H:%M:%S") + f" [Bot] : Đang bấm next trang {str(page_number)} gói thầu...")  # Đợi để trang web tải xong trước khi crawl dữ liệu từ trang tiếp theo
                            break
                        else:
                            continue
                    except ElementClickInterceptedException:
                        self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                            "%d/%m/%Y %H:%M:%S") + f" [Bot] : Time out không bấm được button next page ... đang tải lại trang {str(page_number)} gói thầu...")
                        driver.refresh()
                        # pass
                        # continue
                    # continue
                    # Thoát vòng lặp nếu không còn trang kế tiếp
            except TimeoutException:
                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                    "%d/%m/%Y %H:%M:%S") + f" [Bot] : Page time out ... đang tải lại trang gói thầu...")
                driver.refresh()
                continue

        driver.quit()

        # self.env.cr.execute(
        #     """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_goithau' """,
        #     (page_index_next, len(_ls_goithau) + cauhinh['quantity']))

        # self.message_post(subject=u'Thông báo',
        #                   body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi sản phẩm trả về: %s" % (
        #                       'Sản phẩm', len(_ls_sanpham)))

    # Gói thầu theo số thông báo mời thầu
    def btn_syns_goithau_with_so_tbmt_webdriver(self):
        # local
        url_data = tools.config['crawl_bid_url_data']
        # url_data = "C:/Program Files/BID_CRAWL/odoo-15.0-crawl/data/"

        options = webdriver.ChromeOptions();
        options.headless = False;
        options.add_argument('--disable-gpu')
        options.add_argument(
            "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36")
        # local
        prefs = {"download.default_directory": tools.config['crawl_bid_prefs_data']};
        # prefs = {"download.default_directory": "C:\Program Files\BID_CRAWL\odoo-15.0-crawl\data"};
        options.add_experimental_option("prefs", prefs);
        chromedriver = "chromedriver.exe"
        driver = webdriver.Chrome(service=Service(executable_path=binary_path), options=options)
        print("binary_path")
        print(binary_path)
        driver.maximize_window()
        driver.get('https://muasamcong.mpi.gov.vn/web/guest/contractor-selection?render=index')
        page_number = 1
        while True:
            try:

                _ls_goithau = []
                _ls_nhathau_thamgia = []
                # list nhà thầu trúng thầu
                _ls_ketqua_dauthau = []
                _ls_sanpham = []
                time.sleep(2)

                # Duyệt list số thông báo mời thầu
                self.env.cr.execute("""select * from bid_muasamcong_danhsach_tukhoa_cntt where type_tukhoa = %s and code not in (select matbmt from bid_muasamcong_danhsach_goithau)""", ('so_tbmt',))
                results = self.env.cr.dictfetchall()
                goithauso = 1
                for index_goithau in range(1, len(results)):
                    try:
                        _ls_goithau = []
                        _ls_nhathau_thamgia = []
                        # list nhà thầu trúng thầu
                        _ls_ketqua_dauthau = []
                        _ls_sanpham = []
                        b_random_code = self.get_random_string(8)

                        # Gửi từ khóa vào ô tìm kiếm
                        search_box = driver.find_element(By.NAME, 'keyword')
                        search_box.clear()
                        search_box.send_keys(results[index_goithau]['code'])

                        # Bấm nút tìm kiếm
                        # search_button = driver.find_element(By.CLASS_NAME, 'search-button')
                        # search_button.click()
                        search_button = WebDriverWait(driver, 20).until(
                            EC.element_to_be_clickable((By.CLASS_NAME, 'button__search')))
                        search_button.click()
                        # driver.implicitly_wait(10)

                        a_elements = WebDriverWait(driver, 5).until(
                            EC.visibility_of_all_elements_located((By.CSS_SELECTOR, "div.content__body__left__item a[href]")))
                        urls = []
                        # In nội dung của các thẻ div
                        for a in a_elements:
                            url = a.get_attribute('href')
                            urls.append(url)

                        # Duyệt qua danh sách các URL và thực hiện các hành động sau khi click vào từng trang
                        for index, url in enumerate(urls):
                            driver.get(url)

                            time.sleep(1)
                            driver.execute_script('window.scrollBy(0, 1000)')
                            # driver.implicitly_wait(10)
                            # Thực hiện các hành động trên trang con

                            # Thông tin cơ bản
                            text_goithau_id = self.get_value_from_url(url, 'id')

                            text_ketqua_goithau_id = self.get_value_from_url(url, 'inputResultId')
                            text_ma_tbmt = self.get_value_by_label('Mã TBMT', driver)

                            # check xem mã tbmt tồn tại trong db chưa
                            self.env.cr.execute(
                                """select count(*) from vnpt_asset_muasamcong_danhsach_goithau where matbmt = %s;""",
                                (text_ma_tbmt,))
                            check_exists_tbmt = self.env.cr.dictfetchall()

                            if (check_exists_tbmt[0]['count'] <= 0):
                                text_ngay_dangtai = self.get_value_by_label('Ngày đăng tải', driver)
                                # Thông tin chung của KHLCNT
                                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                    "%d/%m/%Y %H:%M:%S") + f" [Bot] : Đang phân tích gói thầu số {index_goithau} : {text_ma_tbmt}")

                                text_ma_khlcnt = self.get_value_by_label('Mã KHLCNT', driver)
                                text_phanloai_khlcnt = self.get_value_by_label('Phân loại KHLCNT', driver)
                                text_tendutoan_muasam = self.get_value_by_label('Tên dự toán mua sắm', driver)
                                text_giagoithau = self.get_value_by_label('Giá gói thầu', driver)

                                # Thông tin gói thầu
                                text_quytrinh_apdung = self.get_value_by_label('Quy trình áp dụng', driver)
                                text_ten_goithau = self.get_value_by_label('Tên gói thầu', driver)
                                text_ben_moithau = self.get_value_by_label('Bên mời thầu', driver)
                                text_chudautu = self.get_value_by_label('Chủ đầu tư', driver) if self.get_value_by_label('Chủ đầu tư', driver) != '' \
                                    else self.get_value_by_label('Cơ quan ban hành quyết định', driver)
                                text_chitiet_nguonvon = self.get_value_by_label('Chi tiết nguồn vốn', driver)
                                text_linhvuc = self.get_value_by_label('Lĩnh vực', driver)
                                text_hinhthuc_lcnt = self.get_value_by_label('Hình thức lựa chọn nhà thầu', driver)
                                text_loai_hd = self.get_value_by_label('Loại hợp đồng', driver)
                                text_trongnuoc_quocte = self.get_value_by_label('Trong nước/ Quốc tế', driver)
                                text_phuongthuc_lcnt = self.get_value_by_label('Phương thức lựa chọn nhà thầu', driver)
                                text_thoigian_thuchien_hd = self.get_value_by_label('Thời gian thực hiện hợp đồng', driver)

                                # Cách thức dự thầu
                                text_hinhthuc_duthau = self.get_value_by_label('Hình thức dự thầu', driver)
                                text_diadiem_phathanh_e_hsmt = self.get_value_by_label_span('Địa điểm phát hành e-HSMT', driver)
                                text_chiphi_nop_e_hsdt = self.get_value_by_label_span('Chi phí nộp e-HSDT', driver)
                                text_diadiem_nhan_e_hsdt = self.get_value_by_label_span('Địa điểm nhận e-HSDT', driver)
                                text_diadiem_thuchien_goithau = self.get_value_by_label('Địa điểm thực hiện gói thầu', driver)

                                # Thông tin dự thầu
                                text_thoidiem_dongthau = self.get_value_by_label('Thời điểm đóng thầu', driver)
                                text_thoidiem_mothau = self.get_value_by_label('Thời điểm mở thầu', driver)
                                text_diadiem_mothau = self.get_value_by_label('Địa điểm mở thầu', driver)
                                text_hieuluc_hoso_duthau = self.get_value_by_label('Hiệu lực hồ sơ dự thầu', driver)
                                text_sotien_dambao_duthau = self.get_value_by_label('Số tiền đảm bảo dự thầu', driver)
                                text_hinhthuc_dambao_duthau = self.get_value_by_label('Hình thức đảm bảo dự thầu', driver)
                                text_loaicongtrinh = self.get_value_by_label('Loại công trình', driver)

                                # Thông tin quyết định phê duyệt
                                text_soquyetdinh_pheduyet = self.get_value_by_label('Số quyết định phê duyệt', driver)
                                text_ngaypheduyet = self.get_value_by_label('Ngày phê duyệt', driver)
                                text_coquan_banhanh_quyetdinh = self.get_value_by_label('Cơ quan ban hành quyết định', driver)
                                text_quyetdinh_pheduyet = self.get_value_by_label('Quyết định phê duyệt', driver)
                                text_noidung_suadoi_e_hsmt = self.get_value_by_label('Nội dung sửa đổi e-HSMT', driver)

                                # Xử lý bấm vào kế hoạch lựa chọn nhà thầu
                                try:
                                    div_ma_khlcnts = WebDriverWait(driver, 5).until(
                                        EC.visibility_of_all_elements_located((By.CSS_SELECTOR, "div.text-blue-4D7AE6")))

                                    # WebDriverWait(driver, 5).until(
                                    #     EC.presence_of_element_located((By.CSS_SELECTOR, 'div.text-blue-4D7AE6')))
                                    button_ma_khlcnt = div_ma_khlcnts[0]
                                    button_ma_khlcnt.click()

                                    tab_thongtingoithau = WebDriverWait(driver, 5).until(
                                        EC.presence_of_element_located((By.CSS_SELECTOR, "a[href='#tab2']")))
                                    tab_thongtingoithau.click()

                                    # Tìm đối tượng div bằng id
                                    tr_elements = WebDriverWait(driver, 5).until(
                                        EC.visibility_of_all_elements_located(
                                            (By.CSS_SELECTOR, "div#tab2 tbody tr")))
                                    # Tìm tất cả các thẻ tr trong tbody của div

                                    if len(tr_elements) > 0:

                                        # Lặp qua danh sách các thẻ tr
                                        for tr in tr_elements:
                                            try:
                                                # Tìm tất cả các thẻ td trong thẻ tr
                                                td_elements = WebDriverWait(tr, 10).until(
                                                    EC.visibility_of_all_elements_located((By.TAG_NAME, 'td')))
                                                # Tab thông tin Nhà thầu trúng thầu có trên 3 cột
                                                if len(td_elements) > 3:
                                                    # Lặp qua danh sách các thẻ td
                                                    text_thongtin_goithau_ten_goithau = self.get_value_from_list_by_index(
                                                        td_elements,
                                                        1).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             1) is not None else None
                                                    text_thongtin_goithau_gia_goithau = self.get_value_from_list_by_index(
                                                        td_elements,
                                                        3).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             3) is not None else None
                                                    if text_thongtin_goithau_ten_goithau == text_ten_goithau:
                                                        text_giagoithau = self.convert_money_vnd(
                                                            text_thongtin_goithau_gia_goithau)
                                                        break

                                            except (TimeoutException, NoSuchElementException, StaleElementReferenceException, Exception):
                                                pass

                                except (TimeoutException, NoSuchElementException, StaleElementReferenceException, Exception):
                                    time.sleep(2)
                                    pass

                                # Nếu vẫn đang ở màn kế hoạch lcnt thì back về
                                try:
                                    # Đợi tối đa 10 giây cho tab có id nhất định xuất hiện
                                    tab_element = WebDriverWait(driver, 5).until(
                                        EC.presence_of_element_located((By.CSS_SELECTOR, "a[href='#tab2']")))
                                    # Nếu tìm thấy tab, thực hiện hành động back về trang trước đó
                                    driver.back()
                                except:
                                    # Nếu không tìm thấy tab, pass câu lệnh
                                    pass

                                if text_ma_tbmt is not None and text_ma_tbmt != '':
                                    _var_goithau = {
                                        'goithauid': text_goithau_id,
                                        'magoithau': text_goithau_id,
                                        'matbmt': text_ma_tbmt,
                                        'makhlcnt': text_ma_khlcnt,
                                        'tenchudautu': text_chudautu,
                                        'mabenmoithau': text_ben_moithau,
                                        'tenbenmoithau': text_ben_moithau,
                                        'tengoithau': text_ten_goithau,
                                        'tendutoanmuasam': text_tendutoan_muasam,
                                        'nguonvon': text_chitiet_nguonvon,
                                        'loaihopdong': text_loai_hd,
                                        'hinhthuclcnt': text_hinhthuc_lcnt,
                                        'phuongthuclcnt': text_phuongthuc_lcnt,
                                        'linhvuc': text_linhvuc,
                                        'thoigianthuchienhd': text_thoigian_thuchien_hd,
                                        'giagoithau': text_giagoithau,
                                        'hinhthucduthau': text_hinhthuc_duthau,
                                        'thoigiannhanhsdt_tungay': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_mothau),
                                        'thoigiannhanhsdt_denngay': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_dongthau),
                                        'diadiemphathanhhsmt': text_diadiem_phathanh_e_hsmt,
                                        'thoigianhieuluc': text_hieuluc_hoso_duthau,
                                        'diadiemnhanhsdt': text_diadiem_nhan_e_hsdt,
                                        'diadiemthuchiengoithau': text_diadiem_thuchien_goithau,
                                        'thoidiemdongthau': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_dongthau),
                                        'thoidiemmothau': self.convert_date_dd_mm_yyyy_hh_mm(text_thoidiem_mothau),
                                        'diadiemmothau': text_diadiem_mothau,
                                        'sotiendambaoduthau': self.convert_money_vnd(text_sotien_dambao_duthau),
                                        'hinhthucbaodamduthau': text_hinhthuc_dambao_duthau,
                                        'filehosomoithau': '',
                                        'url_goithau': url,
                                        'ngay_dangtai': self.convert_date_dd_mm_yyyy_hh_mm(text_ngay_dangtai),
                                        'phanloai_khlcnt': text_phanloai_khlcnt,
                                        'quytrinh_apdung': text_quytrinh_apdung,
                                        'trongnuoc_quocte': text_trongnuoc_quocte,
                                        'chiphi_nop_e_hsdt': self.convert_money_vnd(text_chiphi_nop_e_hsdt),
                                        'loaicongtrinh': text_loaicongtrinh,
                                        'soquyetdinh_pheduyet': text_soquyetdinh_pheduyet,
                                        'ngaypheduyet': self.convert_date_dd_mm_yyyy(text_ngaypheduyet),
                                        'coquan_banhanh_quyetdinh': text_coquan_banhanh_quyetdinh,
                                        'quyetdinh_pheduyet': text_quyetdinh_pheduyet,
                                        'noidung_suadoi_e_hsmt': text_noidung_suadoi_e_hsmt
                                    }
                                    _ls_goithau.append(_var_goithau)

                                    # Biên bản mở thầu
                                    try:
                                        button_bienban_mothau = WebDriverWait(driver, 5).until(
                                            EC.presence_of_element_located(
                                                (By.CSS_SELECTOR, "a[href='#bidOpeningMinutes']")))
                                        button_bienban_mothau.click()
                                        driver.execute_script('window.scrollBy(0, 1000)')

                                        # Tìm đối tượng div bằng id
                                        tr_elements = WebDriverWait(driver, 5).until(
                                            EC.presence_of_all_elements_located(
                                                (By.CSS_SELECTOR, "div#bidOpeningMinutes tbody tr")))
                                        # Tìm tất cả các thẻ tr trong tbody của div

                                        if len(tr_elements) > 0:

                                            # Lặp qua danh sách các thẻ tr
                                            for tr in tr_elements:
                                                try:
                                                    # Tìm tất cả các thẻ td trong thẻ tr
                                                    td_elements = WebDriverWait(tr, 10).until(
                                                        EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                                    # tr.find_elements(By.TAG_NAME,'td')
                                                    # Lặp qua danh sách các thẻ td
                                                    text_bienban_mothau_madinhdanh = self.get_value_from_list_by_index(
                                                        td_elements, 0).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             0) is not None else None
                                                    text_bienban_mothau_tennhathau = self.get_value_from_list_by_index(
                                                        td_elements, 1).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             1) is not None else None
                                                    text_bienban_mothau_giaduthau = self.get_value_from_list_by_index(
                                                        td_elements, 2).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             2) is not None else None
                                                    text_bienban_mothau_tile_giamgia = self.get_value_from_list_by_index(
                                                        td_elements, 3).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             3) is not None else 0
                                                    text_bienban_mothau_giaduthau_saugiamgia = self.get_value_from_list_by_index(
                                                        td_elements, 4).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             4) is not None else None
                                                    text_bienban_mothau_hieuluc_ehsdt = self.get_value_from_list_by_index(
                                                        td_elements, 5).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             5) is not None else None
                                                    text_bienban_mothau_giatri_dambaoduthau = self.get_value_from_list_by_index(
                                                        td_elements, 6).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             6) is not None else None
                                                    text_bienban_mothau_hieuluc_dambaoduthau = self.get_value_from_list_by_index(
                                                        td_elements, 7).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             7) is not None else None
                                                    text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                        td_elements, 8).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             8) is not None else None

                                                    _var_nhathau_thamgia = {
                                                        'nhathau_thamgia_id': text_bienban_mothau_madinhdanh,
                                                        'matbmt': text_ma_tbmt,
                                                        'madinhdanh': text_bienban_mothau_madinhdanh,
                                                        'tennhathau': text_bienban_mothau_tennhathau,
                                                        'giaduthau': self.convert_money_vnd(text_bienban_mothau_giaduthau),
                                                        'tile_giamgia': self.convert_to_float(
                                                            text_bienban_mothau_tile_giamgia),
                                                        'giaduthau_saugiamgia': self.convert_money_vnd(
                                                            text_bienban_mothau_giaduthau_saugiamgia),
                                                        'hieuluc_ehsdt': text_bienban_mothau_hieuluc_ehsdt,
                                                        'giatri_dambaoduthau': self.convert_money_vnd(
                                                            text_bienban_mothau_giatri_dambaoduthau),
                                                        'hieuluc_dambaoduthau': text_bienban_mothau_hieuluc_dambaoduthau,
                                                        'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                                    }
                                                    _ls_nhathau_thamgia.append(_var_nhathau_thamgia)

                                                except (NoSuchElementException, StaleElementReferenceException, Exception):
                                                    pass
                                    # except NoSuchElementException:
                                    except TimeoutException:
                                        pass

                                    # Biên bản mở E-HSDXKT
                                    try:
                                        button_bienban_mothau = WebDriverWait(driver, 5).until(
                                            EC.presence_of_element_located(
                                                (By.CSS_SELECTOR, "a[href='#hsdxkt']")))
                                        button_bienban_mothau.click()
                                        driver.execute_script('window.scrollBy(0, 1000)')

                                        # Tìm đối tượng div bằng id
                                        tr_elements = WebDriverWait(driver, 5).until(
                                            EC.presence_of_all_elements_located(
                                                (By.CSS_SELECTOR, "div#hsdxkt tbody tr")))
                                        # Tìm tất cả các thẻ tr trong tbody của div

                                        if len(tr_elements) > 0:

                                            # Lặp qua danh sách các thẻ tr
                                            for tr in tr_elements:
                                                try:
                                                    # Tìm tất cả các thẻ td trong thẻ tr
                                                    td_elements = WebDriverWait(tr, 10).until(
                                                        EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                                    # tr.find_elements(By.TAG_NAME,'td')
                                                    # Lặp qua danh sách các thẻ td
                                                    text_bienban_mothau_madinhdanh = self.get_value_from_list_by_index(
                                                        td_elements, 0).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             0) is not None else None
                                                    text_bienban_mothau_tennhathau = self.get_value_from_list_by_index(
                                                        td_elements, 1).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             1) is not None else None
                                                    text_bienban_mothau_giaduthau = 0
                                                    text_bienban_mothau_tile_giamgia = 0
                                                    text_bienban_mothau_giaduthau_saugiamgia = 0
                                                    text_bienban_mothau_hieuluc_ehsdt = self.get_value_from_list_by_index(
                                                        td_elements, 2).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             2) is not None else None
                                                    text_bienban_mothau_giatri_dambaoduthau = self.get_value_from_list_by_index(
                                                        td_elements, 3).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             3) is not None else None
                                                    text_bienban_mothau_hieuluc_dambaoduthau = self.get_value_from_list_by_index(
                                                        td_elements, 4).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             4) is not None else None
                                                    text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                        td_elements, 5).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             5) is not None else None

                                                    _var_nhathau_thamgia = {
                                                        'nhathau_thamgia_id': text_bienban_mothau_madinhdanh,
                                                        'matbmt': text_ma_tbmt,
                                                        'madinhdanh': text_bienban_mothau_madinhdanh,
                                                        'tennhathau': text_bienban_mothau_tennhathau,
                                                        'giaduthau': self.convert_money_vnd(text_bienban_mothau_giaduthau),
                                                        'tile_giamgia': self.convert_to_float(
                                                            text_bienban_mothau_tile_giamgia),
                                                        'giaduthau_saugiamgia': self.convert_money_vnd(
                                                            text_bienban_mothau_giaduthau_saugiamgia),
                                                        'hieuluc_ehsdt': text_bienban_mothau_hieuluc_ehsdt,
                                                        'giatri_dambaoduthau': self.convert_money_vnd(
                                                            text_bienban_mothau_giatri_dambaoduthau),
                                                        'hieuluc_dambaoduthau': text_bienban_mothau_hieuluc_dambaoduthau,
                                                        'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                                    }
                                                    _ls_nhathau_thamgia.append(_var_nhathau_thamgia)

                                                except (NoSuchElementException, StaleElementReferenceException, Exception):
                                                    pass
                                    # except NoSuchElementException:
                                    except TimeoutException:
                                        pass

                                    # Kết quả lựa chọn nhà thầu
                                    try:
                                        button_ketqua_lcnt = WebDriverWait(driver, 5).until(
                                            EC.presence_of_element_located(
                                                (By.CSS_SELECTOR, "a[href='#contractorSelectionResults']")))
                                        button_ketqua_lcnt.click()
                                        driver.execute_script('window.scrollBy(0, 1000)')

                                        # Tìm đối tượng div bằng id
                                        # tr_elements = WebDriverWait(driver, 5).until(
                                        #     EC.presence_of_all_elements_located(
                                        #         (By.CSS_SELECTOR, "div#contractorSelectionResults tbody tr")))

                                        tr_elements = WebDriverWait(driver, 5).until(
                                            EC.presence_of_all_elements_located(
                                                (By.CSS_SELECTOR,
                                                 "#contractorSelectionResults > div:nth-child(2) > div.card-body.item-table > table > tbody tr")))
                                        # Tìm tất cả các thẻ tr trong tbody của div

                                        if len(tr_elements) > 0:

                                            # Lặp qua danh sách các thẻ tr
                                            for tr in tr_elements:
                                                try:
                                                    # Tìm tất cả các thẻ td trong thẻ tr
                                                    td_elements = WebDriverWait(tr, 10).until(
                                                        EC.presence_of_all_elements_located((By.TAG_NAME, 'td')))
                                                    # Tab thông tin Nhà thầu trúng thầu có trên 3 cột, nếu có cột liên danh là 8 cột
                                                    text_ketqua_lcnt_madinhdanh = self.get_value_from_list_by_index(
                                                        td_elements,
                                                        1).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             1) is not None else None
                                                    text_ketqua_lcnt_tennhathau = self.get_value_from_list_by_index(
                                                        td_elements,
                                                        2).text \
                                                        if self.get_value_from_list_by_index(td_elements,
                                                                                             2) is not None else None
                                                    if len(td_elements) > 3:
                                                        # Lặp qua danh sách các thẻ td
                                                        text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            3).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 3) is not None else None
                                                        text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            4).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 4) is not None else 0
                                                        text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                            td_elements, 5).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 5) is not None else None
                                                    # Nếu có điểm kỹ thuật, tổng số cột là 8, các cột sẽ là : stt, mã định danh, tên nhà thầu, tên liên danh, giá dự thầu, điểm kỹ thuật, giá trúng thầu, thời gian giao hàng, thời gian giao hàng chi tiết
                                                    if len(td_elements) > 7:
                                                        # Lặp qua danh sách các thẻ td
                                                        text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            3).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 3) is not None else None
                                                        text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            5).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 5) is not None else 0
                                                        text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                            td_elements, 6).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 6) is not None else None
                                                    # Nếu có liên danh, tổng số cột là 9 các cột sẽ là : stt, mã định danh, tên nhà thầu, tên liên danh, giá dự thầu, điểm kỹ thuật, giá trúng thầu, thời gian giao hàng, thời gian giao hàng chi tiết
                                                    if len(td_elements) > 8:
                                                        # Lặp qua danh sách các thẻ td
                                                        text_ketqua_lcnt_giaduthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            4).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 4) is not None else None
                                                        text_ketqua_lcnt_giatrungthau = self.get_value_from_list_by_index(
                                                            td_elements,
                                                            6).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 6) is not None else 0
                                                        text_bienban_mothau_thoigian_giaohang = self.get_value_from_list_by_index(
                                                            td_elements, 7).text \
                                                            if self.get_value_from_list_by_index(td_elements,
                                                                                                 7) is not None else None

                                                    _var_ketqua_dauthau = {
                                                        'ketqua_id': text_ketqua_goithau_id,
                                                        'goithau_id': text_goithau_id,
                                                        'matbmt': text_ma_tbmt,
                                                        'madinhdanh': text_ketqua_lcnt_madinhdanh,
                                                        'tennhathau': text_ketqua_lcnt_tennhathau,
                                                        'giaduthau': self.convert_money_vnd(text_ketqua_lcnt_giaduthau),
                                                        'giatrungthau': self.convert_money_vnd(
                                                            text_ketqua_lcnt_giatrungthau),
                                                        'thoigian_giaohang': text_bienban_mothau_thoigian_giaohang
                                                    }
                                                    _ls_ketqua_dauthau.append(_var_ketqua_dauthau)

                                                except NoSuchElementException:
                                                    pass
                                    except TimeoutException:
                                        pass
                                    time.sleep(1)

                        # time.sleep(5)
                        if len(_ls_goithau) > 0:
                            b_json_data = json.dumps(_ls_goithau)
                            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_goithau(%s,%s,%s);'''
                            self.env.cr.execute(string_sql_insert_json,
                                                (b_json_data, self._uid, b_random_code))
                            self.env.cr.commit()
                            self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                                "%d/%m/%Y %H:%M:%S") + f" [Bot] : Thu thập thành công gói thầu số {index_goithau} : {text_ma_tbmt}")

                        if len(_ls_nhathau_thamgia) > 0:
                            b_json_data = json.dumps(_ls_nhathau_thamgia)
                            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_nhathau_thamgia(%s,%s,%s);'''
                            self.env.cr.execute(string_sql_insert_json,
                                                (b_json_data, self._uid, b_random_code))
                            self.env.cr.commit()

                        if len(_ls_ketqua_dauthau) > 0:
                            b_json_data = json.dumps(_ls_ketqua_dauthau)
                            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_ketqua_dauthau(%s,%s,%s);'''
                            self.env.cr.execute(string_sql_insert_json,
                                                (b_json_data, self._uid, b_random_code))
                            self.env.cr.commit()

                        if len(_ls_sanpham) > 0:
                            b_json_data = json.dumps(_ls_sanpham)
                            string_sql_insert_json = '''SELECT * FROM fn_vnpt_asset_hrm_sanpham(%s,%s,%s);'''
                            self.env.cr.execute(string_sql_insert_json,
                                                (b_json_data, self._uid, b_random_code))
                            self.env.cr.commit()

                        goithauso += 1
                        # # Quay lại trang trước đó
                        driver.back()
                        time.sleep(1)
                    except TimeoutException:
                        continue

            except TimeoutException:
                self.bot_tele_send_log((datetime.now() + timedelta(hours=7)).strftime(
                    "%d/%m/%Y %H:%M:%S") + f" [Bot] : Page time out ... đang tải lại trang gói thầu...")
                driver.navigate().refresh()
                continue

        driver.quit()

        # self.env.cr.execute(
        #     """ update vnpt_muasamcong_cauhinh_crawl set page_index = %s, quantity = %s where type_crawl = 'btn_syns_goithau' """,
        #     (page_index_next, len(_ls_goithau) + cauhinh['quantity']))

        # self.message_post(subject=u'Thông báo',
        #                   body=u"Đồng bộ dữ liệu: '%s' thành công. Tổng số bản ghi sản phẩm trả về: %s" % (
        #                       'Sản phẩm', len(_ls_sanpham)))


class vnpt_muasamcong_sync_api(models.Model):
    _name = 'vnpt_muasamcong_sync_api'
    _rec_name = 'api_name'
    _description = u'Thông tin API'

    api_name = fields.Char(string=u"Tên API", required=True, )
    api_link = fields.Char(string=u"Link API", required=True, )
    api_description = fields.Text(string=u"Mô tả API", required=False, )

    vnpt_muasamcong_sync_id = fields.Many2one(comodel_name="vnpt_muasamcong_sync", string=u"Đồng bộ dữ liệu HRM",
                                              required=False, )


class vnpt_muasamcong_cauhinh_crawl(models.Model):
    _name = 'vnpt_muasamcong_cauhinh_crawl'
    _inherit = ['mail.thread']
    _rec_name = 'type_crawl'
    _description = u'Cấu hình crawl'

    page_index = fields.Integer(string=u"Page index", required=False, )
    step = fields.Integer(string=u"Step", required=False, )
    limit_page_index = fields.Integer(string=u"Limit", required=False, )
    page_size = fields.Integer(string=u"Page size", required=False, )
    type_crawl = fields.Char(string=u"Loại dữ liệu crawl", required=False, )
    quantity = fields.Integer(string=u"Số lượng data đã đồng bộ", required=False, )
