# -*- coding:utf-8 -*-
_author_ = 'VNPT.IT-ERP'

# -*- coding:utf-8 -*-
import os

import requests
import json
from odoo import models, fields, api

import xml.etree.ElementTree as ET

from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

from odoo.tools import OrderedSet, pycompat


class VnptAssetMuaSamCongListGoiThau(models.Model):
    _name = 'vnpt_asset_muasamcong_danhsach_goithau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tengoithau'
    _description = 'Lấy danh sách gói thầu'

    # nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin cơ bản
    goithauid = fields.Char(string="ID gói thầu", required=False, )
    magoithau = fields.Char(string="Mã gói thầu", required=False, )
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    makhlcnt = fields.Char(string="Mã kế hoạch lựa chọn nhà thầu", required=False, )

    tenchudautu = fields.Char(string="Tên chủ đầu tư", required=False, )
    benmoithauid = fields.Many2one('vnpt_asset_muasamcong_danhsach_benmoithau',
                                 string=u'Bên mời thầu')
    mabenmoithau = fields.Char(string="Mã bên mời thầu", required=False, )
    tenbenmoithau = fields.Char(string="Bên mời thầu", required=False, )
    url_goithau = fields.Char(string="URL gói thầu", required=False, )
    tengoithau = fields.Char(string="Tên gói thầu", required=False, )
    tendutoanmuasam = fields.Char(string="Tên dự toán mua sắm", required=False, )

    nguonvon = fields.Char(string="Nguồn vốn", required=False, )
    loaihopdong = fields.Char(string="Loại hợp đồng", required=False, )

    hinhthuclcnt = fields.Char(string="Hình thức lựa chọn nhà thầu", required=False, )
    phuongthuclcnt = fields.Char(string="Phương thức lcnt", required=False, )

    #Địa chỉ trụ sở
    linhvuc = fields.Char(string="Lĩnh vực", required=False, )
    thoigianthuchienhd = fields.Char(string="Thời gian thực hiện hợp đồng", required=False, )
    giagoithau = fields.Float(string="Giá gói thầu", required=False, )
    hinhthucduthau = fields.Char(string="HÌnh thức dự thầu", required=False, )

    #Người đại diện pháp luật
    thoigiannhanhsdt_tungay = fields.Date(string="Thời gian nhận E-HSDT từ ngày", required=False, )
    thoigiannhanhsdt_denngay = fields.Date(string="Thời gian nhận E-HSDT đến ngày", required=False, )
    diadiemphathanhhsmt = fields.Char(string="Địa điểm phát hành E-HSMT", required=False, )
    thoigianhieuluc = fields.Char(string="Thời gian hiệu lực của E-HSDT", required=False, )

    diadiemnhanhsdt = fields.Char(string="Địa điểm nhận HSDT", required=False, )
    diadiemthuchiengoithau = fields.Char(string="Địa điểm thực hiện gói thầu", required=False, )
    thoidiemdongthau = fields.Date(string="Thời điểm đóng thầu", required=False, )
    thoidiemmothau = fields.Date(string="Thời điểm mở thầu", required=False, )

    diadiemmothau = fields.Char(string="Địa điểm mở thầu", required=False, )
    sotiendambaoduthau = fields.Float(string="Số tiền bảo đảm dự thầu", required=False, )
    hinhthucbaodamduthau = fields.Char(string="Hình thức bảo đảm dự thầu", required=False, )

    filehosomoithau = fields.Char(string="File hồ sơ mời thầu", required=False, )

    #Ngành nghề kinh doanh
    thongtinnhathau_thamgias = fields.One2many('vnpt_asset_muasamcong_nhathau_thamgia',
                                     'goithau_id', string=u'Thông tin nhà thầu tham gia gói thầu')

    ngay_dangtai = fields.Date(string="Thời điểm mở thầu", required=False, )
    phanloai_khlcnt = fields.Char(string="Phân loại kế hoạch lựa chọn nhà thầu", required=False, )
    quytrinh_apdung = fields.Char(string="Quy trình áp dụng", required=False, )
    trongnuoc_quocte = fields.Char(string="Trong nước/quốc tế", required=False, )
    chiphi_nop_e_hsdt = fields.Float(string="Chi phí nộp e-HSDT", required=False, )
    loaicongtrinh = fields.Char(string="Loại công trình", required=False, )
    soquyetdinh_pheduyet = fields.Char(string="Loại công trình", required=False, )
    ngaypheduyet = fields.Date(string="Loại công trình", required=False, )
    coquan_banhanh_quyetdinh = fields.Char(string="Cơ quan ban hành quyết định", required=False, )
    quyetdinh_pheduyet = fields.Char(string="Quyết định phê duyệt", required=False, )
    noidung_suadoi_e_hsmt = fields.Char(string="Nội dung sửa đổi e-HSMT", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

#Nhà thầu tham gia gói thầu
class VnptAssetMuaSamCongNhaThauThamGia(models.Model):
    _name = 'vnpt_asset_muasamcong_nhathau_thamgia'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Nhà thầu tham gia gói thầu'
    _order = 'id DESC'
    _rec_name = 'tennhathau'

    goithau_id = fields.Many2one('vnpt_asset_muasamcong_danhsach_goithau',
                                          string=u'Gói thầu')
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    nhathau_thamgia_id = fields.Char(string="Nhà thầu tham gia ID", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    tennhathau = fields.Char(string="Tên nhà thầu", required=False, )
    giaduthau = fields.Float(string="Giá dự thầu", required=False, )
    tile_giamgia = fields.Float(string="Tỉ lệ giảm giá (%)", required=False, )
    giaduthau_saugiamgia = fields.Float(string=u'Giá dự thầu sau giảm giá (nếu có) (VNĐ)')
    hieuluc_ehsdt = fields.Char(string=u'Hiệu lực E-HSDT (ngày)')
    giatri_dambaoduthau = fields.Float(string=u'Giá trị đảm bảo dự thầu (VNĐ)')
    hieuluc_dambaoduthau = fields.Char(string=u'Hiệu lực đảm bảo dự thầu (ngày)')
    thoigian_giaohang = fields.Char(string=u'Thời gian giao hàng')

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

#Thông tin chi tiết kết quả đấu thầu
class VnptAssetMuaSamCongChiTietKetQuaDauThau(models.Model):
    _name = 'vnpt_asset_muasamcong_chitiet_ketqua_dauthau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Chi tiết kết quả đấu thầu'
    _order = 'id DESC'
    _rec_name = 'tennhathau'

    goithau_id = fields.Many2one('vnpt_asset_muasamcong_danhsach_goithau',
                                          string=u'Gói thầu')
    ketqua_id = fields.Char(string="Kết quả ID", required=False, )
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    tennhathau = fields.Char(string="Tên nhà thầu", required=False, )
    giaduthau = fields.Float(string="Giá dự thầu", required=False, )
    giatrungthau = fields.Float(string="Giá trúng thầu", required=False, )
    thoigian_giaohang = fields.Char(string=u'Thời gian giao hàng')

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

#Thông tin sản phẩm gói thầu
class VnptAssetMuaSamCongDanhSachSanPham(models.Model):
    _name = 'vnpt_asset_muasamcong_danhsach_sanpham'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách sản phẩm gói thầu'
    _order = 'id DESC'
    _rec_name = 'sanpham'

    sotbmt = fields.Char(string="Số thông báo mời thầu", required=False, )
    goithauid = fields.Char(string="Gói thầu ID", required=False, )
    sanpham = fields.Char(string="Sản phẩm", required=False, )
    khoiluong = fields.Char(string="Khối lượng", required=False, )
    donvitinh = fields.Char(string="Đơn vị tính", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

#Category
class VnptAssetMuaSamCongDanhSachDanhMuc(models.Model):
    _name = 'vnpt_asset_muasamcong_danhsach_category'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách danh mục category'
    _order = 'id DESC'
    _rec_name = 'name'

    code = fields.Char(string="Mã danh mục", required=False, )
    name = fields.Char(string="Tên danh mục", required=False, )
    name_en = fields.Char(string="Tên tiếng anh", required=False, )
    category_type_code = fields.Char(string="Loại danh mục", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )
