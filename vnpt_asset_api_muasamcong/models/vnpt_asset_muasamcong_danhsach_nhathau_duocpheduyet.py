# -*- coding:utf-8 -*-
_author_ = 'VNPT.IT-ERP'

# -*- coding:utf-8 -*-
import os

import requests
import json
from odoo import models, fields, api

import xml.etree.ElementTree as ET

from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

from odoo.tools import OrderedSet, pycompat


class VnptAssetMuaSamCongListNhaThauDuocPheDuyet(models.Model):
    _name = 'vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tendaydu'
    _description = 'Lấy danh sách nhà thầu được phê duyệt'

    nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin chung
    tendaydu = fields.Char(string="Tên đầy đủ", required=False, )
    tentienganh = fields.Char(string="Tên tiếng anh", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )
    loaihinhphaply = fields.Char(string="Loại hình pháp lý", required=False, )
    masothue = fields.Char(string="Mã số thuế", required=False, )
    ngaycap_nhathau = fields.Date(string="Ngày cấp", required=False, )
    quocgiacap_nhathau = fields.Char(string="Quốc gia cấp", required=False, )

    #Quyết định thành lập
    ngaythanhlap = fields.Date(string="Ngày thành lập", required=False, )
    coquanbanhanh = fields.Char(string="Cơ quan ban hành", required=False, )
    quocgiabanhanh = fields.Char(string="Quốc gia ban hành", required=False, )
    quyetdinhthanhlap = fields.Char(string="Quyết định thành lập", required=False, )

    #Địa chỉ trụ sở
    tinh = fields.Char(string="Tỉnh/thành phố", required=False, )
    quan = fields.Char(string="Quận/Huyện/Thị xã", required=False, )
    xa = fields.Char(string="Phường/Xã/Thị trấn", required=False, )
    sonha = fields.Char(string="Số nhà, đường phố/ Xóm/ Ấp/ Thôn", required=False, )
    web = fields.Char(string="Web", required=False, )

    #Người đại diện pháp luật
    hovaten = fields.Char(string="Họ và tên người đại diện pháp luật", required=False, )
    chucvu = fields.Char(string="Chức vụ người đại diện pháp luật", required=False, )

    #Đăng ký kinh doanh
    sodangky = fields.Char(string="Số đăng ký kinh doanh", required=False, )
    ngaycap_dangky_kinhdoanh = fields.Date(string="Ngày cấp đăng ký kinh doanh", required=False, )
    quocgiacap_dangky_kinhdoanh = fields.Char(string="Quốc gia cấp đăng ký kinh doanh", required=False, )
    giayphepkinhdoanh = fields.Char(string="Giấy phép đăng ký kinh doanh", required=False, )

    #Thông tin hoạt động
    dieulehoatdong = fields.Char(string="Điều lệ hoạt động của DN", required=False, )
    sodotochuc = fields.Char(string="Sơ đồ tổ chức", required=False, )
    sonhanvien = fields.Char(string="Số nhân viên", required=False, )
    linhvucthamgia = fields.Char(string="Lĩnh vực tham gia", required=False, )
    quymodoanhnghiep = fields.Char(string="Quy mô doanh nghiệp", required=False, )

    #Danh sách thành viên góp vốn
    thanhvien_gopvons = fields.One2many('vnpt_asset_muasamcong_thanhvien_gopvon',
                                     'nhathau_pheduyet_id', string=u'Thành viên góp vốn')

    #Ngành nghề kinh doanh
    nganhnghe_kinhdoanhs = fields.One2many('vnpt_asset_muasamcong_nganhnghe_kinhdoanh',
                                     'nhathau_pheduyet_id', string=u'Ngành nghề kinh doanh')



    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

class VnptAssetMuaSamCongThanhVienGopVon(models.Model):
    _name = 'vnpt_asset_muasamcong_thanhvien_gopvon'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Thành viên góp vốn'
    _order = 'id DESC'
    _rec_name = 'hovaten'

    nhathau_pheduyet_id = fields.Many2one('vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet', string=u'Nhà thầu phê duyệt')
    madinhdanh_nhathau = fields.Char(string="Mã định danh", required=False, )
    hovaten = fields.Char(string="Họ và tên", required=False, )
    sogiaychungthuc = fields.Char(string="Số giấy chứng thực", required=False, )
    ngaycap = fields.Date(string="Ngày cấp", required=False, )
    noicap = fields.Char(string=u'Nơi cấp')
    diachithuongtru = fields.Char(string=u'Địa chỉ thường trú')
    quoctich = fields.Char(string=u'Quốc tịch')
    tile = fields.Float(string=u'Tỉ lệ')

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

class VnptAssetMuaSamCongNganhNgheKinhDoanh(models.Model):
    _name = 'vnpt_asset_muasamcong_nganhnghe_kinhdoanh'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Ngành nghề kinh doanh'
    _order = 'id DESC'
    _rec_name = 'tennganh'

    nhathau_pheduyet_id = fields.Many2one('vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet', string=u'Ngành nghề kinh doanh')
    madinhdanh_nhathau = fields.Char(string="Mã định danh", required=False, )
    manganh = fields.Char(string="Mã ngành", required=False, )
    tennganh = fields.Char(string="Tên ngành", required=False, )
    nganhchinh = fields.Boolean(string="Ngành chính", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )
