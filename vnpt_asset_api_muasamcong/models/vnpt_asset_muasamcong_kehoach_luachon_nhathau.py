# -*- coding:utf-8 -*-
_author_ = 'VNPT.IT-ERP'

# -*- coding:utf-8 -*-
import os

import requests
import json
from odoo import models, fields, api

import xml.etree.ElementTree as ET

from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

from odoo.tools import OrderedSet, pycompat


class VnptAssetMuaSamCongKeHoachLuaChonNhaThau(models.Model):
    _name = 'vnpt_asset_muasamcong_kehoach_luachon_nhathau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tenkhlcnt'
    _description = 'Kế hoạch lựa chọn nhà thầu'

    # Thông tin cơ bản
    makhlcnt = fields.Char(string="Mã KHLCNT", required=False, )
    tenkhlcnt = fields.Char(string="Tên KHLCNT", required=False, )
    phienbanthaydoi = fields.Char(string="Phiên bản thay đổi", required=False, )
    trangthaidangtai = fields.Char(string="Trạng thái đăng tải", required=False, )

    tendutoanmuasam = fields.Char(string="Tên dự toán mua sắm", required=False, )
    benmoithau = fields.Char(string="Bên mời thầu", required=False,)
    soluonggoithau = fields.Integer(string="Số lượng gói thầu", required=False, )

    #Thông tin dự toán mua sắm
    dutoanmuasam = fields.Float(string="Dự toán mua sắm", required=False, )
    sotienbangchu = fields.Char(string="Số tiền bằng chữ", required=False, )

    #Thông tin quyết định phê duyệt
    soquyetdinhpheduyet = fields.Char(string="Số quyết định phê duyệt", required=False, )
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )

    coquanbanhanhquyetdinh = fields.Char(string="Cơ quan ban hành quyết định", required=False, )
    quyetdinhpheduyet = fields.Char(string="Quyết định phê duyệt", required=False, )


    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )
