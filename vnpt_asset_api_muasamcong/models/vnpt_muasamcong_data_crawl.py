# -*- coding:utf-8 -*-
_author_ = 'VNPT.IT-ERP'

# -*- coding:utf-8 -*-
import os

import requests
import json
from odoo import models, fields, api

import xml.etree.ElementTree as ET

from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

from odoo.tools import OrderedSet, pycompat


class BidMuaSamCongNhaThauDuocPheDuyet(models.Model):
    _name = 'bid_muasamcong_danhsach_nhathau_duocpheduyet'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tendaydu'
    _description = 'Lấy danh sách nhà thầu được phê duyệt'

    nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin chung
    tendaydu = fields.Char(string="Tên đầy đủ", required=False, )
    tentienganh = fields.Char(string="Tên tiếng anh", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )
    loaihinhphaply = fields.Char(string="Loại hình pháp lý", required=False, )
    masothue = fields.Char(string="Mã số thuế", required=False, )
    ngaycap_nhathau = fields.Date(string="Ngày cấp", required=False, )
    quocgiacap_nhathau = fields.Char(string="Quốc gia cấp", required=False, )

    #Quyết định thành lập
    ngaythanhlap = fields.Date(string="Ngày thành lập", required=False, )
    coquanbanhanh = fields.Char(string="Cơ quan ban hành", required=False, )
    quocgiabanhanh = fields.Char(string="Quốc gia ban hành", required=False, )
    quyetdinhthanhlap = fields.Char(string="Quyết định thành lập", required=False, )

    #Địa chỉ trụ sở
    tinh = fields.Char(string="Tỉnh/thành phố", required=False, )
    quan = fields.Char(string="Quận/Huyện/Thị xã", required=False, )
    xa = fields.Char(string="Phường/Xã/Thị trấn", required=False, )
    sonha = fields.Char(string="Số nhà, đường phố/ Xóm/ Ấp/ Thôn", required=False, )
    web = fields.Char(string="Web", required=False, )

    #Người đại diện pháp luật
    hovaten = fields.Char(string="Họ và tên người đại diện pháp luật", required=False, )
    chucvu = fields.Char(string="Chức vụ người đại diện pháp luật", required=False, )

    #Đăng ký kinh doanh
    sodangky = fields.Char(string="Số đăng ký kinh doanh", required=False, )
    ngaycap_dangky_kinhdoanh = fields.Date(string="Ngày cấp đăng ký kinh doanh", required=False, )
    quocgiacap_dangky_kinhdoanh = fields.Char(string="Quốc gia cấp đăng ký kinh doanh", required=False, )
    giayphepkinhdoanh = fields.Char(string="Giấy phép đăng ký kinh doanh", required=False, )

    #Thông tin hoạt động
    dieulehoatdong = fields.Char(string="Điều lệ hoạt động của DN", required=False, )
    sodotochuc = fields.Char(string="Sơ đồ tổ chức", required=False, )
    sonhanvien = fields.Char(string="Số nhân viên", required=False, )
    linhvucthamgia = fields.Char(string="Lĩnh vực tham gia", required=False, )
    quymodoanhnghiep = fields.Char(string="Quy mô doanh nghiệp", required=False, )

    #Danh sách thành viên góp vốn
    thanhvien_gopvons = fields.One2many('bid_muasamcong_thanhvien_gopvon',
                                     'nhathau_pheduyet_id', string=u'Thành viên góp vốn')

    #Ngành nghề kinh doanh
    nganhnghe_kinhdoanhs = fields.One2many('bid_muasamcong_nganhnghe_kinhdoanh',
                                     'nhathau_pheduyet_id', string=u'Ngành nghề kinh doanh')

class BidMuaSamCongThanhVienGopVon(models.Model):
    _name = 'bid_muasamcong_thanhvien_gopvon'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Thành viên góp vốn'
    _order = 'id DESC'
    _rec_name = 'hovaten'

    nhathau_pheduyet_id = fields.Many2one('bid_muasamcong_danhsach_nhathau_duocpheduyet', string=u'Nhà thầu phê duyệt')
    madinhdanh_nhathau = fields.Char(string="Mã định danh", required=False, )
    hovaten = fields.Char(string="Họ và tên", required=False, )
    sogiaychungthuc = fields.Char(string="Số giấy chứng thực", required=False, )
    ngaycap = fields.Date(string="Ngày cấp", required=False, )
    noicap = fields.Char(string=u'Nơi cấp')
    diachithuongtru = fields.Char(string=u'Địa chỉ thường trú')
    quoctich = fields.Char(string=u'Quốc tịch')
    tile = fields.Float(string=u'Tỉ lệ')

class BidMuaSamCongNganhNgheKinhDoanh(models.Model):
    _name = 'bid_muasamcong_nganhnghe_kinhdoanh'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Ngành nghề kinh doanh'
    _order = 'id DESC'
    _rec_name = 'tennganh'

    nhathau_pheduyet_id = fields.Many2one('bid_muasamcong_danhsach_nhathau_duocpheduyet', string=u'Ngành nghề kinh doanh')
    madinhdanh_nhathau = fields.Char(string="Mã định danh", required=False, )
    manganh = fields.Char(string="Mã ngành", required=False, )
    tennganh = fields.Char(string="Tên ngành", required=False, )
    nganhchinh = fields.Boolean(string="Ngành chính", required=False, )

class BidMuaSamCongListChuDauTuDuocPheDuyet(models.Model):
    _name = 'bid_muasamcong_danhsach_chudautu_duocpheduyet'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tendaydu'
    _description = 'Lấy danh sách chủ đầu tư được phê duyệt'

    nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin chung
    tendaydu = fields.Char(string="Tên đầy đủ", required=False, )
    tentienganh = fields.Char(string="Tên tiếng anh", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    loaihinhphaply = fields.Char(string="Loại hình pháp lý", required=False, )
    masothue = fields.Char(string="Mã số thuế", required=False, )
    ngaycap_chudautu = fields.Date(string="Ngày cấp", required=False, )
    quocgiacap_chudautu = fields.Char(string="Quốc gia cấp", required=False, )

    #Tình trạng hoạt động
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )
    trangthai_vaitro = fields.Char(string="Trạng thái vai trò", required=False, )

    #Cơ quan chủ quản
    tencoquan = fields.Char(string="Tên cơ quan chủ quản", required=False, )
    maquanhe = fields.Char(string="Mã quan hệ ngân sách", required=False, )

    #Địa chỉ trụ sở
    tinh = fields.Char(string="Tỉnh/thành phố", required=False, )
    diachi = fields.Char(string="Địa chỉ", required=False, )
    sdt = fields.Char(string="Số điện thoại", required=False, )
    web = fields.Char(string="Web", required=False, )

    #Người đại diện pháp luật
    hovaten = fields.Char(string="Họ và tên người đại diện pháp luật", required=False, )
    chucvu = fields.Char(string="Chức vụ người đại diện pháp luật", required=False, )
    

#Gói thầu
class BidMuaSamCongMuaSamCongListGoiThau(models.Model):
    _name = 'bid_muasamcong_danhsach_goithau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tengoithau'
    _description = 'Lấy danh sách gói thầu'

    # nhathau_id = fields.Char('ID nhà thầu')

    # Thông tin cơ bản
    goithauid = fields.Char(string="ID gói thầu", required=False, )
    magoithau = fields.Char(string="Mã gói thầu", required=False, )
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    makhlcnt = fields.Char(string="Mã kế hoạch lựa chọn nhà thầu", required=False, )

    tenchudautu = fields.Char(string="Tên chủ đầu tư", required=False, )
    benmoithauid = fields.Many2one('bid_muasamcong_danhsach_benmoithau',
                                 string=u'Bên mời thầu')
    mabenmoithau = fields.Char(string="Mã bên mời thầu", required=False, )
    tenbenmoithau = fields.Char(string="Bên mời thầu", required=False, )
    url_goithau = fields.Char(string="URL gói thầu", required=False, )
    tengoithau = fields.Char(string="Tên gói thầu", required=False, )
    tendutoanmuasam = fields.Char(string="Tên dự toán mua sắm", required=False, )

    nguonvon = fields.Char(string="Nguồn vốn", required=False, )
    loaihopdong = fields.Char(string="Loại hợp đồng", required=False, )

    hinhthuclcnt = fields.Char(string="Hình thức lựa chọn nhà thầu", required=False, )
    phuongthuclcnt = fields.Char(string="Phương thức lcnt", required=False, )

    #Địa chỉ trụ sở
    linhvuc = fields.Char(string="Lĩnh vực", required=False, )
    thoigianthuchienhd = fields.Char(string="Thời gian thực hiện hợp đồng", required=False, )
    giagoithau = fields.Float(string="Giá gói thầu", required=False, )
    hinhthucduthau = fields.Char(string="HÌnh thức dự thầu", required=False, )

    #Người đại diện pháp luật
    thoigiannhanhsdt_tungay = fields.Date(string="Thời gian nhận E-HSDT từ ngày", required=False, )
    thoigiannhanhsdt_denngay = fields.Date(string="Thời gian nhận E-HSDT đến ngày", required=False, )
    diadiemphathanhhsmt = fields.Char(string="Địa điểm phát hành E-HSMT", required=False, )
    thoigianhieuluc = fields.Char(string="Thời gian hiệu lực của E-HSDT", required=False, )

    diadiemnhanhsdt = fields.Char(string="Địa điểm nhận HSDT", required=False, )
    diadiemthuchiengoithau = fields.Char(string="Địa điểm thực hiện gói thầu", required=False, )
    thoidiemdongthau = fields.Date(string="Thời điểm đóng thầu", required=False, )
    thoidiemmothau = fields.Date(string="Thời điểm mở thầu", required=False, )

    diadiemmothau = fields.Char(string="Địa điểm mở thầu", required=False, )
    sotiendambaoduthau = fields.Float(string="Số tiền bảo đảm dự thầu", required=False, )
    hinhthucbaodamduthau = fields.Char(string="Hình thức bảo đảm dự thầu", required=False, )

    filehosomoithau = fields.Char(string="File hồ sơ mời thầu", required=False, )

    #Ngành nghề kinh doanh
    thongtinnhathau_thamgias = fields.One2many('bid_muasamcong_nhathau_thamgia',
                                     'goithau_id', string=u'Thông tin nhà thầu tham gia gói thầu')

    ngay_dangtai = fields.Date(string="Ngày đăng tải", required=False, )
    phanloai_khlcnt = fields.Char(string="Phân loại kế hoạch lựa chọn nhà thầu", required=False, )
    quytrinh_apdung = fields.Char(string="Quy trình áp dụng", required=False, )
    trongnuoc_quocte = fields.Char(string="Trong nước/quốc tế", required=False, )
    chiphi_nop_e_hsdt = fields.Float(string="Chi phí nộp e-HSDT", required=False, )
    loaicongtrinh = fields.Char(string="Loại công trình", required=False, )
    soquyetdinh_pheduyet = fields.Char(string="Số quyết định phê duyệt", required=False, )
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )
    coquan_banhanh_quyetdinh = fields.Char(string="Cơ quan ban hành quyết định", required=False, )
    quyetdinh_pheduyet = fields.Char(string="Quyết định phê duyệt", required=False, )
    noidung_suadoi_e_hsmt = fields.Char(string="Nội dung sửa đổi e-HSMT", required=False, )

#Nhà thầu tham gia gói thầu
class BidMuaSamCongMuaSamCongNhaThauThamGia(models.Model):
    _name = 'bid_muasamcong_nhathau_thamgia'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Nhà thầu tham gia gói thầu'
    _order = 'id DESC'
    _rec_name = 'tennhathau'

    goithau_id = fields.Many2one('vnpt_asset_muasamcong_danhsach_goithau',
                                          string=u'Gói thầu')
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    nhathau_thamgia_id = fields.Char(string="Nhà thầu tham gia ID", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    tennhathau = fields.Char(string="Tên nhà thầu", required=False, )
    giaduthau = fields.Float(string="Giá dự thầu", required=False, )
    tile_giamgia = fields.Float(string="Tỉ lệ giảm giá (%)", required=False, )
    giaduthau_saugiamgia = fields.Float(string=u'Giá dự thầu sau giảm giá (nếu có) (VNĐ)')
    hieuluc_ehsdt = fields.Char(string=u'Hiệu lực E-HSDT (ngày)')
    giatri_dambaoduthau = fields.Float(string=u'Giá trị đảm bảo dự thầu (VNĐ)')
    hieuluc_dambaoduthau = fields.Char(string=u'Hiệu lực đảm bảo dự thầu (ngày)')
    thoigian_giaohang = fields.Char(string=u'Thời gian giao hàng')

#Thông tin chi tiết kết quả đấu thầu
class BidMuaSamCongChiTietKetQuaDauThau(models.Model):
    _name = 'bid_muasamcong_chitiet_ketqua_dauthau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Chi tiết kết quả đấu thầu'
    _order = 'id DESC'
    _rec_name = 'tennhathau'

    goithau_id = fields.Many2one('bid_muasamcong_danhsach_goithau',
                                          string=u'Gói thầu')
    ketqua_id = fields.Char(string="Kết quả ID", required=False, )
    matbmt = fields.Char(string="Mã thông báo mời thầu", required=False, )
    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    tennhathau = fields.Char(string="Tên nhà thầu", required=False, )
    giaduthau = fields.Float(string="Giá dự thầu", required=False, )
    giatrungthau = fields.Float(string="Giá trúng thầu", required=False, )
    thoigian_giaohang = fields.Char(string=u'Thời gian giao hàng')

#Bên mời thầu
class BidMuaSamCongListBenMoiThau(models.Model):
    _name = 'bid_muasamcong_danhsach_benmoithau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tendaydu'
    _description = 'Lấy danh sách bên mời thầu'

    # Thông tin cơ bản
    tendaydu = fields.Char(string="Tên đầy đủ", required=False, )
    tentienganh = fields.Char(string="Tên tiếng anh", required=False, )

    madinhdanh = fields.Char(string="Mã định danh", required=False, )
    loaihinhphaply = fields.Char(string=u'Loại hình pháp lý')
    masothue = fields.Char(string="Mã số thuế", required=False, )
    ngaycap = fields.Date(string="Ngày cấp", required=False, )
    quocgiacap = fields.Char(string="Quốc gia cấp", required=False, )

    #Tình trạng hoạt động
    ngaypheduyet = fields.Date(string="Ngày phê duyệt yêu cầu đăng ký", required=False, )
    trangthaivaitro = fields.Char(string="Trạng thái vai trò", required=False, )

    #Cơ quan chủ quản
    tencoquan = fields.Char(string="Tên cơ quan chủ quản", required=False, )
    maquanhe = fields.Char(string="Mã quan hệ ngân sách", required=False, )

    #Địa chỉ trụ sở
    tinh = fields.Char(string="Tỉnh/ thành phố", required=False, )
    diachi = fields.Char(string="Địa chỉ", required=False, )
    sdt = fields.Char(string="Số điện thoại", required=False, )
    web = fields.Char(string="Web", required=False, )

    #Người đại diện pháp luật
    hovaten = fields.Char(string="Họ và tên", required=False, )
    chucvu = fields.Char(string="Chức vụ", required=False, )


#Kế hoạch lựa chọn nhà thầu
class BidMuaSamCongListKeHoachLuaChonNhaThau(models.Model):
    _name = 'bid_muasamcong_danhsach_kehoach_luachon_nhathau'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'tenkhlcnt'
    _description = 'Lấy danh sách kế hoạch lựa chọn nhà thầu'

    # Thông tin cơ bản
    makhlcnt = fields.Char(string="Mã KHLCNT", required=False, )
    tenkhlcnt = fields.Char(string="Tên KHLCNT", required=False, )
    phienbanthaydoi = fields.Char(string="Phiên bản thay đổi", required=False, )
    trangthaidangtai = fields.Char(string="Trạng thái đăng tải", required=False, )

    tendutoanmuasam = fields.Char(string="Tên dự toán mua sắm", required=False, )
    benmoithau = fields.Char(string="Bên mời thầu", required=False,)
    soluonggoithau = fields.Integer(string="Số lượng gói thầu", required=False, )

    #Thông tin dự toán mua sắm
    dutoanmuasam = fields.Float(string="Dự toán mua sắm", required=False, )
    sotienbangchu = fields.Char(string="Số tiền bằng chữ", required=False, )

    #Thông tin quyết định phê duyệt
    soquyetdinhpheduyet = fields.Char(string="Số quyết định phê duyệt", required=False, )
    ngaypheduyet = fields.Date(string="Ngày phê duyệt", required=False, )

    coquanbanhanhquyetdinh = fields.Char(string="Cơ quan ban hành quyết định", required=False, )
    quyetdinhpheduyet = fields.Char(string="Quyết định phê duyệt", required=False, )

# Thông tin sản phẩm gói thầu
class BidMuaSamCongDanhSachSanPham(models.Model):
    _name = 'bid_muasamcong_danhsach_sanpham'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách sản phẩm gói thầu'
    _order = 'id DESC'
    _rec_name = 'sanpham'

    sotbmt = fields.Char(string="Số thông báo mời thầu", required=False, )
    goithauid = fields.Char(string="Gói thầu ID", required=False, )
    sanpham = fields.Char(string="Sản phẩm", required=False, )
    khoiluong = fields.Char(string="Khối lượng", required=False, )
    donvitinh = fields.Char(string="Đơn vị tính", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )

class BidMuaSamCongMuaSamCongDanhSachSanPham(models.Model):
    _name = 'bid_muasamcong_muasamcong_danhsach_sanpham'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách sản phẩm gói thầu'
    _order = 'id DESC'
    _rec_name = 'sanpham'

    sotbmt = fields.Char(string="Số thông báo mời thầu", required=False, )
    goithauid = fields.Char(string="Gói thầu ID", required=False, )
    sanpham = fields.Char(string="Sản phẩm", required=False, )
    khoiluong = fields.Char(string="Khối lượng", required=False, )
    donvitinh = fields.Char(string="Đơn vị tính", required=False, )

    random_code = fields.Char(string="Code đồng bộ", required=False, index=True)

    time_sync_create = fields.Datetime(string="TG tạo mới", required=False, )
    time_sync_write = fields.Datetime(string="TG cập nhật", required=False, )
    is_active = fields.Boolean(string="Trạng thái", default=False, index=True)
    sync_error = fields.Text(string="Thông báo", required=False, )
    sync_status = fields.Selection(string="Trạng thái",
                                   selection=[('fail', 'Lỗi'), ('none', 'Chờ validate'), ('pass', 'Thỏa mãn'),
                                              ('sync_qlts', 'Cập nhật thông tin trên QLTS')],
                                   default='none',
                                   required=False, )


#Category
class BidMuaSamCongDanhSachDanhMuc(models.Model):
    _name = 'bid_muasamcong_danhsach_category'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách danh mục category'
    _order = 'id DESC'
    _rec_name = 'name'

    code = fields.Char(string="Mã danh mục", required=False, )
    name = fields.Char(string="Tên danh mục", required=False, )
    name_en = fields.Char(string="Tên tiếng anh", required=False, )
    category_type_code = fields.Char(string="Loại danh mục", required=False, )

#Từ khóa công nghệ thông tin
class BidMuaSamCongDanhSachTuKhoaCntt(models.Model):
    _name = 'bid_muasamcong_danhsach_tukhoa_cntt'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách danh mục từ khóa cntt'
    _order = 'id DESC'
    _rec_name = 'name'

    code = fields.Char(string="Mã từ khóa", required=False, )
    name = fields.Char(string="Tên từ khóa", required=False, )
    type_tukhoa = fields.Char(string="Kiểu từ khóa", required=False, )

#Bảng lưu list số thông báo mời thầu
class BidMuaSamCongDanhSachSoTBMT(models.Model):
    _name = 'bid_muasamcong_danhsach_so_tbmt'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Danh sách danh mục số thông báo mời thầu'
    _order = 'id DESC'
    _rec_name = 'type_tukhoa'

    key = fields.Char(string="Mã từ khóa", required=False, )
    type_tukhoa = fields.Char(string="Kiểu từ khóa", required=False, )
