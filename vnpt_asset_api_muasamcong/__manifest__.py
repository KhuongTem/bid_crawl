# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Vnpt Asset API CRAWL BID',
    'version': '1.0.0',
    'summary': 'Quản lý API CRAWL BID',
    "depends": ['mail'],
    # 'depends': [' 'mail''],
    'description': """
        Quản lý API CRAWL BID
    """,
    'author': 'VNPT-IT ERP',
    'website': 'https://vnptit.vn/',
    'category': 'vnpt_asset',
    'sequence': 32,
    'data': [
        'views/vnpt_muasamcong_sync.xml',
        'views/vnpt_asset_muasamcong_list_nhathau_duocpheduyet.xml',
        'views/vnpt_asset_muasamcong_list_chudautu_duocpheduyet.xml',
        'views/vnpt_asset_muasamcong_list_kehoach_luachon_nhathau.xml',
        'views/vnpt_asset_muasamcong_list_benmoithau.xml',
        'views/vnpt_asset_muasamcong_list_goithau.xml',
        'views/vnpt_asset_muasamcong_list_danhmuc.xml',
        'views/vnpt_asset_muasamcong_list_tukhoa_cntt.xml',
        'security/vnpt_asset_api_muasamcong_group.xml',
        'data/xml/vnpt_asset_api_cron_job.xml',
        'security/ir.model.access.csv',
        'menuitem.xml',
    ],
    # "images": ['static/description/icon.png'],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
