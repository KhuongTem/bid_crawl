DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_sync_nganhnghe_kinhdoanh(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_sync_nganhnghe_kinhdoanh
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_nganhnghe_kinhdoanh where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"madinhdanh_nhathau" TEXT,
				"manganh" TEXT,
				"tennganh" TEXT,
				"nganhchinh" BOOLEAN
			)
	)
	Insert into vnpt_asset_muasamcong_nganhnghe_kinhdoanh(
	            madinhdanh_nhathau,
				manganh,
				tennganh,
				nganhchinh,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select
		        madinhdanh_nhathau,
				manganh,
				tennganh,
				nganhchinh,b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  madinhdanh_nhathau,
				manganh,
				tennganh,
				nganhchinh
        from vnpt_asset_muasamcong_nganhnghe_kinhdoanh tb1
        where tb1.random_code = b_random_code
        and tb1.madinhdanh_nhathau not in (select tb2.madinhdanh_nhathau from bid_muasamcong_nganhnghe_kinhdoanh tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_nganhnghe_kinhdoanh (
                madinhdanh_nhathau,
				manganh,
				tennganh,
				nganhchinh,create_date)
                 Select
                    madinhdanh_nhathau,
                    manganh,
                    tennganh,
                    nganhchinh,now()
                 from data_update
     )
    UPDATE bid_muasamcong_nganhnghe_kinhdoanh temp_up
    SET
        madinhdanh_nhathau = temp_data.madinhdanh_nhathau,
        manganh = temp_data.manganh,
        tennganh = temp_data.tennganh,
        nganhchinh = temp_data.nganhchinh
    FROM vnpt_asset_muasamcong_nganhnghe_kinhdoanh temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.madinhdanh_nhathau = temp_up.madinhdanh_nhathau;

-------------------------------------------------
END;
$$;