DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_chudautu_duocpheduyet(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_chudautu_duocpheduyet
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_danhsach_chudautu_duocpheduyet where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"tendaydu" TEXT,
				"tentienganh" TEXT,
				"madinhdanh" TEXT,
				"loaihinhphaply" TEXT,
				"masothue" TEXT,
				"ngaycap_chudautu" DATE ,
				"quocgiacap_chudautu" TEXT,
				"ngaypheduyet" DATE ,
				"trangthai_vaitro" TEXT,
				"tencoquan" TEXT,
				"maquanhe" TEXT,
				"tinh" TEXT,
				"diachi" TEXT,
				"sdt" TEXT,
				"web" TEXT,
				"hovaten" TEXT,
				"chucvu" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_danhsach_chudautu_duocpheduyet(
	            tendaydu,
				tentienganh,
				madinhdanh,
				loaihinhphaply,
				masothue,
				ngaycap_chudautu,
				quocgiacap_chudautu,
				ngaypheduyet,
				trangthai_vaitro,
				tencoquan,
				maquanhe,
				tinh,
				diachi,
				sdt,
				web,
				hovaten,
				chucvu,random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		         tendaydu,
				tentienganh,
				madinhdanh,
				loaihinhphaply,
				masothue,
				ngaycap_chudautu,
				quocgiacap_chudautu,
				ngaypheduyet,
				trangthai_vaitro,
				tencoquan,
				maquanhe,
				tinh,
				diachi,
				sdt,
				web,
				hovaten,
				chucvu, b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select   tendaydu,
				tentienganh,
				madinhdanh,
				loaihinhphaply,
				masothue,
				ngaycap_chudautu,
				quocgiacap_chudautu,
				ngaypheduyet,
				trangthai_vaitro,
				tencoquan,
				maquanhe,
				tinh,
				diachi,
				sdt,
				web,
				hovaten,
				chucvu
        from vnpt_asset_muasamcong_danhsach_chudautu_duocpheduyet tb1
        where tb1.random_code = b_random_code
        and tb1.madinhdanh not in (select tb2.madinhdanh from bid_muasamcong_danhsach_chudautu_duocpheduyet tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_danhsach_chudautu_duocpheduyet ( tendaydu,
				tentienganh,
				madinhdanh,
				loaihinhphaply,
				masothue,
				ngaycap_chudautu,
				quocgiacap_chudautu,
				ngaypheduyet,
				trangthai_vaitro,
				tencoquan,
				maquanhe,
				tinh,
				diachi,
				sdt,
				web,
				hovaten,
				chucvu,
				create_date)
                 Select
                    tendaydu,
                    tentienganh,
                    madinhdanh,
                    loaihinhphaply,
                    masothue,
                    ngaycap_chudautu,
                    quocgiacap_chudautu,
                    ngaypheduyet,
                    trangthai_vaitro,
                    tencoquan,
                    maquanhe,
                    tinh,
                    diachi,
                    sdt,
                    web,
                    hovaten,
                    chucvu,now()
                 from data_update
     )
    UPDATE bid_muasamcong_danhsach_chudautu_duocpheduyet temp_up
    SET
        tendaydu = temp_data.tendaydu,
        tentienganh = temp_data.tentienganh,
        madinhdanh = temp_data.madinhdanh,
        loaihinhphaply = temp_data.loaihinhphaply,
        masothue = temp_data.masothue,
        ngaycap_chudautu = temp_data.ngaycap_chudautu,
        quocgiacap_chudautu = temp_data.quocgiacap_chudautu,
        ngaypheduyet = temp_data.ngaypheduyet,
        trangthai_vaitro = temp_data.trangthai_vaitro,
        tencoquan = temp_data.tencoquan,
        maquanhe = temp_data.maquanhe,
        tinh = temp_data.tinh,
        diachi = temp_data.diachi,
        sdt = temp_data.sdt,
        web = temp_data.web,
        hovaten = temp_data.hovaten,
        chucvu = temp_data.chucvu

    FROM vnpt_asset_muasamcong_danhsach_chudautu_duocpheduyet temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.madinhdanh = temp_up.madinhdanh;

-------------------------------------------------
END;
$$;