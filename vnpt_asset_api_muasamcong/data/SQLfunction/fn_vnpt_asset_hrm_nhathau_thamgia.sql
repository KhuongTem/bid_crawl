DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_nhathau_thamgia(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_nhathau_thamgia
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_nhathau_thamgia where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
			    "nhathau_thamgia_id" TEXT,
				"matbmt" TEXT,
				"madinhdanh" TEXT,
				"tennhathau" TEXT,
				"giaduthau" FLOAT,
				"tile_giamgia" FLOAT,
				"giaduthau_saugiamgia" FLOAT,
				"hieuluc_ehsdt" TEXT ,
				"giatri_dambaoduthau" FLOAT,
				"hieuluc_dambaoduthau" TEXT,
				"thoigian_giaohang" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_nhathau_thamgia(
	            nhathau_thamgia_id,
				matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				tile_giamgia,
				giaduthau_saugiamgia,
				hieuluc_ehsdt,
				giatri_dambaoduthau,
				hieuluc_dambaoduthau,
				thoigian_giaohang,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select
	            nhathau_thamgia_id,
				matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				tile_giamgia,
				giaduthau_saugiamgia,
				hieuluc_ehsdt,
				giatri_dambaoduthau,
				hieuluc_dambaoduthau,
				thoigian_giaohang,
				b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
    Select  	nhathau_thamgia_id,
				matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				tile_giamgia,
				giaduthau_saugiamgia,
				hieuluc_ehsdt,
				giatri_dambaoduthau,
				hieuluc_dambaoduthau,
				thoigian_giaohang
        from vnpt_asset_muasamcong_nhathau_thamgia tb1
        where tb1.random_code = b_random_code
        and tb1.nhathau_thamgia_id not in (select tb2.nhathau_thamgia_id from bid_muasamcong_nhathau_thamgia tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_nhathau_thamgia (nhathau_thamgia_id,
				matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				tile_giamgia,
				giaduthau_saugiamgia,
				hieuluc_ehsdt,
				giatri_dambaoduthau,
				hieuluc_dambaoduthau,
				thoigian_giaohang,create_date)
                 Select
                  nhathau_thamgia_id,
				matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				tile_giamgia,
				giaduthau_saugiamgia,
				hieuluc_ehsdt,
				giatri_dambaoduthau,
				hieuluc_dambaoduthau,
				thoigian_giaohang,now()
                 from data_update
     )
    UPDATE bid_muasamcong_nhathau_thamgia temp_up
    SET
        nhathau_thamgia_id = temp_data.nhathau_thamgia_id,
        matbmt = temp_data.matbmt,
        madinhdanh = temp_data.madinhdanh,
        tennhathau = temp_data.tennhathau,
        giaduthau = temp_data.giaduthau,
        tile_giamgia = temp_data.tile_giamgia,
        giaduthau_saugiamgia = temp_data.giaduthau_saugiamgia,
        hieuluc_ehsdt = temp_data.hieuluc_ehsdt,
        giatri_dambaoduthau = temp_data.giatri_dambaoduthau,
        hieuluc_dambaoduthau = temp_data.hieuluc_dambaoduthau,
        thoigian_giaohang = temp_data.thoigian_giaohang

    FROM vnpt_asset_muasamcong_nhathau_thamgia temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.nhathau_thamgia_id = temp_up.nhathau_thamgia_id;

-------------------------------------------------
END;
$$;