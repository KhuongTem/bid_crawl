DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_kehoach_luachon_nhathau(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_kehoach_luachon_nhathau
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_kehoach_luachon_nhathau where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"makhlcnt" TEXT,
				"tenkhlcnt" TEXT,
				"phienbanthaydoi" TEXT,
				"trangthaidangtai" TEXT,
				"tendutoanmuasam" TEXT,
				"benmoithau" TEXT ,
				"soluonggoithau" INTEGER,
				"dutoanmuasam" FLOAT,
				"sotienbangchu" TEXT,
				"soquyetdinhpheduyet" TEXT,
				"ngaypheduyet" DATE ,
				"coquanbanhanhquyetdinh" TEXT,
				"quyetdinhpheduyet" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_kehoach_luachon_nhathau(
	            makhlcnt,
				tenkhlcnt,
				phienbanthaydoi,
				trangthaidangtai,
				tendutoanmuasam,
				benmoithau,
				soluonggoithau,
				dutoanmuasam,
				sotienbangchu,
				soquyetdinhpheduyet,
				ngaypheduyet,
				coquanbanhanhquyetdinh,
				quyetdinhpheduyet,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		        makhlcnt,
				tenkhlcnt,
				phienbanthaydoi,
				trangthaidangtai,
				tendutoanmuasam,
				benmoithau,
				soluonggoithau,
				dutoanmuasam,
				sotienbangchu,
				soquyetdinhpheduyet,
				ngaypheduyet,
				coquanbanhanhquyetdinh,
				quyetdinhpheduyet,
				b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  makhlcnt,
				tenkhlcnt,
				phienbanthaydoi,
				trangthaidangtai,
				tendutoanmuasam,
				benmoithau,
				soluonggoithau,
				dutoanmuasam,
				sotienbangchu,
				soquyetdinhpheduyet,
				ngaypheduyet,
				coquanbanhanhquyetdinh,
				quyetdinhpheduyet
        from vnpt_asset_muasamcong_kehoach_luachon_nhathau tb1
        where tb1.random_code = b_random_code
        and tb1.makhlcnt not in (select tb2.makhlcnt from bid_muasamcong_danhsach_kehoach_luachon_nhathau tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_danhsach_kehoach_luachon_nhathau (makhlcnt,
				tenkhlcnt,
				phienbanthaydoi,
				trangthaidangtai,
				tendutoanmuasam,
				benmoithau,
				soluonggoithau,
				dutoanmuasam,
				sotienbangchu,
				soquyetdinhpheduyet,
				ngaypheduyet,
				coquanbanhanhquyetdinh,
				quyetdinhpheduyet,create_date)
                 Select
                     makhlcnt,
                    tenkhlcnt,
                    phienbanthaydoi,
                    trangthaidangtai,
                    tendutoanmuasam,
                    benmoithau,
                    soluonggoithau,
                    dutoanmuasam,
                    sotienbangchu,
                    soquyetdinhpheduyet,
                    ngaypheduyet,
                    coquanbanhanhquyetdinh,
                    quyetdinhpheduyet,now()
                 from data_update
     )
    UPDATE bid_muasamcong_danhsach_kehoach_luachon_nhathau temp_up
    SET
        makhlcnt = temp_data.makhlcnt,
        tenkhlcnt = temp_data.tenkhlcnt,
        phienbanthaydoi = temp_data.phienbanthaydoi,
        trangthaidangtai = temp_data.trangthaidangtai,
        tendutoanmuasam = temp_data.tendutoanmuasam,
        benmoithau = temp_data.benmoithau,
        soluonggoithau = temp_data.soluonggoithau,
        dutoanmuasam = temp_data.dutoanmuasam,
        sotienbangchu = temp_data.sotienbangchu,
        soquyetdinhpheduyet = temp_data.soquyetdinhpheduyet,
        ngaypheduyet = temp_data.ngaypheduyet,
        coquanbanhanhquyetdinh = temp_data.coquanbanhanhquyetdinh,
        quyetdinhpheduyet = temp_data.quyetdinhpheduyet

    FROM vnpt_asset_muasamcong_kehoach_luachon_nhathau temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.makhlcnt = temp_up.makhlcnt;

-------------------------------------------------
END;
$$;