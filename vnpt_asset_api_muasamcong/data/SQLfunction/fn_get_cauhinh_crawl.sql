DROP FUNCTION IF EXISTS fn_get_cauhinh_crawl(varchar);
CREATE OR REPLACE FUNCTION fn_get_cauhinh_crawl(type_cauhinh varchar)
returns table("json_cauhinh_crawl" json)
as $BODY$
    DECLARE
        p_count INTEGER;
BEGIN
--   Update page_index về 0 nếu vượt quá limit page cron job
     select count(*) into p_count from vnpt_muasamcong_cauhinh_crawl where type_crawl = type_cauhinh;
     if p_count <= 0 then
         if type_cauhinh = 'btn_syns_nhathau_duocpheduyet' then
            insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,7550,1000,20,'btn_syns_nhathau_duocpheduyet',0);
         elseif type_cauhinh = 'btn_syns_chudautu_duocpheduyet' then
            insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,10000,1000,20,'btn_syns_chudautu_duocpheduyet',0);
         elseif type_cauhinh = 'btn_syns_benmoithau' then
            insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,10000,500,20,'btn_syns_benmoithau',0);
         elseif type_cauhinh = 'btn_syns_goithau' then
            insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,14,1,10000,'btn_syns_goithau',0);
         elseif type_cauhinh = 'btn_syns_kehoach_luachon_nhathau' then
            insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,10,1,1000,'btn_syns_kehoach_luachon_nhathau',0);
         else
             insert into vnpt_muasamcong_cauhinh_crawl(page_index,limit_page_index,step,page_size,type_crawl,quantity) values (0,10000,500,20,'',0);
         end if;
     end if;
     update vnpt_muasamcong_cauhinh_crawl set page_index = 0, quantity = 0
                   where type_crawl = type_cauhinh and page_index >= limit_page_index;
return query
with abc as (
    SELECT  * from vnpt_muasamcong_cauhinh_crawl where type_crawl = type_cauhinh
        )
    SELECT array_to_json(array_agg(abc), FALSE) AS ok_json FROM abc;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;