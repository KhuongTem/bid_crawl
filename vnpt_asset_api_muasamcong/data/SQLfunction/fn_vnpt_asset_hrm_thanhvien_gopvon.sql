DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_sync_thanhvien_gopvon(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_sync_thanhvien_gopvon
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_thanhvien_gopvon where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"madinhdanh_nhathau" TEXT,
				"hovaten" TEXT,
				"sogiaychungthuc" TEXT,
				"ngaycap" DATE ,
				"noicap" TEXT,
				"diachithuongtru" TEXT,
				"quoctich" TEXT,
				"tile" FLOAT
			)
	)
	Insert into vnpt_asset_muasamcong_thanhvien_gopvon(
	            madinhdanh_nhathau,
				hovaten,
				sogiaychungthuc,
				ngaycap,
				noicap,
				diachithuongtru,
				quoctich,
				tile,random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		        madinhdanh_nhathau,
				hovaten,
				sogiaychungthuc,
				ngaycap,
				noicap,
				diachithuongtru,
				quoctich,
				tile, b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  madinhdanh_nhathau,
				hovaten,
				sogiaychungthuc,
				ngaycap,
				noicap,
				diachithuongtru,
				quoctich,
				tile
        from vnpt_asset_muasamcong_thanhvien_gopvon tb1
        where tb1.random_code = b_random_code
        and tb1.madinhdanh_nhathau not in (select tb2.madinhdanh_nhathau from bid_muasamcong_thanhvien_gopvon tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_thanhvien_gopvon (
                madinhdanh_nhathau,
                hovaten,
                sogiaychungthuc,
                ngaycap,
                noicap,
                diachithuongtru,
                quoctich,
                tile,create_date)
             Select
                madinhdanh_nhathau,
                hovaten,
                sogiaychungthuc,
                ngaycap,
                noicap,
                diachithuongtru,
                quoctich,
                tile,now()
             from data_update
     )
    UPDATE bid_muasamcong_thanhvien_gopvon temp_up
    SET
        madinhdanh_nhathau = temp_data.madinhdanh_nhathau,
        hovaten = temp_data.hovaten,
        sogiaychungthuc = temp_data.sogiaychungthuc,
        ngaycap = temp_data.ngaycap,
        noicap = temp_data.noicap,
        diachithuongtru = temp_data.diachithuongtru,
        quoctich = temp_data.quoctich,
        tile = temp_data.tile
    FROM vnpt_asset_muasamcong_thanhvien_gopvon temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.madinhdanh_nhathau = temp_up.madinhdanh_nhathau;

-------------------------------------------------
END;
$$;