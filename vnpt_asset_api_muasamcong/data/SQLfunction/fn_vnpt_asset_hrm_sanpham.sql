DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_sanpham(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_sanpham
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_danhsach_sanpham where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"goithauid" TEXT,
				"sanpham" TEXT,
				"khoiluong" TEXT,
				"donvitinh" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_danhsach_sanpham(
	            goithauid,
				sanpham,
				khoiluong,
				donvitinh,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		        goithauid,
				sanpham,
				khoiluong,
				donvitinh, b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  goithauid,
				sanpham,
				khoiluong,
				donvitinh
        from vnpt_asset_muasamcong_danhsach_sanpham tb1
        where tb1.random_code = b_random_code
        and tb1.goithauid not in (select tb2.goithauid from bid_muasamcong_danhsach_sanpham tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_danhsach_sanpham ( goithauid,
				sanpham,
				khoiluong,
				donvitinh,create_date)
                 Select
                    goithauid,
                    sanpham,
                    khoiluong,
                    donvitinh,now()
                 from data_update
     )
    UPDATE bid_muasamcong_danhsach_sanpham temp_up
    SET
        goithauid = temp_data.goithauid,
        sanpham = temp_data.sanpham,
        khoiluong = temp_data.khoiluong,
        donvitinh = temp_data.donvitinh
    FROM vnpt_asset_muasamcong_danhsach_sanpham temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.goithauid = temp_up.goithauid;

-------------------------------------------------
END;
$$;