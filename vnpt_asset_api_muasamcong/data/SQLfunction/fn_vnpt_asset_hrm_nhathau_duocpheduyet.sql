DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_sync_nhathau_duocpheduyet(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_sync_nhathau_duocpheduyet
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"nhathau_id" TEXT,
				"tendaydu" TEXT,
				"tentienganh" TEXT,
				"madinhdanh" TEXT,
				"ngaypheduyet" DATE,
				"loaihinhphaply" TEXT,
				"masothue" TEXT,
				"ngaycap_nhathau" DATE ,
				"quocgiacap_nhathau" TEXT,
				"ngaythanhlap" DATE ,
				"coquanbanhanh" TEXT,
				"quocgiabanhanh" TEXT,
				"quyetdinhthanhlap" TEXT,
				"tinh" TEXT,
				"quan" TEXT,
				"xa" TEXT,
				"sonha" TEXT,
				"web" TEXT,
				"hovaten" TEXT,
				"chucvu" TEXT,
				"sodangky" TEXT,
				"ngaycap_dangky_kinhdoanh" DATE ,
				"quocgiacap_dangky_kinhdoanh" TEXT,
				"giayphepkinhdoanh" TEXT,
				"dieulehoatdong" TEXT,
				"sodotochuc" TEXT,
				"sonhanvien" TEXT,
				"linhvucthamgia" TEXT,
				"quymodoanhnghiep" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet(
	            nhathau_id,
				tendaydu,
				tentienganh,
				madinhdanh,
				ngaypheduyet,
				loaihinhphaply,
				masothue,
				ngaycap_nhathau,
				quocgiacap_nhathau,
				ngaythanhlap,
				coquanbanhanh,
				quocgiabanhanh,
				quyetdinhthanhlap,
				tinh,
				quan,
				xa,
				sonha,
				web,
				hovaten,
				chucvu,
				sodangky,
				ngaycap_dangky_kinhdoanh,
				quocgiacap_dangky_kinhdoanh,
				giayphepkinhdoanh,
				dieulehoatdong,
				sodotochuc,
				sonhanvien,
				linhvucthamgia,
				quymodoanhnghiep,random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		        nhathau_id,
				tendaydu,
				tentienganh,
				madinhdanh,
				ngaypheduyet,
				loaihinhphaply,
				masothue,
				ngaycap_nhathau,
				quocgiacap_nhathau,
				ngaythanhlap,
				coquanbanhanh,
				quocgiabanhanh,
				quyetdinhthanhlap,
				tinh,
				quan,
				xa,
				sonha,
				web,
				hovaten,
				chucvu,
				sodangky,
				ngaycap_dangky_kinhdoanh,
				quocgiacap_dangky_kinhdoanh,
				giayphepkinhdoanh,
				dieulehoatdong,
				sodotochuc,
				sonhanvien,
				linhvucthamgia,
				quymodoanhnghiep, b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  nhathau_id,
				tendaydu,
				tentienganh,
				madinhdanh,
				ngaypheduyet,
				loaihinhphaply,
				masothue,
				ngaycap_nhathau,
				quocgiacap_nhathau,
				ngaythanhlap,
				coquanbanhanh,
				quocgiabanhanh,
				quyetdinhthanhlap,
				tinh,
				quan,
				xa,
				sonha,
				web,
				hovaten,
				chucvu,
				sodangky,
				ngaycap_dangky_kinhdoanh,
				quocgiacap_dangky_kinhdoanh,
				giayphepkinhdoanh,
				dieulehoatdong,
				sodotochuc,
				sonhanvien,
				linhvucthamgia,
				quymodoanhnghiep
        from vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet tb1
        where tb1.random_code = b_random_code
        and tb1.madinhdanh not in (select tb2.madinhdanh from bid_muasamcong_danhsach_nhathau_duocpheduyet tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_danhsach_nhathau_duocpheduyet (nhathau_id,
				tendaydu,
				tentienganh,
				madinhdanh,
				ngaypheduyet,
				loaihinhphaply,
				masothue,
				ngaycap_nhathau,
				quocgiacap_nhathau,
				ngaythanhlap,
				coquanbanhanh,
				quocgiabanhanh,
				quyetdinhthanhlap,
				tinh,
				quan,
				xa,
				sonha,
				web,
				hovaten,
				chucvu,
				sodangky,
				ngaycap_dangky_kinhdoanh,
				quocgiacap_dangky_kinhdoanh,
				giayphepkinhdoanh,
				dieulehoatdong,
				sodotochuc,
				sonhanvien,
				linhvucthamgia,
				quymodoanhnghiep,create_date)
                 Select
                    nhathau_id,
                    tendaydu,
                    tentienganh,
                    madinhdanh,
                    ngaypheduyet,
                    loaihinhphaply,
                    masothue,
                    ngaycap_nhathau,
                    quocgiacap_nhathau,
                    ngaythanhlap,
                    coquanbanhanh,
                    quocgiabanhanh,
                    quyetdinhthanhlap,
                    tinh,
                    quan,
                    xa,
                    sonha,
                    web,
                    hovaten,
                    chucvu,
                    sodangky,
                    ngaycap_dangky_kinhdoanh,
                    quocgiacap_dangky_kinhdoanh,
                    giayphepkinhdoanh,
                    dieulehoatdong,
                    sodotochuc,
                    sonhanvien,
                    linhvucthamgia,
                    quymodoanhnghiep,now()
                 from data_update
     )
    UPDATE bid_muasamcong_danhsach_nhathau_duocpheduyet temp_up
    SET
        nhathau_id = temp_data.nhathau_id,
        tendaydu = temp_data.tendaydu,
        tentienganh = temp_data.tentienganh,
        madinhdanh = temp_data.madinhdanh,
        ngaypheduyet = temp_data.ngaypheduyet,
        loaihinhphaply = temp_data.loaihinhphaply,
        masothue = temp_data.masothue,
        ngaycap_nhathau = temp_data.ngaycap_nhathau,
        quocgiacap_nhathau = temp_data.quocgiacap_nhathau,
        ngaythanhlap = temp_data.ngaythanhlap,
        coquanbanhanh = temp_data.coquanbanhanh,
        quocgiabanhanh = temp_data.quocgiabanhanh,
        quyetdinhthanhlap = temp_data.quyetdinhthanhlap,
        tinh = temp_data.tinh,
        quan = temp_data.quan,
        xa = temp_data.xa,
        sonha = temp_data.sonha,
        web = temp_data.web,
        hovaten = temp_data.hovaten,
        chucvu = temp_data.chucvu,
        sodangky = temp_data.sodangky,
        ngaycap_dangky_kinhdoanh = temp_data.ngaycap_dangky_kinhdoanh,
        quocgiacap_dangky_kinhdoanh = temp_data.quocgiacap_dangky_kinhdoanh,
        giayphepkinhdoanh = temp_data.giayphepkinhdoanh,
        dieulehoatdong = temp_data.dieulehoatdong,
        sodotochuc = temp_data.sodotochuc,
        sonhanvien = temp_data.sonhanvien,
        linhvucthamgia = temp_data.linhvucthamgia,
        quymodoanhnghiep = temp_data.quymodoanhnghiep

    FROM vnpt_asset_muasamcong_danhsach_nhathau_duocpheduyet temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.madinhdanh = temp_up.madinhdanh;

-------------------------------------------------
END;
$$;