DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_danhmuc(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_danhmuc
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_danhsach_category where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/	
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
				"code" TEXT,
				"name" TEXT,
				"name_en" TEXT,
				"category_type_code" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_danhsach_category(
	            code,
				name,
				name_en,
				category_type_code,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select 
		        code,
				name,
				name_en,
				category_type_code,b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
        Select  code,
				name,
				name_en,
				category_type_code
        from vnpt_asset_muasamcong_danhsach_category tb1
        where tb1.random_code = b_random_code
        and tb1.code not in (select tb2.code from bid_muasamcong_danhsach_category tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_danhsach_category ( code,
				name,
				name_en,
				category_type_code)
                 Select
                    code,
                    name,
                    name_en,
                    category_type_code
                 from data_update
     )
    UPDATE bid_muasamcong_danhsach_category temp_up
    SET
        code = temp_data.code,
        name = temp_data.name,
        name_en = temp_data.name_en,
        category_type_code = temp_data.category_type_code

    FROM vnpt_asset_muasamcong_danhsach_category temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.code = temp_up.code;

-------------------------------------------------
END;
$$;