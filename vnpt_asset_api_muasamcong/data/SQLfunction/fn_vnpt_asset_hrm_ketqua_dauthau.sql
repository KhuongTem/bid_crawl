DROP FUNCTION IF EXISTS fn_vnpt_asset_hrm_ketqua_dauthau(JSON , INTEGER, TEXT);
CREATE OR REPLACE FUNCTION fn_vnpt_asset_hrm_ketqua_dauthau
  (b_json_data JSON, b_user_id INTEGER, b_random_code TEXT)
      RETURNS VOID
LANGUAGE plpgsql
AS $$
-- DECLARE b_sl_lich_su int = 2;
BEGIN
-------------------------------------------------
/*Xóa dữ liệu cho lần đồng bộ trước*/
	Delete from vnpt_asset_muasamcong_chitiet_ketqua_dauthau where random_code != b_random_code;
/*Thêm mới dữ liệu cho lần đồng bộ trước*/
	with table_chung as
	(
		Select *
		  from json_to_recordset(b_json_data)
			AS x(
			    "ketqua_id" TEXT,
				"matbmt" TEXT,
				"madinhdanh" TEXT,
				"tennhathau" TEXT,
				"giaduthau" FLOAT,
				"giatrungthau" FLOAT,
				"thoigian_giaohang" TEXT
			)
	)
	Insert into vnpt_asset_muasamcong_chitiet_ketqua_dauthau(
	            ketqua_id,
	            matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				giatrungthau,
				thoigian_giaohang,
				random_code,
	            is_active, sync_error, sync_status, time_sync_create,time_sync_write)
	Select      ketqua_id,
	            matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				giatrungthau,
				thoigian_giaohang,
				b_random_code,
	    True, '','none',Now(),Now()
	    from table_chung;

	-- Check trùng dữ liệu, nếu dữ liệu mới thì insert, trùng thì bỏ qua
    with data_update as (
    Select  	ketqua_id,
                matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				giatrungthau,
				thoigian_giaohang
        from vnpt_asset_muasamcong_chitiet_ketqua_dauthau tb1
        where tb1.random_code = b_random_code
        and tb1.ketqua_id not in (select tb2.ketqua_id from bid_muasamcong_chitiet_ketqua_dauthau tb2)
    ),
    insertData AS (
         Insert
             into bid_muasamcong_chitiet_ketqua_dauthau (ketqua_id,
                matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				giatrungthau,
				thoigian_giaohang,create_date)
                 Select
                ketqua_id,
                matbmt,
				madinhdanh,
				tennhathau,
				giaduthau,
				giatrungthau,
				thoigian_giaohang,now()
                 from data_update
     )
    UPDATE bid_muasamcong_chitiet_ketqua_dauthau temp_up
    SET
        ketqua_id = temp_data.ketqua_id,
        matbmt = temp_data.matbmt,
        madinhdanh = temp_data.madinhdanh,
        tennhathau = temp_data.tennhathau,
        giaduthau = temp_data.giaduthau,
        giatrungthau = temp_data.giatrungthau,
        thoigian_giaohang = temp_data.thoigian_giaohang

    FROM vnpt_asset_muasamcong_chitiet_ketqua_dauthau temp_data
    WHERE temp_data.random_code = b_random_code
      AND temp_data.ketqua_id = temp_up.ketqua_id;

-------------------------------------------------
END;
$$;