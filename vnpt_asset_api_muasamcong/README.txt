###############################################
Author: VNPT-IT
Department: Develop
Release: package vnpt_asset v1.0
Env: odoo v11, pgsql 10.5, python 3.6.6
Description: Phần mềm Quản lý tài sản, gồm tài sản cố định và công cụ dụng cụ
##############################################

Hướng dẫn cài đặt gói: vnpt_asset
"""""""""""""""""
"""""""""""""""""
1. Patch file: form_widgets.js --> cắt khoảng trắng ở đầu và cuối chuỗi
Thay thế file: form_widgets.js trong thư mục \static\src\js vào gói web của odoov10 tại đường dẫn: odoov10\addons\web\static\src\js\views

2. Các gói thư viện kèm theo:
    # Gói chính
    vnpt_asset

    # Gói Nghiệp vụ tự động cài theo gói chính
    hr
    purchase
    account_asset

    # Gói tiện ích
    backend_theme_v11
    drag-and-drop
    rowno_in_tree
    web_favicon
    inputmask_widget
    odoo_web_login
    web_google_map
    vnpt_asset_api
    attachment_large_object
    google_place_autocomplete
    auditlog - lưu log người dùng, phải cấu hình

    # 4. Gói bắt buộc đi kèm login SSO
    auth_oauth

3. Hướng dẫn cấu hình:
    Cài api HRM: chú ý cấu hình connect DB
    Cấu hình multi company
    Cấu hình multi image
    Cấu hình đa tiền tệ
    Cấu hình VND
    Cấu hình tài khoản tài sản


4. Cài đặt thư viện để gửi email đơn hàng
    Tham khảo: https://webkul.com/blog/odoo-wkhtmltopdf-upgrade-version/
    WINDOW: add thêm vào biến môi trường Path: <path-to>\wkhtmltopdf\bin
    LINUX (ubuntu)
    1. sudo apt-get install xvfb libfontconfig wkhtmltopdf
    2. which wkhtmltopdf --> tìm thư mục cài, cần thì export biến môi trường này vào