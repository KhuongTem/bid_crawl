odoo.define('vnpt_dashboard.VNPTCustomReportHtmlWidget', function (require) {
    "use strict";

    let AbstractField = require('web.AbstractField');
    let concurrency = require('web.concurrency');
    let core = require('web.core');
    let field_registry = require('web.field_registry');
    let framework = require('web.framework');
    let QWeb = core.qweb;

    let VNPTCustomReportHtmlWidget = AbstractField.extend({
        events: {},
        init: function () {
            this._super.apply(this, arguments);
            this.dm = new concurrency.DropMisordered();
            this.url_asset = '';
            this.list_header = {};
        },
        _getReportDataHeader: function (record_id) {
            let self = this;
            return this.dm.add(this._rpc({
                model: self.model,
                method: 'widget_get_report_header',
                args: [record_id]
            })).then(function (response) {
                self.reportData = response;
                if (response['current_error'] && response['current_data'] !== null && response['current_data']['list_sheet'] !== null && response['current_data']['list_sheet'].length > 0) {
                    $.each(response['current_data']['list_sheet'], function (index, value) {
                        // Chuyển từ array dict thành 1 dict
                        self.list_header['sheet_' + value['sheet_code']] = Object.assign({}, ...value['list_variant'].map((x) => ({
                            [x['variant_order'].toString()]: {
                                "class": (x['class'] || ""),
                                "action": (x['action'] || "")
                            }
                        })));
                    });
                }
            });
        },
        _render: function () {
            let self = this;
            if (self.recordData.id !== undefined && (self.recordData.show_html_data === undefined || self.recordData.show_html_data)) {
                return self._getReportDataHeader(self.recordData.id).then(function () {
                    self.$el.html(QWeb.render("vnpt_custom_report_html_template", self.reportData)).ready(function () {
                        self._actChangeSheet();
                        self._actChangeSheetLink();
                        self._getReportData(0, 1);
                    });
                });
            }
        },
        _getReportData: function (sheet_id, page_index) {
            let self = this;
            framework.blockUI();
            return this.dm.add(this._rpc({
                model: self.model,
                method: 'widget_get_report_data',
                args: [self.recordData.id, sheet_id, page_index]
            })).then(function (response) {
                if (response['current_error'] && response['current_data'] !== null) {
                    self.url_asset = response['current_data']['url_asset'];
                    page_index = parseInt(page_index);
                    $('.site-footer a.nav-link').each(function () {
                        let current_id = $(this).attr('data-id');
                        if (sheet_id !== 0 && current_id !== sheet_id) {
                            return;
                        }
                        let html_data = '',
                            html_page = '',
                            current_code = $(this).attr('data-code'),
                            total_page = 0,
                            total_record = 0;
                        if (response['current_data']['list_data'] !== null && response['current_data']['list_data'][current_code] !== undefined && response['current_data']['list_data'][current_code] !== null && response['current_data']['list_data'][current_code].length > 0) {
                            $.each(response['current_data']['list_data'][current_code], function (index, value) {
                                html_data += '<tr>';
                                let _rowspan_html = '',
                                    _rowspan = 1,
                                    current_option = self.list_header[current_code],
                                    order_class = 1;
                                let ordered = dictToArray(value);
                                // Tính rowspan
                                $.each(ordered, function (key_td, value_td) {
                                    if ($.isArray(value_td)) {
                                        let _length_value = value_td.length;
                                        if (_length_value > _rowspan) {
                                            _rowspan = _length_value;
                                        }
                                    }
                                });
                                $.each(ordered, function (key_td, value_td) {
                                    // Không chạy context
                                    if (key_td === '99_context') return;
                                    let _th_option = current_option[order_class.toString()],
                                    _th_class = _th_option !== undefined ? _th_option['class'] : '';
                                    // Nếu có action
                                    if (value_td !== '' && _th_option !== undefined && _th_option['action'] !== '') {
                                        let td_context = '';
                                        if ('99_context' in ordered && key_td in ordered['99_context']) {
                                            td_context = ordered['99_context'][key_td];
                                        }
                                        value_td = `<a href="javascript:void(0);" name="show_custom_ir_act_window" title="Chi tiết"
                                           data-context='${JSON.stringify(td_context)}'
                                           data-order="${order_class}">${value_td}</a>`;
                                    }
                                    // Nếu là array data
                                    else if ($.isArray(value_td)) {
                                        $.each(value_td, function (key_array, value_array) {
                                            let _child_html = '',
                                                _ordered_array = dictToArray(value_array);
                                            $.each(_ordered_array, function (key_child, value_child) {
                                                _child_html += '<td class="' + _th_class + '">' + (value_child || '') + '</td>';
                                                if (key_array === 0) {
                                                    order_class += 1;
                                                }
                                            });
                                            if (key_array === 0) {
                                                html_data += _child_html;
                                            } else {
                                                _rowspan_html += '<tr>' + _child_html + '</tr>';
                                            }
                                        });
                                        return;
                                    }
                                    html_data += '<td data-order="' + order_class + '" rowspan="' + _rowspan + '" class="' + _th_class + '">' + (value_td || '') + '</td>';
                                    order_class += 1;
                                });
                                html_data += '</tr>';
                                html_data += _rowspan_html;
                            });
                            total_page = response['current_data']['list_total'][current_code]['total_page'];
                            total_record = response['current_data']['list_total'][current_code]['total_record'];
                            if (total_page > 0) {
                                html_page = `<span class="o_pager_counter mr-2">
                                        <span class="o_pager_value">${response['current_data']['list_total'][current_code]['from_record']}-${response['current_data']['list_total'][current_code]['to_record']}</span>
                                        <span> / </span>
                                        <span class="o_pager_limit">${total_record}</span>
                                    </span>`;
                                if (total_page > 1) {
                                    html_page += `<span aria-atomic="true" class="btn-group">
                                    <button type="button" aria-label="Trang trước" title="Trang trước" data-page="${(page_index === 1 ? 1 : (page_index - 1))}" tabindex="-1" class="fa fa-chevron-left btn btn-secondary o_pager_previous rounded-left o_pager_action"></button>
                                    <button type="button" aria-label="Trang tiếp" title="Trang tiếp" data-page="${(page_index < total_page ? (page_index + 1) : total_page)}" tabindex="-1" class="fa fa-chevron-right btn btn-secondary o_pager_next rounded-right o_pager_action"></button>
                                </span>`;
                                }
                            }
                        } else {
                            html_data = '<tr><td colspan="' + Object.keys(self.list_header[current_code]).length + '">Không có dữ liệu</td></tr>';
                        }

                        // Vẽ dữ liệu từng sheet
                        $('#' + current_code + ' tbody').html(html_data).ready(function () {
                            let current_tbody = $('#' + current_code + ' tbody');
                            self._actLinkToAsset(current_tbody);
                            self._showCustomIrActWindow(current_tbody);
                        });

                        // Vẽ paging từng sheet
                        $('.site-footer .footer-right nav[data-code="' + current_code + '"]').html(html_page).ready(function () {
                            self._actChangePage();
                        });

                        // Vẽ đếm số từng sheet
                        $('span[name="sheet_total"]', $(this)).text(`(${total_record})`);
                        if (sheet_id !== 0 && current_id === sheet_id) {
                            return False;
                        }
                    });
                }

                // Set height table
                let header_height = $('html .o_web_client .o_main_navbar').height(),
                    report_filter = $('html .report_filter').height();
                $('.vnpt_custom_report_html').css('height', 'calc(100vh - ' + (header_height + report_filter + 110) + 'px)');
                framework.unblockUI();
            }).catch(function () {
                framework.unblockUI();
            });
        },
        _actChangeSheet: function () {
            $('.site-footer .nav-tabs a', this.$el).off('click').on('click', function () {
                let current_code = $(this).attr('data-code');
                $(".site-footer .nav-tabs a.active", self.$el).removeClass('active').removeAttr('id');
                $(this).addClass('active').attr('id', 'current_sheet');

                $('.tab-content .tab-pane.active', self.$el).removeClass('active').addClass('fade');
                $('.tab-content .tab-pane[id="' + current_code + '"]', self.$el).removeClass('fade').addClass('active');

                $('.modal-content .modal-body').scrollLeft(0).scrollTop(0);

                $('.site-footer .footer-right nav', self.$el).addClass('hidden-element');
                $('.site-footer .footer-right nav[data-code="' + current_code + '"]', self.$el).removeClass('hidden-element');
            });
        },
        _actChangePage: function () {
            let self = this;
            $('.site-footer .footer-right nav .o_pager_action', self.$el).off('click').on('click', function () {
                let current_node = $(this);
                self._getReportData(current_node.closest('nav').attr('data-id'), current_node.attr('data-page'));
                $('.modal-content .modal-body').scrollLeft(0).scrollTop(0);
            });
        },
        _actChangeSheetLink: function () {
            $('.site-footer .footer-left .o_sheet_act', this.$el).off('click').on('click', function () {
                let current_link = $('.site-footer .nav-tabs a.active'),
                    current_parent = $('.site-footer .nav-tabs a'),
                    current_index = current_parent.index(current_link),
                    count_link = current_parent.length;

                if ($(this).hasClass('o_sheet_next') && current_index < (count_link + 2)) {
                    current_index += 3;
                }
                if ($(this).hasClass('o_sheet_previous')) {
                    current_index = current_index < 2 ? 2 : (current_index + 1);
                }
                $('.site-footer .nav-tabs li:nth-child(' + current_index + ') a').click();
                setTimeout(function () {
                    if (isVisibleX($('.footer-left ul.nav-tabs'), $('#current_sheet')) === false) {
                        document.getElementById('current_sheet').scrollIntoView({
                            behavior: 'auto',
                            block: 'center',
                            inline: 'center'
                        });
                    }
                }, 1);
            });
        },
        _actLinkToAsset: function (parent_element) {
            let self = this;
            $('[name="show_asset_detail"]', parent_element).off('click').on('click', function () {
                window.open(self.url_asset + '&id=' + $(this).attr('data-asset'), '_blank');
            });
        },
        _showCustomIrActWindow: function (parent_element) {
            let self = this;
            $('[name="show_custom_ir_act_window"]', parent_element).off('click').on('click', function () {
                let current_node = $(this),
                    parent_action = self.list_header[parent_element.closest('div').attr('id')][current_node.attr('data-order')]['action'],
                    node_data_context = JSON.parse(current_node.attr('data-context').replace(/'/ig, '"'));
                parent_action['context'] = Object.assign({}, parent_action['context'], node_data_context);
                self.do_action(parent_action);
            });
        }
    });

    function isVisibleX($parent_el, $el) {
        let winLeft = $parent_el.scrollLeft();
        let winRight = winLeft + $parent_el.width();
        let elLeft = $el.offset().left;
        let elRight = elLeft + $el.width();
        return ((elRight <= winRight) && (elLeft >= winLeft));
    }

    function dictToArray(input_dict) {
        return Object.keys(input_dict).sort().reduce(
            (_obj, _key) => {
                _obj[_key] = input_dict[_key];
                return _obj;
            },
            {}
        );
    }

    field_registry.add('vnpt_custom_report_html', VNPTCustomReportHtmlWidget);
    return VNPTCustomReportHtmlWidget;
});
