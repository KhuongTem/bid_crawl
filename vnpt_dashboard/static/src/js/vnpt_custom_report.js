odoo.define('vnpt_custom_report', function (require) {
    'use strict';

    require('web.dom_ready');
    var utils = require('report.utils');

    if (window.self === window.top) {
        return;
    }

    var web_base_url = window.origin;
    var trusted_host = utils.get_host_from_url(web_base_url);
    var trusted_protocol = utils.get_protocol_from_url(web_base_url);
    var trusted_origin = utils.build_origin(trusted_protocol, trusted_host);

    var custom = $('[name="show_custom_ir_act_window"]');
    if (custom.length > 0) {
        custom.on('click', function (ev) {
            ev.preventDefault();
            var current_node = $(this),
                parent_action = current_node.closest('div').attr('data-' + current_node.attr('data-action')),
                node_data_context = current_node.attr('data-context');
            parent_action = JSON.parse(parent_action.replace(/'/ig, '"').replace(/":array/ig, '').replace(/:array"/ig, ''));
            node_data_context = JSON.parse(node_data_context.replace(/'/ig, '"'));
            parent_action['context'] = Object.assign({}, parent_action['context'], node_data_context);
            window.parent.postMessage({
                'message': 'report:do_action',
                'action': parent_action,
            }, trusted_origin);
        });
    }

});
