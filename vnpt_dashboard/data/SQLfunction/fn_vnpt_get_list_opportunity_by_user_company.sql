-- kienct90 - Lấy danh sách ids giới hạn theo đơn vị truyền vào
DROP FUNCTION IF EXISTS fn_vnpt_get_list_opportunity_by_user_company(INT, INT, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_get_list_opportunity_by_user_company(_user_id INT,
_user_company_id INT,
_from_date DATE,
 _to_date DATE)
    RETURNS TABLE
            (
                opportunity_id INT
            )
AS
$$
DECLARE
    _user_employee_id INT;
    _user_current_company_id INT;
BEGIN
    SELECT id
    INTO _user_employee_id
    FROM hr_employee
    WHERE user_id = _user_id
    LIMIT 1;

    SELECT company_id
    INTO _user_current_company_id
    FROM res_users WHERE id = 2
    LIMIT 1;

    CREATE TABLE IF NOT EXISTS temp_company_opportunity_by_user_company(id INT);
    WITH RECURSIVE table_temp_company AS (
            SELECT id, name FROM res_company WHERE id = _user_company_id
            UNION ALL
            SELECT res2.id, res2.name FROM res_company res2
            INNER JOIN table_temp_company m ON res2.parent_id = m.id
        )
    INSERT INTO temp_company_opportunity_by_user_company
        SELECT id FROM table_temp_company GROUP BY id;

    RETURN QUERY
       WITH getListCompany AS (
                SELECT usr_com.res_company_id
                FROM res_users_company_check usr_com
                JOIN temp_company_opportunity_by_user_company user_company
                    ON user_company.id = usr_com.res_company_id
                WHERE usr_com.res_users_id = _user_id
                  AND usr_com.module_code = 'opportunity'
                  AND usr_com.is_view IS TRUE
                GROUP BY usr_com.res_company_id
        )
        SELECT opp.id
        FROM vnpt_crm_list_opportunity opp
                 LEFT JOIN getListCompany com ON com.res_company_id = opp.company_id
                 LEFT JOIN vnpt_crm_list_opportunity_list_member mem ON mem.opportunity_id = opp.id
        WHERE (_from_date IS NULL OR opp.create_date::DATE >= _from_date)
          AND (_to_date IS NULL OR opp.create_date::DATE <= _to_date)
          -- Người tạo được xem
          AND (opp.create_uid = _user_id
            OR (_user_id = 2 AND _user_current_company_id = _user_company_id)
            -- Người quản trị đơn vị được xem
            OR (com.res_company_id IS NOT NULL)
            -- Nhân sự phụ trách được xem
            OR (mem.id IS NOT NULL AND mem.member_id = _user_employee_id))
        GROUP BY opp.id;

    --     Xóa bảng tạm
    DROP TABLE IF EXISTS temp_company_opportunity_by_user_company;
END
$$ LANGUAGE PLPGSQL;