DROP FUNCTION IF EXISTS fn_vnpt_dashboard_am_doanhthu_doanhso(INT, VARCHAR, INT, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_am_doanhthu_doanhso(_dong_to_billion INT, _current_year VARCHAR,
                                                                 _stage_damphan_id INT, _first_day_of_year DATE,
                                                                 _last_day_of_year DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH
            -- Lấy danh sách cơ hội có chuyển trạng thái Đấu thầu sang Đàm phán và Đã phê duyệt trong năm
            getListOpportunityBidDone AS (
                SELECT temp_opp.opportunity_id,
                       temp_log.create_date::DATE         AS log_date,
                       opp.estimated_sales                AS doanhso,
                       ROW_NUMBER()
                       OVER (PARTITION BY temp_opp.opportunity_id, temp_log.stage_id
                           ORDER BY temp_log.create_date) AS order_row
                FROM temp_dashboard_am_dashboard temp_opp
                         JOIN vnpt_crm_list_opportunity opp ON opp.id = temp_opp.opportunity_id
                         JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                              ON temp_log.opportunity_id = temp_opp.opportunity_id
                WHERE temp_log.stage_id = _stage_damphan_id
                  AND temp_log.create_date BETWEEN _first_day_of_year AND _last_day_of_year
                  AND temp_log.state = 'confirm'
                  AND temp_log.is_reject_log IS NOT TRUE
            ),
            -- Lấy thông tin doanh số, doanh thu
            getListOpportunityValue AS (
                SELECT temp_opp.opportunity_id,
                       SUM(contract.giatrivnptthuchien) AS doanhthu
                FROM getListOpportunityBidDone temp_opp
                         JOIN bid_hopdong_tuongtu contract
                              ON contract.opportunity_id = temp_opp.opportunity_id
                WHERE temp_opp.order_row = 1
                GROUP BY temp_opp.opportunity_id
            ),
            getListQuarter AS (
                SELECT temp_quarter.quarter,
                       'Quý ' || CASE
                                     WHEN temp_quarter.quarter = 1 THEN 'I'
                                     WHEN temp_quarter.quarter = 2 THEN 'II'
                                     WHEN temp_quarter.quarter = 3 THEN 'III'
                                     ELSE 'IV' END                                                       AS quarter_name,
                       CONCAT(_current_year, (CASE
                                                  WHEN temp_quarter.quarter = 1 THEN '-01-01'
                                                  WHEN temp_quarter.quarter = 2 THEN '-04-01'
                                                  WHEN temp_quarter.quarter = 3 THEN '-07-01'
                                                  ELSE '-10-01' END))::DATE                              AS from_date,
                       fn_vnpt_get_last_day_of_month(CONCAT(_current_year, CASE
                                                                               WHEN temp_quarter.quarter = 1
                                                                                   THEN '-03-01'
                                                                               WHEN temp_quarter.quarter = 2
                                                                                   THEN '-06-01'
                                                                               WHEN temp_quarter.quarter = 3
                                                                                   THEN '-09-01'
                                                                               ELSE '-12-01' END))::DATE AS to_date
                FROM unnest(ARRAY [1, 2, 3, 4]) temp_quarter(quarter)
            ),
            calcualateListProjectQuarterData AS (
                SELECT temp_quarter.quarter,
                       SUM(CASE
                               WHEN date_part('year', temp_opp.log_date)::VARCHAR = _current_year
                                   THEN temp_opp.doanhso
                               ELSE 0 END) AS doanhso,
                       SUM(CASE
                               WHEN date_part('year', temp_opp.log_date)::VARCHAR = _current_year
                                   THEN temp_contract.doanhthu
                               ELSE 0 END) AS doanhthu
                FROM getListOpportunityBidDone temp_opp
                         JOIN getListOpportunityValue temp_contract
                              ON temp_contract.opportunity_id = temp_opp.opportunity_id
                         JOIN getListQuarter temp_quarter
                              ON temp_opp.log_date BETWEEN temp_quarter.from_date AND temp_quarter.to_date
                WHERE temp_opp.order_row = 1
                GROUP BY temp_quarter.quarter
            ),
            prepareListProjectQuarterData AS (
                SELECT temp_data.quarter,
                       SUM(temp_data.doanhso) / _dong_to_billion  AS doanhso,
                       SUM(temp_data.doanhthu) / _dong_to_billion AS doanhthu
                FROM calcualateListProjectQuarterData temp_data
                GROUP BY temp_data.quarter
            ),
            getListProjectQuarterData AS (
                SELECT json_build_object(
                               'name', 'Doanh thu (tỷ đồng)',
                               'data', ARRAY_AGG(CASE
                                                     WHEN temp_data.doanhthu > 0
                                                         THEN round(temp_data.doanhthu::DECIMAL, 2)
                                                     ELSE 0 END
                                                 ORDER BY temp_quarter.quarter)) AS doanhthu,
                       json_build_object(
                               'name', 'Doanh số (tỷ đồng)',
                               'dashStyle', 'Solid',
                               'data', ARRAY_AGG(CASE
                                                     WHEN temp_data.doanhso > 0
                                                         THEN round(temp_data.doanhso::DECIMAL, 2)
                                                     ELSE 0 END
                                                 ORDER BY temp_quarter.quarter)) AS doanhso
                FROM getListQuarter temp_quarter
                         LEFT JOIN prepareListProjectQuarterData temp_data
                                   ON temp_data.quarter = temp_quarter.quarter
            )
        SELECT json_build_object(
                       'element', 'opportunity_quarter_data',
                       'icon', 'fa-line-chart',
                       'title', 'Thống kê doanh thu, doanh số cơ hội',
                       'type', 'line',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_quarter.quarter_name ORDER BY temp_quarter.quarter)
                           FROM getListQuarter temp_quarter
                       ),
                       'value',
                       (SELECT ('[' ||
                                temp_data.doanhthu
                                    || ', ' ||
                                temp_data.doanhso
                           || ']')::JSON
                        FROM getListProjectQuarterData temp_data)
                   );

END
$$ LANGUAGE PLPGSQL;