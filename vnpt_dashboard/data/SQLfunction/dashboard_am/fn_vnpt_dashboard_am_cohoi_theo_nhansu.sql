DROP FUNCTION IF EXISTS fn_vnpt_dashboard_am_cohoi_theo_nhansu(DATE, VARCHAR);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_am_cohoi_theo_nhansu(_first_day_current_month DATE,
                                                              _phongban_id VARCHAR)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _last_day_current_month DATE;
    _stage_trienkhaihopdong                         INT;
    _stage_kyhopdong                                INT;
BEGIN
    _last_day_current_month = fn_vnpt_get_last_day_of_month(_first_day_current_month);

    _stage_kyhopdong                        = (SELECT id FROM vnpt_crm_stage WHERE code = 'kyket' LIMIT 1);
    _stage_trienkhaihopdong                 = (SELECT id FROM vnpt_crm_stage WHERE code = 'trienkhai' LIMIT 1);

    RETURN QUERY
        WITH
        table_phongban_multi AS (
                 SELECT (CASE WHEN value_phongban_id <> '' THEN value_phongban_id::INT ELSE 0 END) value_phongban_id
                  FROM UNNEST(STRING_TO_ARRAY(_phongban_id::text,';'::text)) AS value_phongban_id
         ),
        table_create_user AS
            (
                SELECT res_users.id, employee.name AS name
                FROM res_users
                JOIN hr_employee employee ON employee.user_id = res_users.id
                JOIN table_phongban_multi phongban_multi ON phongban_multi.value_phongban_id = res_users.department_id
                ORDER BY employee.name
            ),
        table_kyhopdong AS
            (
                SELECT opportunity_by_user.opportunity_id,
                       COALESCE(cont.contract_value, 0) contract_value, -- Giá trị hợp đồng
                       opp.create_uid
                FROM temp_dashboard_am_dashboard opportunity_by_user
                INNER JOIN vnpt_crm_list_opportunity opp ON opp.id = opportunity_by_user.opportunity_id
                INNER JOIN table_create_user create_user ON create_user.id = opp.create_uid
                JOIN bid_hopdong_tuongtu cont
                               ON cont.opportunity_id = opportunity_by_user.opportunity_id

                JOIN vnpt_crm_list_opportunity_list_log_stage log_stage
                    ON log_stage.opportunity_id = opportunity_by_user.opportunity_id
                        AND log_stage.stage_id = _stage_trienkhaihopdong
                        AND log_stage.previous_stage_id = _stage_kyhopdong
                        AND log_stage.state = 'confirm'
                        AND log_stage.is_reject_log IS NOT TRUE
                        AND log_stage.create_date BETWEEN _first_day_current_month AND _last_day_current_month
                GROUP BY opportunity_by_user.opportunity_id, cont.contract_value, opp.create_uid
            ),
            temp_contract_value AS
            (
                SELECT SUM(COALESCE(kyhopdong.contract_value, 0)) contract_value, create_user.id
                FROM table_create_user create_user
                JOIN table_kyhopdong kyhopdong ON kyhopdong.create_uid = create_user.id
                GROUP BY create_user.id
            ),
            temp_count AS
            (
                SELECT COUNT(1) count, create_user.id
                FROM table_create_user create_user
                JOIN table_kyhopdong kyhopdong ON kyhopdong.create_uid = create_user.id
                GROUP BY create_user.id
            ),
            temp_final AS
            (
                SELECT COALESCE(temp_contract_value.contract_value, 0) contract_value,
                       COALESCE(temp_count.count, 0) count,
                       create_user.id
                FROM table_create_user create_user
                LEFT JOIN temp_contract_value ON temp_contract_value.id = create_user.id
                LEFT JOIN temp_count ON temp_count.id = create_user.id
                GROUP BY temp_contract_value.contract_value, temp_count.count, create_user.id
            )
        SELECT json_build_object(
                       'element',
                       'opportunity_month_stage_data_cohoi_theo_nhansu_chart',
                       'icon', 'fa-line-chart',
                       'title',
                       'Thống kê cơ hội theo nhân sự',
                       'type', 'xy',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(create_user.name)
                           FROM table_create_user create_user
                       ),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Giá trị cơ hội',
                                        'type', 'column',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data',
                                        (
                                            SELECT ARRAY_AGG(
                                                            table_kyhopdong.contract_value
                                                            )
                                            FROM temp_final table_kyhopdong
                                            WHERE create_user.id = table_kyhopdong.id )

                                        )
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Giá trị trung bình',
                                        'type', 'column',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data',
                                        (
                                            SELECT ARRAY_AGG(
                                            ROUND((table_kyhopdong.contract_value::FLOAT / table_kyhopdong.count)::DECIMAL, 2)
                                                            )
                                            FROM temp_final table_kyhopdong
                                            WHERE create_user.id = table_kyhopdong.id)

                                        )
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Số lượng cơ hội',
                                        'type', 'spline',
                                        'yAxis', 1,
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Số lượng'),
                                        'data', (
                                           SELECT ARRAY_AGG(
                                                            table_kyhopdong.count
                                                            )
                                            FROM temp_final table_kyhopdong
                                            WHERE create_user.id = table_kyhopdong.id
                                            )
                                    )
                           || ']')::JSON
                        FROM table_create_user create_user
                                 )::JSON);

END
$$ LANGUAGE PLPGSQL;