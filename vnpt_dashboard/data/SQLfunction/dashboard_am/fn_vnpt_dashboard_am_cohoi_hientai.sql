DROP FUNCTION IF EXISTS fn_vnpt_dashboard_am_cohoi_hientai(VARCHAR, DATE, DATE, VARCHAR);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_am_cohoi_hientai(_current_year VARCHAR,
                                                              _first_day_of_year DATE,
                                                              _first_day_current_month DATE,
                                                              _phongban_id VARCHAR)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _first_day_last_month   DATE;
    _last_day_last_month    DATE;
    _last_day_current_month DATE;
BEGIN

    _first_day_last_month = to_char(_first_day_current_month - interval '1 month', 'yyyy-mm-01')::DATE;
    _last_day_last_month = fn_vnpt_get_last_day_of_month(_first_day_last_month);
    _last_day_current_month = fn_vnpt_get_last_day_of_month(_first_day_current_month);

    RETURN QUERY
        WITH
        table_phongban_multi AS (
                 SELECT (CASE WHEN value_phongban_id <> '' THEN value_phongban_id::INT ELSE 0 END) value_phongban_id
                  FROM UNNEST(STRING_TO_ARRAY(_phongban_id::text,';'::text)) AS value_phongban_id
         ),

            -- Lấy danh sách cơ hội có chuyển trạng thái trong tháng và tháng trước
            getListOpportunityChange AS (
                SELECT temp_opp.opportunity_id,
                       temp_log.stage_id,
                       temp_log.create_date::DATE         AS log_date,
                       ROW_NUMBER()
                       OVER (PARTITION BY temp_opp.opportunity_id, temp_log.stage_id, to_char(temp_log.create_date, 'mm/yyyy')
                           ORDER BY temp_log.create_date) AS order_row
                FROM temp_dashboard_am_dashboard temp_opp
                         JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                              ON temp_log.opportunity_id = temp_opp.opportunity_id
                JOIN vnpt_crm_list_opportunity opp ON opp.id = temp_opp.opportunity_id
                LEFT JOIN res_users ru on opp.create_uid = ru.id
                LEFT JOIN table_phongban_multi phongban_multi ON phongban_multi.value_phongban_id = ru.department_id
                WHERE temp_log.create_date BETWEEN _first_day_last_month AND _last_day_current_month
                AND temp_log.state = 'confirm'
                AND temp_log.is_reject_log IS NOT TRUE
                AND (_phongban_id IS NULL OR _phongban_id = '0' OR (phongban_multi.value_phongban_id IS NOT NULL))
            ),
            getListStage AS (
                SELECT stage.id,
                       stage.name,
                       stage.sequence
                FROM vnpt_crm_stage stage
            ),
            getListMonth AS (
                SELECT temp_month.t_month
                FROM unnest(ARRAY [to_char(_first_day_last_month, 'mm/yyyy'),
                    to_char(_last_day_current_month, 'mm/yyyy')]) temp_month(t_month)
            ),
            prepareListProjectMonthData AS (
                SELECT temp_data.stage_id,
                       temp_month.t_month,
                       SUM(1) AS opportunity_number
                FROM getListOpportunityChange temp_data
                         JOIN getListMonth temp_month ON temp_month.t_month = to_char(temp_data.log_date, 'mm/yyyy')
                    OR temp_month.t_month = to_char(COALESCE(temp_data.log_date, _last_day_current_month), 'mm/yyyy')
                WHERE temp_data.order_row = 1
                GROUP BY temp_data.stage_id,
                         temp_month.t_month
            ),
            getListProjectMonthData AS (
                SELECT temp_month.t_month                                       AS sequence,
                       json_build_object(
                               'name', CONCAT('Tháng ', temp_month.t_month),
                               'search_month', temp_month.t_month,
                               'data', ARRAY_AGG(CASE
                                                     WHEN temp_data.opportunity_number > 0
                                                         THEN temp_data.opportunity_number END
                                                 ORDER BY temp_stage.sequence)) AS json_data
                FROM getListMonth temp_month
                         INNER JOIN getListStage temp_stage ON TRUE
                         LEFT JOIN prepareListProjectMonthData temp_data
                                   ON temp_data.t_month = temp_month.t_month
                                       AND temp_data.stage_id = temp_stage.id
                GROUP BY temp_month.t_month
            )
        SELECT json_build_object(
                       'element', 'opportunity_month_stage_data_hientai_chart',
                       'icon', 'fa-bar-chart',
                       'title',
                       'Thống kê cơ hội hiện tại',
                       'type', 'bar',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (SELECT ARRAY_AGG(temp_stage.name ORDER BY temp_stage.sequence)
                                    FROM getListStage temp_stage),
                       'value',
                       (SELECT json_agg(temp_data.json_data
                                        ORDER BY date_part('year', to_date('01/' || temp_data.sequence, 'dd/mm/yyyy')))
                        FROM getListProjectMonthData temp_data)::JSON
                   );

END
$$ LANGUAGE PLPGSQL;