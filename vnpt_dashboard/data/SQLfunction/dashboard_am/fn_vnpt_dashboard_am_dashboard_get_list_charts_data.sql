-- hungvdh - [Dashboard AM] Lấy danh sách data biểu đồ
-- kienct90: 14/12/2022: Bổ sung thêm các tiêu chí
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_am_dashboard_get_list_charts_data
(INT, INT, VARCHAR, INT, VARCHAR, INT, INT, INT, VARCHAR, INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_am_dashboard_get_list_charts_data(
                                _user_id            INT,
                                _user_company_id    INT,
                                _filter_month       VARCHAR,
                                _company_id         INT,
                                _service_id         VARCHAR,
                                _location_id        INT,
                                _level              INT,
                                _nhom_sanpham_dv    INT,
                                _phongban_id        VARCHAR,
                                _is_change_level               INT,
                                _is_change_nhom_sp               INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _first_day_current_month DATE = to_char(CURRENT_DATE, 'yyyy-mm-01')::DATE;
    _last_day_current_month DATE;
    _current_year            VARCHAR;
    _first_day_of_year       DATE;
    _last_day_of_year        DATE;
    _dong_to_billion         INT  = 1000000;
    _current_employee_id     INT;
    _stage_damphan_id        INT;
    _stage_kyket_sequence        INT;
    _stage_saunghiemthu_sequence INT;
BEGIN
    _stage_damphan_id = (SELECT stage.id FROM vnpt_crm_stage stage WHERE stage.code = 'damphan' LIMIT 1);
    _stage_kyket_sequence = (SELECT stage.id FROM vnpt_crm_stage stage WHERE stage.code = 'kyket' LIMIT 1);
    _stage_saunghiemthu_sequence = (SELECT stage.id FROM vnpt_crm_stage stage WHERE stage.code = 'quyettoan' LIMIT 1);
    _current_employee_id = (SELECT he.id FROM hr_employee he WHERE he.user_id = _user_id LIMIT 1);
    _last_day_current_month = fn_vnpt_get_last_day_of_month(_first_day_current_month);


    IF _filter_month IS NOT NULL THEN
        _first_day_current_month = to_date('01/' || _filter_month, 'dd/mm/yyyy');
    END IF;

    _current_year = date_part('year', _first_day_current_month)::VARCHAR;
    _first_day_of_year = to_char(_first_day_current_month, 'yyyy-01-01')::DATE;
    _last_day_of_year = (date_trunc('month', to_char(_first_day_of_year, 'YYYY-12-01')::DATE)
                             + interval '1 month' - interval '1 day')::DATE;

    -- Tạo temp validate
    CREATE TEMP TABLE IF NOT EXISTS temp_dashboard_am_dashboard
    (
        opportunity_id INT,   -- [Cơ hội kinh doanh] Id
        stage_sequence INT    -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Thứ tự
    );
    TRUNCATE TABLE temp_dashboard_am_dashboard;

    -- Lấy danh sách cơ hội gắn với AM
    WITH table_service_multi AS (
             SELECT (CASE WHEN value_service_id <> '' THEN value_service_id::INT ELSE 0 END) value_service_id
              FROM UNNEST(STRING_TO_ARRAY(_service_id::text,';'::text)) AS value_service_id
     )
    INSERT INTO temp_dashboard_am_dashboard
        (opportunity_id, stage_sequence)
    SELECT opp.id AS opportunity_id,
           stage.sequence             AS stage_sequence
    FROM vnpt_crm_list_opportunity opp
             INNER JOIN vnpt_crm_stage stage ON stage.id = opp.stage_id
             JOIN vnpt_crm_list_opportunity_list_member mem
                  ON mem.opportunity_id = opp.id
             JOIN vnpt_project_role role ON role.id = mem.role_id
             LEFT JOIN vnpt_crm_list_opportunity_list_service list_service
                                                ON list_service.opportunity_id = opp.id
                LEFT JOIN bid_sanpham_dv sanpham_dv ON sanpham_dv.id = list_service.service_id
                LEFT JOIN table_service_multi service_multi ON service_multi.value_service_id = sanpham_dv.id
                LEFT JOIN bid_nhom_sp nhom_sp ON nhom_sp.id = sanpham_dv.nhomspid
                LEFT JOIN rel_list_opportunity_city opportunity_city -- xử lý địa bàn
                                            ON opportunity_city.opportunity_id = opp.id
    WHERE (_user_id = 2 OR mem.member_id = _current_employee_id)
      AND (_company_id = 1 OR opp.company_id = _company_id)
      AND role.code = 'am'
      AND (_service_id IS NULL OR _service_id = '0' OR (sanpham_dv.id IS NOT NULL AND list_service.id IS NOT NULL))
      AND (_nhom_sanpham_dv IS NULL OR _nhom_sanpham_dv = 0 OR (sanpham_dv.nhomspid = _nhom_sanpham_dv AND list_service.id IS NOT NULL))
      AND (_location_id IS NULL OR _location_id = 0 OR (opportunity_city.city_id = _location_id))
      AND (_level IS NULL OR _level = 0
              OR (
              nhom_sp.id IS NOT NULL AND nhom_sp.level = _level
--                   list_service.id IS NOT NULL
--                   AND (
--                       CASE
--                       WHEN _level = 1 THEN (sanpham_dv.level1 IS NOT NULL AND sanpham_dv.level1 != '')
--                       WHEN _level = 2 THEN (sanpham_dv.level2 IS NOT NULL AND sanpham_dv.level2 != '')
--                       WHEN _level = 3 THEN (sanpham_dv.level3 IS NOT NULL AND sanpham_dv.level3 != '')
--                       END
--                       )
              )
          )
    GROUP BY opp.id, stage.sequence;

    RETURN QUERY
        SELECT json_build_object(
                       'current_status', TRUE,
                       'current_message', 'Thành công',
                       'current_data', json_build_object(
                               'list_div',
                               ('[' ||
                                    -- Div trái màn hình
                                json_build_object(
                                        'start_div', '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                        'end_div', '</div>',
                                        'list_charts',
                                        ('[' ||
                                         (
                                             SELECT *
                                             FROM fn_vnpt_dashboard_am_doanhthu_doanhso(_dong_to_billion, _current_year,
                                                                                        _stage_damphan_id,
                                                                                        _first_day_of_year,
                                                                                        _last_day_of_year)
                                         )
                                         || ',' ||
                                         (
                                             SELECT *
                                             FROM fn_vnpt_dashboard_am_cohoi_hientai(_current_year,
                                                                                  _first_day_of_year,
                                                                                  _first_day_current_month,
                                                                                  _phongban_id
                                                                                  )
                                         )
                                             || ', ' ||
                                             -- Biến động tình hình kinh doanh theo tháng (Đối với Cơ hội chưa ký hợp đồng)
                                         (
                                             SELECT *
                                             FROM fn_vnpt_dashboard_am_tinhhinh_kinhdoanh_chuaky(
                                                     _stage_kyket_sequence,
                                                 _first_day_current_month,
                                                 _last_day_current_month)
                                         )

                                            || ']')::JSON)
                                    || ', ' ||
                                    -- Div phải màn hình
                                json_build_object(
                                        'start_div',
                                        '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                        'end_div', '</div>',
                                        'list_charts',
                                        ('['
                                             ||
                                         (SELECT *
                                          FROM fn_vnpt_dashboard_am_cohoi_thang_m(_current_year,
                                                                                  _first_day_of_year,
                                                                                  _first_day_current_month))

                                         || ',' ||
                                         (
                                             SELECT *
                                             FROM fn_vnpt_dashboard_am_cohoi_theo_nhansu(
                                                                                  _first_day_current_month,
                                                                                  _phongban_id
                                                                                  )
                                         )
                                             || ', ' ||
                                             -- Biến động tình hình kinh doanh theo tháng (Đã ký hợp đồng)
                                         (
                                             SELECT *
                                             FROM fn_vnpt_dashboard_am_tinhhinh_kinhdoanh_daky(
                                                     _stage_kyket_sequence,
                                                     _stage_saunghiemthu_sequence,
                                                     _first_day_current_month,
                                                    _last_day_current_month)
                                         )

                                            || ']')::JSON)


                                   || ']')::JSON
                           ),

                        'list_nhom_sp',
                          (
                              CASE
                                  WHEN _is_change_level = 1 THEN
                                      (
                                          SELECT ARRAY_AGG(json_build_object('id', id, 'name', tenhomsp))
                                          FROM bid_nhom_sp
                                          WHERE (_level IS NULL OR _level = 0 OR level = _level)
                                      )
                                  ELSE
                                      '{}'
                                  END
                          ),
                          'list_sanpham_dv',
                          (
                              CASE
                                  WHEN _is_change_level = 1 OR _is_change_nhom_sp = 1
                                      THEN
                                      (
                                          SELECT ARRAY_AGG(
                                                         json_build_object(
                                                                 'id',
                                                                 bid_sanpham_dv.id,
                                                                 'name',
                                                                 bid_sanpham_dv.tennoibo))
                                          FROM bid_sanpham_dv
                                                   INNER JOIN bid_nhom_sp ON bid_nhom_sp.id = bid_sanpham_dv.nhomspid
                                          WHERE (_is_change_level = 1 AND (_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level) )
                                             OR (_is_change_nhom_sp = 1 AND
                                                 ((_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level)
                                                     AND (_nhom_sanpham_dv IS NULL
                                                              OR _nhom_sanpham_dv = 0
                                                              OR bid_nhom_sp.id = _nhom_sanpham_dv)
                                                     )

                                                 )
                                      )
                                  ELSE
                                      '{}'
                                  END
                          )

                   );

    DROP TABLE temp_dashboard_am_dashboard;

END
$$ LANGUAGE PLPGSQL;