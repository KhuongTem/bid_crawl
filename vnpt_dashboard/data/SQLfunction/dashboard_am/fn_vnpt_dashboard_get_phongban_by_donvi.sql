-- kienct - [Dashboard AM] Lấy ra Phòng ban cấp 4 của đơn vị được chọn, với User là quyền Admin của đơn vị được chọn đó
-- kienct - [Dashboard AM] Lấy ra Phòng ban cấp 4 của đơn vị được chọn, với User là quyền Admin của đơn vị được chọn đó
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_get_phongban_by_donvi(INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_get_phongban_by_donvi(_user_id INT, _donvi INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN


        IF EXISTS(select 1 from res_users_company_check
                WHERE _user_id = 2 OR (module_code = 'opportunity'
                  AND res_company_id = _donvi
                  AND res_users_id = _user_id
                  AND is_admin = True)) THEN
            BEGIN
                RETURN QUERY
                WITH RECURSIVE getListCompany AS (
                        SELECT pcom.id, pcom.name, pcom.type, 1 AS level, CAST(pcom.id AS VARCHAR) AS tree
                        FROM res_company AS pcom
                        WHERE pcom.id = 1
                        UNION ALL
                        SELECT ccom.id, (com_parrent.name || '/' || ccom.name) AS name,  ccom.type, (lt.level + 1) AS level, (lt.tree || '/' || CAST(ccom.id AS VARCHAR)) AS tree
                        FROM res_company AS ccom
                                 INNER JOIN getListCompany AS lt
                                            ON ccom.parent_id = lt.id
                                JOIN res_company com_parrent ON ccom.parent_id = com_parrent.id
                    )
                SELECT json_build_object('data', ARRAY_AGG(json_build_object('id',id, 'name', name))) FROM getListCompany company
                WHERE (_donvi = 1 AND company.level = 4 AND type = 'BP')
                OR (_donvi <> 1 AND company.level = 4 AND type = 'BP' AND tree like ('%/' || CAST(_donvi AS VARCHAR) || '/%'));
            END;
        ELSE
            BEGIN
                RETURN QUERY
                SELECT json_build_object('data', '[]'::JSON) ;
            END;
        END IF;
END
$$ LANGUAGE PLPGSQL;