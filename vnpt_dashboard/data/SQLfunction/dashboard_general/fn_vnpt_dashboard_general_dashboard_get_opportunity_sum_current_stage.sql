-- hungvdh - [Dashboard chung] Thống kê số lượng hiện tại của cơ hội theo giai đoạn
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_get_opportunity_sum_current_stage();
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_get_opportunity_sum_current_stage()
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN
    RETURN QUERY
        WITH getListStage AS (
            SELECT stage.id, stage.code, stage.name, stage.sequence
            FROM vnpt_crm_stage stage
            ORDER BY stage.sequence
        )

        SELECT json_build_object(
                       'element', 'opportunity_sum_current_stage_chart',
                       'icon', 'fa-bar-pie',
                       'title', 'Thống kê số lượng hiện tại của cơ hội theo giai đoạn',
                       'type', 'pie',
                       'other_option', '[]'::JSON,
                       'category', '',
                       'value',
                       (
                           SELECT ARRAY_AGG(
                                          json_build_object(
                                                  'name',
                                                  stage.name,
                                                  'y',
                                                  (
                                                      SELECT COUNT(1)
                                                      FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp
                                                      WHERE temp.stage_id = stage.id
                                                  )
                                              )
                                      )
                           FROM getListStage stage
                       )
                   );

END
$$ LANGUAGE PLPGSQL;