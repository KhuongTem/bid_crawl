-- Biểu đồ phân loại lý do trượt thầu
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_get_opplose_cate_reason
(DATE, DATE);
CREATE OR REPLACE FUNCTION  fn_vnpt_dashboard_general_dashboard_get_opplose_cate_reason(
_from_date    DATE,
_to_date      DATE
)
    returns TABLE(output_data json)
    language plpgsql
as
$$

BEGIN
    RETURN QUERY
    WITH table_truotthau AS
    (
        -- (1)	Kiểm tra trong Nhóm lý do trượt thầu đang thống kê có những lý do trượt thầu nào.
        -- (2)	Kiểm tra các gói thầu có thời điểm cập nhật trạng thái gói thầu là “Trượt thầu” trong khoảng thời gian thống kê.
        -- (3)	Đếm số lượng các gói thầu ở mục (2) mà trong trường “ Lý do trượt thầu” trong gói thầu đó là lý do trượt thầu ở (1)
        SELECT opportunity_by_user.opportunity_id, trungthau.date_bid_fail, lydo_truotthau.nhomlydo2
        FROM
        temp_dashboard_vnpt_crm_list_opportunity_ver2 opportunity_by_user
        JOIN
        vnpt_crm_list_opportunity opp ON opp.id = opportunity_by_user.opportunity_id
        INNER JOIN bid_goithau_dacoketqua_dauthau trungthau
            ON trungthau.opportunity_id = opportunity_by_user.opportunity_id AND trungthau.trangthai = 'TRUOT'
        INNER JOIN bid_lydo_truotthau lydo_truotthau ON lydo_truotthau.id = trungthau.lydoid
        WHERE trungthau.date_bid_fail BETWEEN _from_date AND _to_date
        GROUP BY opportunity_by_user.opportunity_id, trungthau.date_bid_fail, lydo_truotthau.nhomlydo2
    ),
    table_nhomlydo_truotthau AS
    (
        SELECT id, tennhomlydo FROM bid_nhomlydo_truotthau ORDER BY sequence
    )
    SELECT json_build_object(
           'element', 'opportunity_opp_lose_cate_reason_chart',
           'icon', 'fa-bar-pie',
           'title', 'Biểu đồ phân loại lý do trượt thầu',
           'type', 'pie',
           'other_option', '[]'::JSON,
           'category', '',
           'value',
            (
                SELECT ARRAY_AGG(
                   json_build_object(
                       'name',
                       nhomlydo_truotthau.tennhomlydo,
                       'y',
                       (

                           (
                                SELECT COUNT(1) FROM    table_truotthau WHERE nhomlydo2 = nhomlydo_truotthau.id
                           )
                       )
                       )
                    )
                FROM table_nhomlydo_truotthau nhomlydo_truotthau
            )
        )
        ;
END
$$;
