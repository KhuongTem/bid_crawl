-- hungvdh - [Dashboard chung] Thống kê số cơ hội phát sinh trong tháng
-- kienct90 -  Bổ sung thêm tìm kiếm _type theo Số lượng, Giá trị Ngân sách dự toán estimated_budget
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_cohoi_phatsinh_trongthang(DATE, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_cohoi_phatsinh_trongthang(_first_day_of_month DATE, _type INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH getListStage AS (
            SELECT stage.id, stage.code, stage.name, stage.sequence
            FROM vnpt_crm_stage stage
            ORDER BY stage.sequence
        ),
             temp_phatsinh_trongthang AS (
                SELECT
                       list_opportunity_ver2.opportunity_id,
                       list_opportunity_ver2.stage_id,
                       list_opportunity_ver2.estimated_budget,
                        ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                        ORDER BY temp_log.create_date DESC) AS order_row
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                          ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
                 WHERE temp_log.create_date >= _first_day_of_month
                    AND temp_log.state = 'confirm'
                    AND temp_log.is_reject_log IS NOT TRUE
             ),
             -- Thống kê số cơ hội phát sinh trong tháng
             getListOpportunityStageCurrentMonth_5 AS (
                 SELECT temp_data.stage_id,
                        SUM(temp_data.estimated_budget) estimated_budget,
                        SUM(1) AS opportunity_number
                 FROM temp_phatsinh_trongthang temp_data
                 WHERE temp_data.order_row = 1
                 GROUP BY temp_data.stage_id
             )
        SELECT json_build_object(
                       'element', 'opportunity_stage_month',
                       'icon', 'fa-bar-chart',
                       'title', 'Thống kê số cơ hội phát sinh theo giai đoạn trong tháng',
                       'type', 'column',
                       'other_option', json_build_object(
                               'dataLabels', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_stage.name)
                           FROM getListStage temp_stage
                       ),
                       'value',
                       (SELECT ('[' ||
                                (CASE WHEN _type = 0 THEN
                                    json_build_object(
                                        'name', 'Số lượng',
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.opportunity_number > 0
                                                                  THEN temp_data.opportunity_number END
                                                          ))
                                WHEN _type = 1 THEN
                                    json_build_object(
                                        'name', 'Ngân sách dự toán',
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.estimated_budget > 0
                                                                  THEN temp_data.estimated_budget END
                                                          ))
                                END)
                           || ']')::JSON
                        FROM getListStage temp_stage
                                 LEFT JOIN getListOpportunityStageCurrentMonth_5 temp_data
                                           ON temp_data.stage_id = temp_stage.id));

END
$$ LANGUAGE PLPGSQL;