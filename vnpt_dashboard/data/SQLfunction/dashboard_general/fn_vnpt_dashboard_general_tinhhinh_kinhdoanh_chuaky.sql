-- hungvdh - [Dashboard chung] Biến động tình hình kinh doanh (Chưa ký hợp đồng)
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_tinhhinh_kinhdoanh_chuaky(INT, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_tinhhinh_kinhdoanh_chuaky(_stage_kyket_sequence INT, _from_date DATE, _to_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH getListMonth_10 AS (
            SELECT to_char(a.month_name, 'mm/yyyy')     AS month_name,
                   to_char(a.month_name, 'yyyymm')::INT AS month_number,
                   a.sequence
            FROM generate_series(
                         date_trunc('month',
                                    to_char(date_trunc('year', _from_date), 'yyyy-mm-dd')::DATE),
                         _to_date, '1 month'
                     )
                     WITH ORDINALITY a(month_name, sequence)
        ),
             -- Lấy trạng thái cuối cùng của từng tháng
             getListStageFluctuationsMonthMember_10 AS (
                 SELECT temp_log.create_date,
                        mem.member_id                                                                    AS sell_id,
                        ROW_NUMBER()
                        OVER (PARTITION BY temp_data.opportunity_id,
                            to_char(temp_log.create_date, 'mm/yyyy') ORDER BY temp_log.create_date DESC) AS order_row
                 FROM temp_dashboard_vnpt_crm_list_opportunity temp_data
                          JOIN vnpt_crm_list_opportunity_list_member mem
                               ON mem.opportunity_id = temp_data.opportunity_id
                          JOIN vnpt_project_role role ON role.id = mem.role_id
                          JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                               ON temp_log.opportunity_id = temp_data.opportunity_id
                          JOIN vnpt_crm_stage stage ON stage.id = temp_log.stage_id
                 WHERE temp_data.stage_sequence < _stage_kyket_sequence
                   AND role.code = 'am'
                   AND temp_log.create_date::DATE BETWEEN _from_date AND _to_date
                   AND temp_log.state = 'confirm'
                   AND temp_log.is_reject_log IS NOT TRUE
             ),
             getListMemberAMCategory_10 AS (
                 SELECT temp_flu.sell_id,
                        replace(he.work_email, '@vnpt.vn', '') AS work_email
                 FROM getListStageFluctuationsMonthMember_10 temp_flu
                          JOIN hr_employee he ON he.id = temp_flu.sell_id
                 WHERE temp_flu.order_row = 1
                 GROUP BY temp_flu.sell_id,
                          replace(he.work_email, '@vnpt.vn', '')
             ),
             getLastData_10 AS (
                 SELECT temp_data.sell_id,
                        to_char(temp_data.create_date, 'mm/yyyy') AS month_name,
                        SUM(1)                                    AS opportunity_number
                 FROM getListStageFluctuationsMonthMember_10 temp_data
                 WHERE temp_data.order_row = 1
                 GROUP BY temp_data.sell_id,
                          to_char(temp_data.create_date, 'mm/yyyy')
             ),
             calcualateListFluctuationsMonthMember_10 AS (
                 SELECT temp_month.month_name,
                        temp_month.sequence               AS month_sequence,
                        temp_sell.sell_id,
                        temp_sell.work_email,
                        SUM(temp_data.opportunity_number)
                        OVER (PARTITION BY temp_sell.sell_id, temp_sell.work_email
                            ORDER BY temp_month.sequence) AS opportunity_number
                 FROM getListMonth_10 temp_month
                          JOIN getListMemberAMCategory_10 temp_sell ON TRUE
                          LEFT JOIN getLastData_10 temp_data
                                    ON temp_sell.sell_id = temp_data.sell_id
                                        AND
                                       temp_data.month_name = temp_month.month_name
             ),
             getListFluctuationsMonthMember_10 AS (
                 SELECT temp_data.sell_id,
                        json_build_object(
                                'name', temp_data.work_email,
                                'data', ARRAY_AGG(CASE
                                                      WHEN temp_data.opportunity_number > 0
                                                          THEN temp_data.opportunity_number
                                                      ELSE NULL END
                                                  ORDER BY temp_data.month_sequence)) AS json_data
                 FROM calcualateListFluctuationsMonthMember_10 temp_data
                 GROUP BY temp_data.sell_id,
                          temp_data.work_email
             )
        SELECT json_build_object(
                       'element', 'opportunity_fluctuations_month',
                       'icon', 'fa-line-chart',
                       'title', 'Biến động tình hình kinh doanh (Chưa ký hợp đồng)',
                       'type', 'line',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_month.month_name ORDER BY temp_month.sequence)
                           FROM getListMonth_10 temp_month
                       ),
                       'value',
                       (SELECT json_agg(temp_data.json_data ORDER BY temp_data.sell_id)
                        FROM getListFluctuationsMonthMember_10 temp_data)::JSON);

END
$$ LANGUAGE PLPGSQL;