
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_list_charts_data_version5(INT, INT, INT, VARCHAR, INT, INT, INT, VARCHAR, VARCHAR, INT, INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_list_charts_data_version5(_user_id            INT,
                                                                                    _user_company_id    INT,
                                                                                    _company_id         INT,
                                                                                    _service_id         VARCHAR,
                                                                                    _location_id        INT,
                                                                                    _level              INT,
                                                                                    _nhom_sanpham_dv    INT,
                                                                                    _input_from_date    VARCHAR,
                                                                                    _input_to_date      VARCHAR,
                                                                                    _type               INT,
                                                                                    _page_load_data_chart               INT,
                                                                                    _is_change_level               INT,
                                                                                    _is_change_nhom_sp               INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _from_date                   DATE = to_char(date_trunc('year', CURRENT_DATE), 'yyyy-mm-dd')::DATE;
    _to_date                     DATE = CURRENT_DATE;
    _previous_month_date         DATE;
    _dong_to_million             INT  = 1000000;
    _stage_kyket_sequence        INT;
    _stage_saunghiemthu_sequence INT;
    _stage_dexuat_sequence       INT;
    _stage_dexuat_id             INT;
    _stage_lose_id               INT;
    _first_day_of_month          DATE;
BEGIN

    IF _input_from_date IS NOT NULL AND _input_from_date != '' THEN
        _from_date = to_date(_input_from_date, 'dd/mm/yyyy');
    END IF;

    IF _input_to_date IS NOT NULL AND _input_to_date != '' THEN
        _to_date = to_date(_input_to_date, 'dd/mm/yyyy');
    END IF;

    _previous_month_date = ((date_trunc('month', _to_date::DATE) - interval '1 month')::DATE);
    _first_day_of_month = to_char(_to_date, 'YYYY-mm-01')::DATE;

    -- Lấy thông tin giai đoạn cơ hội
    WITH getListStageId AS (
        SELECT jsonb_object_agg(stage.code, stage.id)       json_ids,
               jsonb_object_agg(stage.code, stage.sequence) json_sequences
        FROM vnpt_crm_stage stage
        WHERE stage.code IN ('dexuat', 'kyket', 'quyettoan', 'khongthanhcong')
    )
    SELECT COALESCE((temp_stage.json_ids -> 'dexuat')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'dexuat')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'kyket')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'quyettoan')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'khongthanhcong')::INT, 0)
    INTO _stage_dexuat_id,
        _stage_dexuat_sequence,
        _stage_kyket_sequence,
        _stage_saunghiemthu_sequence,
        _stage_lose_id
    FROM getListStageId temp_stage;

    -- Tạo temp validate
    CREATE TEMP TABLE IF NOT EXISTS temp_dashboard_vnpt_crm_list_opportunity_ver2
    (
        opportunity_id                INT,     -- [Cơ hội kinh doanh] Id
        company_id                    INT,     -- [Cơ hội kinh doanh] Đơn vị
        proof_form                    VARCHAR, -- [Cơ hội kinh doanh] Hình thức chứng minh
        risk_assessment               VARCHAR, -- [Cơ hội kinh doanh] Mức độ rủi ro
        potential_level               VARCHAR, -- [Cơ hội kinh doanh] Mức độ tiềm năng
        estimated_budget              FLOAT,   -- [Cơ hội kinh doanh] Ngân sách dự toán (VNĐ)
--         previous_stage_id             INT,     -- [Cơ hội kinh doanh] Trạng thái cũ trước khi chuyển
--         log_date                      DATE,    -- [Cơ hội kinh doanh] Ngày tạo log chuyển trạng thái
        create_date                     DATE,  -- [Cơ hội kinh doanh] Ngày tạo cơ hội
        stage_id                      INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Id
        stage_code                    VARCHAR, -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Mã
        stage_sequence                INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Thứ tự
        previous_month_log_date       DATE,    -- [Cơ hội kinh doanh] Ngày tạo log chuyển trạng thái tháng trước
        previous_month_stage_id       INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Id tháng trước
        previous_month_stage_code     VARCHAR, -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Mã tháng trước
        previous_month_stage_sequence INT      -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Thứ tự tháng trước
    );
    TRUNCATE TABLE temp_dashboard_vnpt_crm_list_opportunity_ver2;

    -- Lấy dữ liệu Cơ hội kinh doanh hiện tại
    WITH
         table_service_multi AS (
             SELECT (CASE WHEN value_service_id <> '' THEN value_service_id::INT ELSE 0 END) value_service_id
              FROM UNNEST(STRING_TO_ARRAY(_service_id::text,';'::text)) AS value_service_id
         ),
         getListOpportunityUser AS (
             -- Lấy ra dữ liệu Cơ hội kinh doanh theo phân quyền
        SELECT temp_opp.opportunity_id,
               (opp.estimated_budget) AS estimated_budget,
               opp.company_id,
               opp.risk_assessment,
               opp.potential_level,
               opp.proof_form,
               opp.create_date, -- Ngày tạo
               opp.stage_id, -- Trạng thái hiện tại
--                temp_log.create_date::DATE                AS log_date,
--                temp_log.previous_stage_id,
--                temp_log.stage_id,
              ROW_NUMBER() OVER (PARTITION BY temp_opp.opportunity_id
                   ORDER BY opp.create_date DESC)   AS order_row
        FROM vnpt_crm_list_opportunity opp
                  -- KienCT tạm sửa sang hàm mới, lấy theo cả Company truyền vào
                JOIN fn_vnpt_get_list_opportunity_by_user_company(_user_id, _company_id,
                                                           null, null) temp_opp
                      ON temp_opp.opportunity_id = opp.id
--                 JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
--                       ON temp_log.opportunity_id = temp_opp.opportunity_id
                LEFT JOIN vnpt_crm_list_opportunity_list_service list_service
                                                ON list_service.opportunity_id = opp.id
                LEFT JOIN bid_sanpham_dv sanpham_dv ON sanpham_dv.id = list_service.service_id
                LEFT JOIN table_service_multi service_multi ON service_multi.value_service_id = sanpham_dv.id
                LEFT JOIN bid_nhom_sp nhom_sp ON nhom_sp.id = sanpham_dv.nhomspid
                LEFT JOIN rel_list_opportunity_city opportunity_city -- xử lý địa bàn
                                            ON opportunity_city.opportunity_id = opp.id
        WHERE
--               temp_log.create_date::DATE BETWEEN _from_date AND _to_date
--         AND temp_log.state = 'confirm'
          -- Tiêu chí tìm kiếm theo SPDV
--         AND
            (_service_id IS NULL OR _service_id = '0' OR (sanpham_dv.id IS NOT NULL AND list_service.id IS NOT NULL))
        AND (_nhom_sanpham_dv IS NULL OR _nhom_sanpham_dv = 0 OR (sanpham_dv.nhomspid = _nhom_sanpham_dv AND list_service.id IS NOT NULL))
        AND (_location_id IS NULL OR _location_id = 0 OR (opportunity_city.city_id = _location_id))
        AND (_level IS NULL OR _level = 0
                OR (
                    nhom_sp.id IS NOT NULL AND nhom_sp.level = _level
                )
            )
    ),
         -- Lấy trạng thái cơ hội tháng trước
         getListPreviousMonthOpportunity AS (
             SELECT temp_opp.opportunity_id,
                    temp_log.stage_id,
                    temp_log.create_date::DATE              AS log_date,
                    ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                        ORDER BY temp_log.create_date DESC) AS order_row
             FROM getListOpportunityUser temp_opp
                      JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                          ON temp_log.opportunity_id = temp_opp.opportunity_id
             WHERE temp_opp.order_row = 1
               AND temp_log.create_date <= _first_day_of_month
               AND temp_log.state = 'confirm'
               AND temp_log.is_reject_log IS NOT TRUE
         )
    INSERT
    INTO temp_dashboard_vnpt_crm_list_opportunity_ver2
    (opportunity_id, company_id, proof_form, risk_assessment, potential_level, estimated_budget,
--      previous_stage_id, log_date,
     create_date,
     stage_id, stage_code, stage_sequence,
     previous_month_log_date, previous_month_stage_id, previous_month_stage_code, previous_month_stage_sequence)
    SELECT temp_opp.opportunity_id,
           temp_opp.company_id,
           temp_opp.proof_form,
           temp_opp.risk_assessment,
           temp_opp.potential_level,
           temp_opp.estimated_budget,
--            temp_opp.previous_stage_id,
--            temp_opp.log_date,
           temp_opp.create_date,        -- Ngày tạo Cơ hội
           stage.id                   AS stage_id,
           stage.code                 AS stage_code,
           stage.sequence             AS stage_sequence,
           temp_opp_previous.log_date AS previous_month_log_date,
           stage_previous.id          AS previous_month_stage_id,
           stage_previous.code        AS previous_month_stage_code,
           stage_previous.sequence    AS previous_month_stage_sequence
    FROM getListOpportunityUser temp_opp
             INNER JOIN vnpt_crm_stage stage ON stage.id = temp_opp.stage_id -- trạng thái hiện tại
             LEFT JOIN getListPreviousMonthOpportunity temp_opp_previous
                       ON temp_opp_previous.opportunity_id = temp_opp.opportunity_id
                           AND temp_opp_previous.order_row = 1
             LEFT JOIN vnpt_crm_stage stage_previous ON stage_previous.id = temp_opp_previous.stage_id
    WHERE temp_opp.order_row = 1;

    RETURN QUERY
        SELECT json_build_object(
                       'current_status', TRUE,
                       'current_message', 'Thành công',
                       'current_data', json_build_object(
                               'list_current_process',
                               (
                                   CASE WHEN _page_load_data_chart = 0 THEN
                                    (SELECT *
                                   FROM fn_vnpt_dashboard_general_list_process(_dong_to_million,
                                                                                _stage_kyket_sequence,
                                                                               _stage_saunghiemthu_sequence,
                                                                               _stage_lose_id, _to_date,
                                                                               _first_day_of_month,
                                                                               _previous_month_date))
                                   ELSE
                                        NULL
                                   END
                               ),
                               'list_manager_total_by_stage',
                                (
                                    CASE WHEN _page_load_data_chart = 0 THEN
                                    -- Đã call sang bảng temp
                                             (SELECT *
                                              FROM fn_vnpt_dashboard_get_opportunity_total_by_stage(
                                                      _user_id,
                                                      _user_company_id,
                                                      0,
                                                      _service_id,
                                                      _input_from_date,
                                                      _input_to_date,
                                                      _type,
                                                      _company_id,
                                                      _location_id,
                                                      _level,
                                                      _nhom_sanpham_dv)
                                             )
                                    ELSE
                                        NULL
                                    END
                                ),
                                'list_manager_opportunity_stuck',
                                (
                                    CASE WHEN _page_load_data_chart = 0 THEN
                                    -- Đã call sang bảng temp
                                        (SELECT * FROM fn_vnpt_dashboard_get_opportunity_stuck(
                                            _user_id,
                                            _user_company_id,
                                            0,
                                            _service_id,
                                            _input_from_date,
                                            _input_to_date,
                                            _type,
                                            _company_id,
                                            _location_id,
                                            _level,
                                            _nhom_sanpham_dv))
                                    ELSE
                                        NULL
                                    END
                                ),
                               'list_div',
                               ('[' ||
                               -- Vẽ lần lượt các DIV
                                CASE WHEN _page_load_data_chart = 0 THEN
                                       -- Thống kê số lượng cơ hội lũy kế theo giai đoạn
                                         (json_build_object(
                                                  'start_div', '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                  'end_div', '</div>',
                                                  'data',
                                                  (
                                                      -- Đã call sang bảng temp
                                                      SELECT *
                                                      FROM fn_vnpt_dashboard_general_accumulate_opportunity_stage()
                                                  )
                                              )
                                              || ', ' ||
                                              --Thống kê số lượng hiện tại của cơ hội theo giai đoạn (bổ sung mới, sẽ mô tả bên dưới)
                                          json_build_object(
                                                  'start_div', '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                  'end_div', '</div>',
                                                  'data',
                                                  (
                                                      -- Đã call sang bảng temp
                                                      SELECT *
                                                      FROM fn_vnpt_dashboard_general_dashboard_get_opportunity_sum_current_stage()
                                                  )
                                              )
                                              || ', ' ||
                                              --Thống kê giá trị hợp đồng theo cơ hội lũy kế
                                          json_build_object(
                                                  'start_div', '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                  'end_div', '</div>',
                                                  'data',
                                                  (
                                                      -- Đã call sang bảng temp
                                                      SELECT *
                                                      FROM fn_vnpt_dashboard_general_cohoi_luyke_giatri_hopdong(
                                                              _dong_to_million,
                                                              _stage_kyket_sequence,
                                                              _stage_lose_id,
                                                              _from_date,
                                                              _to_date)
                                                  )
                                              )
                                              || ', ' ||
                                             -- Biểu đồ tỷ lệ chuyển đổi cơ hội thành công (Tổng mức)
                                          json_build_object(
                                                  'start_div', '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                  'end_div', '</div>',
                                                  'data',
                                                  (
                                                      -- Đã call sang bảng temp
                                                      SELECT *
                                                      FROM fn_vnpt_dashboard_get_opportunity_successful_conversion_total(
                                                              _user_id,
                                                              _user_company_id,
                                                              0,
                                                              _service_id,
                                                              _input_from_date,
                                                              _input_to_date,
                                                              _type,
                                                              _company_id,
                                                              _location_id,
                                                              _level,
                                                              _nhom_sanpham_dv)
                                                  )
                                              )
                                             )

                                    -- Kết thúc Page 0
                                --|| ', ' ||
                                 WHEN _page_load_data_chart = 1 THEN
                                         (
                                             -- 	Thống kê tình hình bán hàng theo tháng
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_dashboard_thongke_banhang_theothang(
                                                                                         _dong_to_million,
                                                                                         _from_date, _to_date,
                                                                                         _first_day_of_month,
                                                                                         _stage_kyket_sequence,
                                                                                         _stage_saunghiemthu_sequence)
                                                                             )
                                                                         )
                                                                     || ', ' ||
                                                                 -- Biểu đồ Thống kê tỷ lệ và số lượng cơ hội chuyển hoá thành công
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_cohoi_chuyenhoa_thanhcong(
                                                                                         _stage_kyket_sequence,
                                                                                         _stage_saunghiemthu_sequence,
                                                                                         _company_id,
                                                                                     _from_date,
                                                                                     _to_date)
                                                                             )
                                                                         )
                                                             || ', ' ||
                                                         -- Biểu đồ Thống kê giá trị hợp đồng các cơ hội chuyển hoá thành công
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_giatri_cohoi_chuyenhoa_thanhcong(
                                                                                         _dong_to_million,
                                                                                         _stage_lose_id,
                                                                                         _stage_kyket_sequence,
                                                                                         _stage_saunghiemthu_sequence,
                                                                                         _company_id,
                                                                                         _from_date, _to_date)
                                                                             )
                                                                         )
                                                     || ', ' ||

                                                 -- Biểu đồ phân bổ các mốc cơ hội theo Tỉnh
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_get_opportunity_milestone_by_province(
                                                                                         _user_id,
                                                                                         _user_company_id,
                                                                                         0,
                                                                                         _service_id,
                                                                                         _input_from_date,
                                                                                         _input_to_date,
                                                                                         _type,
                                                                                         _company_id,
                                                                                         _location_id,
                                                                                         _level,
                                                                                         _nhom_sanpham_dv)
                                                                             )
                                                                         )
                                             )
                                --|| ', ' ||
                                WHEN _page_load_data_chart = 2 THEN
                                         (
                                             -- THỐNG KÊ THEO SẢN PHẨM DỊCH VỤ
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_dashboard_thongke_spdv(_to_date)
                                                                             )
                                                                         )
                                                                     || ', ' ||

                                                                 -- Thống kê cơ hội phát sinh trong tháng
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_cohoi_phatsinh_trongthang(
                                                                                         _first_day_of_month, _type)
                                                                             )
                                                                         )
                                                             || ', ' ||

                                                         -- 	Số lượng hình thức chứng minh năng lực trong tháng
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_general_hinhthuc_nangluc_trongthang(
                                                                                         _first_day_of_month,
                                                                                         _stage_dexuat_id)
                                                                             )
                                                                         )
                                                     || ', ' ||

                                                 -- Phát sinh mới theo tháng
                                                                     json_build_object(
                                                                             'start_div',
                                                                             '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                             'end_div', '</div>',
                                                                             'data',
                                                                             (
                                                                                 -- Đã call sang bảng temp
                                                                                 SELECT *
                                                                                 FROM fn_vnpt_dashboard_get_opportunity_stage_new_by_month(
                                                                                         _user_id,
                                                                                         _user_company_id,
                                                                                         0,
                                                                                         _service_id,
                                                                                         _input_from_date,
                                                                                         _input_to_date,
                                                                                         _type,
                                                                                         _company_id,
                                                                                         _location_id,
                                                                                         _level,
                                                                                         _nhom_sanpham_dv)
                                                                             )
                                                                         )
                                             )
                                --|| ', ' ||
                                WHEN _page_load_data_chart = 3 THEN
                                 (
                                     -- Luỹ kế phát sinh mới theo tháng
                                                             json_build_object(
                                                                     'start_div',
                                                                     '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                     'end_div',
                                                                     '</div>',
                                                                     'data',
                                                                     (
                                                                         -- Đã call sang bảng temp
                                                                         SELECT *
                                                                         FROM fn_vnpt_dashboard_get_opportunity_stage_new_by_month_luyke(
                                                                                 _user_id,
                                                                                 _user_company_id,
                                                                                 0,
                                                                                 _service_id,
                                                                                 _input_from_date,
                                                                                 _input_to_date,
                                                                                 _type,
                                                                                 _company_id,
                                                                                 _location_id,
                                                                                 _level,
                                                                                 _nhom_sanpham_dv)
                                                                     )
                                                                 )
                                                             ||
                                                             ', ' ||
                                                         -- Luỹ kế giá trị cơ hội theo tháng
                                                             json_build_object(
                                                                     'start_div',
                                                                     '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                     'end_div',
                                                                     '</div>',
                                                                     'data',
                                                                     (
                                                                         -- Đã call sang bảng temp
                                                                         SELECT *
                                                                         FROM fn_vnpt_dashboard_get_opportunity_value_luyke_by_month(
                                                                                 _user_id,
                                                                                 _user_company_id,
                                                                                 0,
                                                                                 _service_id,
                                                                                 _input_from_date,
                                                                                 _input_to_date,
                                                                                 _type,
                                                                                 _company_id,
                                                                                 _location_id,
                                                                                 _level,
                                                                                 _nhom_sanpham_dv)
                                                                     )
                                                                 )
                                                     ||
                                                             ', ' ||
                                                 -- Tỷ lệ chuyển đổi cơ hội thành công theo tháng
                                                             json_build_object(
                                                                     'start_div',
                                                                     '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                     'end_div',
                                                                     '</div>',
                                                                     'data',
                                                                     (
                                                                         -- Đã call sang bảng temp
                                                                         SELECT *
                                                                         FROM fn_vnpt_dashboard_get_opportunity_transfor_implement_by_month(
                                                                                 _user_id,
                                                                                 _user_company_id,
                                                                                 0,
                                                                                 _service_id,
                                                                                 _input_from_date,
                                                                                 _input_to_date,
                                                                                 _type,
                                                                                 _company_id,
                                                                                 _location_id,
                                                                                 _level,
                                                                                 _nhom_sanpham_dv)
                                                                     )
                                                                 )
                                             ||
                                                             ', ' ||
                                         -- Giá trị cơ hội ký hợp đồng phát sinh theo tháng
                                                             json_build_object(
                                                                     'start_div',
                                                                     '<div class="col-sm-12 col-dashboard-custom">',
                                                                     'end_div',
                                                                     '</div>',
                                                                     'data',
                                                                     (
                                                                         -- Đã call sang bảng temp
                                                                         SELECT *
                                                                         FROM fn_vnpt_dashboard_get_opportunity_contract_value_new_by_month(
                                                                                 _user_id,
                                                                                 _user_company_id,
                                                                                 0,
                                                                                 _service_id,
                                                                                 _input_from_date,
                                                                                 _input_to_date,
                                                                                 _type,
                                                                                 _company_id,
                                                                                 _location_id,
                                                                                 _level,
                                                                                 _nhom_sanpham_dv)
                                                                     )
                                                                 )
                                     )
                                --|| ', ' ||
                                WHEN _page_load_data_chart = 4 THEN
                                                                             (
                                                                                 -- Giá trị cơ hội trúng thầu phát sinh theo tháng
                                                                                                         json_build_object(
                                                                                                                 'start_div',
                                                                                                                 '<div class="col-sm-12 col-dashboard-custom">',
                                                                                                                 'end_div',
                                                                                                                 '</div>',
                                                                                                                 'data',
                                                                                                                 (
                                                                                                                     -- Đã call sang bảng temp
                                                                                                                     SELECT *
                                                                                                                     FROM fn_vnpt_dashboard_get_opportunity_bid_win_new_by_month(
                                                                                                                             _user_id,
                                                                                                                             _user_company_id,
                                                                                                                             0,
                                                                                                                             _service_id,
                                                                                                                             _input_from_date,
                                                                                                                             _input_to_date,
                                                                                                                             _type,
                                                                                                                             _company_id,
                                                                                                                             _location_id,
                                                                                                                             _level,
                                                                                                                             _nhom_sanpham_dv)
                                                                                                                 )
                                                                                                             )
                                                                                                         || ', ' ||


                                                                                                     -- Giá trị cơ hội trượt thầu phát sinh theo tháng
                                                                                                         json_build_object(
                                                                                                                 'start_div',
                                                                                                                 '<div class="col-sm-12 col-dashboard-custom">',
                                                                                                                 'end_div',
                                                                                                                 '</div>',
                                                                                                                 'data',
                                                                                                                 (
                                                                                                                     -- Đã call sang bảng temp
                                                                                                                     SELECT *
                                                                                                                     FROM fn_vnpt_dashboard_get_opportunity_bid_lose_new_by_month(
                                                                                                                             _user_id,
                                                                                                                             _user_company_id,
                                                                                                                             0,
                                                                                                                             _service_id,
                                                                                                                             _input_from_date,
                                                                                                                             _input_to_date,
                                                                                                                             _type,
                                                                                                                             _company_id,
                                                                                                                             _location_id,
                                                                                                                             _level,
                                                                                                                             _nhom_sanpham_dv)
                                                                                                                 )
                                                                                                             )
                                                                                                 || ', ' ||

                                                                                             -- Giá trị cơ hội mở thầu phát sinh theo tháng (bổ sung)
                                                                                                         json_build_object(
                                                                                                                 'start_div',
                                                                                                                 '<div class="col-sm-12 col-dashboard-custom">',
                                                                                                                 'end_div',
                                                                                                                 '</div>',
                                                                                                                 'data',
                                                                                                                 (
                                                                                                                     -- Đã call sang bảng temp
                                                                                                                     SELECT *
                                                                                                                     FROM fn_vnpt_dashboard_general_dashboard_get_opp_bidopen_value_month
                                                                                                                         (
                                                                                                                             _input_from_date,
                                                                                                                             _input_to_date)
                                                                                                                 )
                                                                                                             )
                                                                                         || ', ' ||


                                                                                     -- Giá trị cơ hội kết thúc bất thường
                                                                                                         json_build_object(
                                                                                                                 'start_div',
                                                                                                                 '<div class="col-sm-12 col-dashboard-custom">',
                                                                                                                 'end_div',
                                                                                                                 '</div>',
                                                                                                                 'data',
                                                                                                                 (
                                                                                                                     -- Đã call sang bảng temp
                                                                                                                     SELECT *
                                                                                                                     FROM fn_vnpt_dashboard_get_opportunity_unusual_ending_new_by_month(
                                                                                                                             _user_id,
                                                                                                                             _user_company_id,
                                                                                                                             0,
                                                                                                                             _service_id,
                                                                                                                             _input_from_date,
                                                                                                                             _input_to_date,
                                                                                                                             _type,
                                                                                                                             _company_id,
                                                                                                                             _location_id,
                                                                                                                             _level,
                                                                                                                             _nhom_sanpham_dv)
                                                                                                                 )
                                                                                                             )
                                                                                 )
                                 WHEN _page_load_data_chart = 5 THEN
                                             (
                                                 --|| ', ' ||

                                                 -- Biểu đồ phân bổ cơ hội
                                                 -- 	Biểu đồ phân bổ dự án (Giá trị dự án) sửa tên
                                                                         json_build_object(
                                                                                 'start_div',
                                                                                 '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                                 'end_div', '</div>',
                                                                                 'data',
                                                                                 (
                                                                                     -- Đã call sang bảng temp
                                                                                     SELECT *
                                                                                     FROM fn_vnpt_dashboard_get_opportunity_project_allocation(
                                                                                             _user_id,
                                                                                             _user_company_id,
                                                                                             0,
                                                                                             _service_id,
                                                                                             _input_from_date,
                                                                                             _input_to_date,
                                                                                             _type,
                                                                                             _company_id,
                                                                                             _location_id,
                                                                                             _level,
                                                                                             _nhom_sanpham_dv)
                                                                                 )
                                                                             )
                                                                         || ', ' ||


                                                                     -- Biểu đồ mức đồ hoàn thành kế hoạch
                                                                     -- 	Biểu đồ phân bổ dự án và mức đô hoàn thành kế hoạch sửa tên thành
                                                                         json_build_object(
                                                                                 'start_div',
                                                                                 '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                                 'end_div', '</div>',
                                                                                 'data',
                                                                                 (
                                                                                     -- Đã call sang bảng temp
                                                                                     SELECT *
                                                                                     FROM fn_vnpt_dashboard_get_opportunity_project_allocation_completion(
                                                                                             _user_id,
                                                                                             _user_company_id,
                                                                                             0,
                                                                                             _service_id,
                                                                                             _input_from_date,
                                                                                             _input_to_date,
                                                                                             _type,
                                                                                             _company_id,
                                                                                             _location_id,
                                                                                             _level,
                                                                                             _nhom_sanpham_dv)
                                                                                 )
                                                                             )
                                                                 || ', ' ||

                                                             -- 	Biểu đồ phân loại lý do trượt thầu (bổ sung)
                                                                         json_build_object(
                                                                                 'start_div',
                                                                                 '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                                                                 'end_div', '</div>',
                                                                                 'data',
                                                                                 (
                                                                                     -- Đã call sang bảng temp
                                                                                     SELECT *
                                                                                     FROM fn_vnpt_dashboard_general_dashboard_get_opplose_cate_reason
                                                                                         (_from_date, _to_date)
                                                                                 )
                                                                             )
                                                         || ', ' ||

                                                     -- Thống kê theo phân loại tiềm năng rủi ro và quy mô dự toán
                                                                         json_build_object(
                                                                                 'start_div',
                                                                                 '<div class="col-sm-12 col-dashboard-custom">',
                                                                                 'end_div', '</div>',
                                                                                 'data',
                                                                                 (
                                                                                     SELECT *
                                                                                     FROM fn_vnpt_dashboard_general_dashboard_thongke_tiemnang_ruiro(_from_date, _to_date)
                                                                                 )
                                                                             )
                                                 )
                                    END
                                    -- Kết thúc mảng
                                   || ']'
                                )::JSON)
                            ,

                          'list_nhom_sp',
                          (
                              CASE
                                  WHEN _is_change_level = 1 THEN
                                      (
                                          SELECT ARRAY_AGG(json_build_object('id', id, 'name', tenhomsp))
                                          FROM bid_nhom_sp
                                          WHERE (_level IS NULL OR _level = 0 OR level = _level)
                                      )
                                  ELSE
                                      '{}'
                                  END
                          ),
                          'list_sanpham_dv',
                          (
                              CASE
                                  WHEN _is_change_level = 1 OR _is_change_nhom_sp = 1
                                      THEN
                                      (
                                          SELECT ARRAY_AGG(
                                                         json_build_object(
                                                                 'id',
                                                                 bid_sanpham_dv.id,
                                                                 'name',
                                                                 bid_sanpham_dv.tennoibo))
                                          FROM bid_sanpham_dv
                                                   INNER JOIN bid_nhom_sp ON bid_nhom_sp.id = bid_sanpham_dv.nhomspid
                                          WHERE (_is_change_level = 1 AND (_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level) )
                                             OR (_is_change_nhom_sp = 1 AND
                                                 ((_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level)
                                                     AND (_nhom_sanpham_dv IS NULL
                                                              OR _nhom_sanpham_dv = 0
                                                              OR bid_nhom_sp.id = _nhom_sanpham_dv)
                                                     )

                                                 )
                                      )
                                  ELSE
                                      '{}'
                                  END
                          )

                    );

    DROP TABLE temp_dashboard_vnpt_crm_list_opportunity_ver2;
END
$$ LANGUAGE PLPGSQL;

