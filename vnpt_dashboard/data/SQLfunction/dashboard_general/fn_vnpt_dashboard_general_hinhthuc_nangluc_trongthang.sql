-- hungvdh - [Dashboard chung] Số lượng hình thức chứng minh năng lực trong tháng
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_hinhthuc_nangluc_trongthang(DATE, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_hinhthuc_nangluc_trongthang(_first_day_of_month DATE,
                                                                                 _stage_dexuat_id INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH getListProof AS (
            SELECT temp_selec.name, temp_selec.value, temp_selec.sequence
            FROM ir_model_fields temp_field
                     JOIN ir_model_fields_selection temp_selec
                          ON temp_selec.field_id = temp_field.id
            WHERE temp_field.model = 'vnpt_crm_list_opportunity'
              AND temp_field.name = 'proof_form'
        ),
             getListProofData_3 AS (
                 SELECT temp_data.opportunity_id,
                        temp_data.proof_form,
                        ROW_NUMBER() OVER (PARTITION BY temp_data.opportunity_id, temp_data.proof_form
                            ORDER BY temp_data.opportunity_id) AS order_row
                 FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp_data
                          JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                               ON temp_log.opportunity_id = temp_data.opportunity_id
                 WHERE temp_log.create_date::DATE >= _first_day_of_month
                   AND temp_log.stage_id = _stage_dexuat_id
                   AND temp_log.state = 'confirm'
                   AND temp_log.is_reject_log IS NOT TRUE
                 GROUP BY temp_data.opportunity_id,
                          temp_data.proof_form
             ),
             getListProofDataCurrentMonth_3 AS (
                 SELECT temp_data.proof_form,
                        SUM(1) AS opportunity_number
                 FROM getListProofData_3 temp_data
                 WHERE temp_data.order_row = 1
                 GROUP BY temp_data.proof_form
             )
        SELECT json_build_object(
                       'element', 'opportunity_proof_form',
                       'icon', 'fa-bar-chart',
                       'title', 'Số lượng hình thức chứng minh năng lực trong tháng',
                       'type', 'bar',
                       'category',
                       (SELECT ARRAY_AGG(temp_proof.name ORDER BY temp_proof.sequence)
                        FROM getListProof temp_proof),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Số lượng',
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.opportunity_number > 0
                                                                  THEN temp_data.opportunity_number END
                                                          ORDER BY temp_proof.sequence))
                           || ']')::JSON
                        FROM getListProof temp_proof
                                 LEFT JOIN getListProofDataCurrentMonth_3 temp_data
                                           ON temp_data.proof_form = temp_proof.value));

END
$$ LANGUAGE PLPGSQL;