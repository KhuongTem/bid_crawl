-- hungvdh - [Dashboard chung] Thống kê số lượng cơ hội lũy kế theo giai đoạn
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_accumulate_opportunity_stage();
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_accumulate_opportunity_stage()
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN


    RETURN QUERY
      WITH getListStage AS (
        SELECT stage.id, stage.code, stage.name, stage.sequence
        FROM vnpt_crm_stage stage
      ),
      temp_previous_stage_id AS (
        SELECT list_opportunity_ver2.opportunity_id,
            list_opportunity_ver2.stage_id,
            temp_log.previous_stage_id,
            ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                    ORDER BY temp_log.create_date DESC)   AS order_row
        FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
            JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
            ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
        WHERE temp_log.state = 'confirm'
        AND temp_log.is_reject_log IS NOT TRUE
      ),
      -- Thông tin Thống kê số cơ hội lũy kế tới hiện tại
      getListOpportunityAccumulatedCurrent_1 AS (
        SELECT temp_stage.id AS stage_id,
        SUM(1)        AS opportunity_number
        FROM temp_previous_stage_id temp_data

          INNER JOIN getListStage stage ON stage.id = temp_data.stage_id
          LEFT JOIN getListStage temp_lose ON temp_lose.id = temp_data.previous_stage_id
          INNER JOIN getListStage temp_stage
          ON temp_stage.sequence <= CASE
          WHEN stage.code != 'khongthanhcong'
          THEN stage.sequence
          ELSE temp_lose.sequence END
        WHERE temp_data.order_row = 1
        GROUP BY temp_stage.id
      )
      SELECT json_build_object(
          'element', 'opportunity_stage_accumulate',
          'icon', 'fa-bar-chart',
          'title', 'Thống kê số lượng cơ hội lũy kế theo giai đoạn',
          'type', 'funnel',
          'other_option', json_build_object(
          'legend', true
          ),
          'category',
          (SELECT ARRAY_AGG(temp_stage.name ORDER BY temp_stage.sequence)
          FROM getListStage temp_stage),
          'value',
          (SELECT ('[' ||
          json_build_object(
          'name', 'Số lượng cơ hội',
          'data', json_agg(json_build_object('name',
          temp_stage.name,
          'y',
          COALESCE(temp_data.opportunity_number, 0)
          )
          ORDER BY temp_stage.sequence))
          || ']')::JSON
          FROM getListStage temp_stage
          LEFT JOIN getListOpportunityAccumulatedCurrent_1 temp_data
          ON temp_data.stage_id = temp_stage.id
          WHERE temp_stage.name != 'Không thành công'));

END
$$ LANGUAGE PLPGSQL;