-- hungvdh - [Dashboard chung] Thống kê giá trị hợp đồng các cơ hội chuyển hóa thành công
-- kienct90: 12/12/2022: user_company_id truyền từ
-- fn_vnpt_dashboard_general_dashboard_get_list_charts_data có thể khác đơn vị hiện tại của user
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_giatri_cohoi_chuyenhoa_thanhcong(INT, INT, INT, INT, INT, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_giatri_cohoi_chuyenhoa_thanhcong(_dong_to_million INT,
                                                                                      _stage_lose_id INT,
                                                                                      _stage_kyket_sequence INT,
                                                                                      _stage_saunghiemthu_sequence INT,
                                                                                      _user_company_id INT,
                                                                                      _from_date DATE, _to_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
        DECLARE
        _stage_moi_id      INT;
BEGIN
    _stage_moi_id = (SELECT id FROM vnpt_crm_stage WHERE code = 'moi' LIMIT  1);
    RETURN QUERY
        WITH RECURSIVE
            getListCompany_7 AS (
                SELECT pcom.id,
                       pcom.name,
                       1                    AS level,
                       CONCAT('/', pcom.id) AS path
                FROM res_company AS pcom
                WHERE pcom.id = _user_company_id
                UNION ALL
                SELECT ccom.id,
                       ccom.name,
                       lt.level + 1                  AS level,
                       CONCAT(lt.path, '/', ccom.id) AS path
                FROM res_company AS ccom
                         INNER JOIN getListCompany_7 AS lt
                                    ON ccom.parent_id = lt.id
                WHERE ccom.type = 'DV'
            ),
             temp_opportunity_all_by_end_date AS
                (
                    SELECT
                        list_opportunity_ver2.opportunity_id,
                           list_opportunity_ver2.company_id,
                           list_opportunity_ver2.stage_sequence,
                           temp_log.create_date,
                        ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                            ORDER BY temp_log.create_date DESC) AS order_row
                   FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                    JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                            ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
                    WHERE temp_log.state = 'confirm'
                    AND temp_log.create_date > _from_date AND temp_log.create_date < _to_date
                    AND temp_log.stage_id != _stage_moi_id
                    AND temp_log.is_reject_log IS NOT TRUE
                ),
            -- Lấy thông tin hợp đồng
            prepareListOpportunityContract_7 AS (
                SELECT temp_opp.opportunity_id,
                       SUM(cont.contract_value) / _dong_to_million     AS contract_value,
                       SUM(cont.giatrivnptthuchien) / _dong_to_million AS vnpt_get_value
                FROM temp_opportunity_all_by_end_date temp_opp
                         JOIN bid_hopdong_tuongtu cont
                              ON cont.opportunity_id = temp_opp.opportunity_id
                WHERE temp_opp.order_row = 1
                  AND temp_opp.stage_sequence >= _stage_kyket_sequence
                  AND temp_opp.stage_sequence != _stage_lose_id
                  --AND cont.ngaybatdauhd BETWEEN _from_date AND _to_date
                GROUP BY temp_opp.opportunity_id
            ),
            prepareListOpportunitySuccessRate_7 AS (
                SELECT temp_data.company_id,
                       SUM(temp_cont.contract_value) AS contract_value,
                       SUM(temp_cont.vnpt_get_value) AS vnpt_get_value
                FROM temp_opportunity_all_by_end_date temp_data
                         LEFT JOIN prepareListOpportunityContract_7 temp_cont
                                   ON temp_cont.opportunity_id = temp_data.opportunity_id
                WHERE
                      temp_data.order_row = 1
                AND temp_data.stage_sequence BETWEEN _stage_kyket_sequence AND _stage_saunghiemthu_sequence
                GROUP BY temp_data.company_id
            ),
            -- Thống kê tỉ lệ và số lượng cơ hội chuyển hóa thành công
            getListOpportunitySuccessRate_7 AS (
                SELECT temp_com.id,
                       temp_com.name,
                       temp_com.level,
                       SUM(temp_opp.contract_value) AS contract_value,
                       SUM(temp_opp.vnpt_get_value) AS vnpt_get_value
                FROM prepareListOpportunitySuccessRate_7 temp
                         JOIN getListCompany_7 temp_com ON temp_com.id = temp.company_id
                         JOIN (SELECT opp.company_id,
                                      t.path,
                                      opp.contract_value,
                                      opp.vnpt_get_value
                               FROM getListCompany_7 t
                                        JOIN prepareListOpportunitySuccessRate_7 opp ON opp.company_id = t.id) AS temp_opp
                              ON temp_opp.path LIKE temp_com.path || '%'
                WHERE temp_com.level IN (1, 2)
                GROUP BY temp_com.id,
                         temp_com.name,
                         temp_com.level
            )
        SELECT json_build_object(
                       'element', 'opportunity_success_rate_contract_value',
                       'icon', 'fa-bar-chart',
                       'title',
                       'Thống kê giá trị hợp đồng các cơ hội chuyển hóa thành công',
                       'type', 'column',
                       'other_option', json_build_object(
                               'legend', true,
                               'dataLabels', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_service.name ORDER BY temp_service.level)
                           FROM getListOpportunitySuccessRate_7 temp_service
                       ),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Tổng giá trị hợp đồng',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.contract_value > 0
                                                                  THEN temp_data.contract_value END
                                                          ORDER BY temp_data.level))
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Giá trị hợp đồng VNPT',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.vnpt_get_value > 0
                                                                  THEN temp_data.vnpt_get_value END
                                                          ORDER BY temp_data.level))
                           || ']')::JSON
                        FROM getListOpportunitySuccessRate_7 temp_data
                        WHERE (temp_data.contract_value > 0 OR temp_data.vnpt_get_value > 0)
                       ));

END
$$ LANGUAGE PLPGSQL;