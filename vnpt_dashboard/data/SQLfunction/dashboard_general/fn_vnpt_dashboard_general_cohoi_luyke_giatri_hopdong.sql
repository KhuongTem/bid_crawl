-- hungvdh - [Dashboard chung] Thống kê giá trị hợp đồng theo cơ hội lũy kế
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_cohoi_luyke_giatri_hopdong(INT, INT, INT, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_cohoi_luyke_giatri_hopdong(_dong_to_million INT,
                                                                                _stage_kyket_sequence INT,
                                                                                _stage_lose_sequence_id INT,
                                                                                _from_date DATE, _to_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
    DECLARE
    _sequence_nghiemthu                   INT;
BEGIN
    SELECT COALESCE(stage.sequence, 0)
    INTO _sequence_nghiemthu
    FROM vnpt_crm_stage stage
    WHERE stage.code = 'nghiemthu' LIMIT 1;

    RETURN QUERY
        WITH getListStage AS (
            SELECT stage.id, stage.code, stage.name, stage.sequence
            FROM vnpt_crm_stage stage order by stage.sequence
        ),
             -- Lấy thông tin hợp đồng
             prepareListOpportunityContract_2 AS (
                 SELECT temp_opp.opportunity_id,
                        SUM(cont.contract_value) / _dong_to_million     AS contract_value,
                        SUM(cont.giatrivnptthuchien) / _dong_to_million AS vnpt_get_value
                 FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp_opp
                          JOIN bid_hopdong_tuongtu cont
                               ON cont.opportunity_id = temp_opp.opportunity_id
                 WHERE temp_opp.stage_sequence >= _stage_kyket_sequence
                   AND temp_opp.stage_sequence != _stage_lose_sequence_id
                   --AND cont.ngaybatdauhd BETWEEN _from_date AND _to_date -- Nghiên cứu lại cái này ngày
                 GROUP BY temp_opp.opportunity_id
             ),
             -- Trạng thái trước gần nhất.
             temp_previous_stage_id AS (
                 SELECT list_opportunity_ver2.opportunity_id,
                    list_opportunity_ver2.stage_id,
                    list_opportunity_ver2.estimated_budget,
                    temp_log.previous_stage_id,
                    ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                            ORDER BY temp_log.create_date DESC)   AS order_row
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                    JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                    ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
                WHERE temp_log.state = 'confirm'
                AND temp_log.is_reject_log IS NOT TRUE
             ),
             -- Thông tin Thống kê số cơ hội, giá trị hợp đồng lũy kế tới hiện tại
             getListOpportunityAccumulatedCurrent_2 AS (
                 SELECT temp_stage.id                   AS stage_id,
                        SUM(temp_data.estimated_budget) AS estimated_budget,
                        SUM(temp_cont.contract_value)   AS contract_value,
                        SUM(temp_cont.vnpt_get_value)   AS vnpt_get_value
                 FROM temp_previous_stage_id temp_data
                          INNER JOIN getListStage stage ON stage.id = temp_data.stage_id
                          LEFT JOIN getListStage temp_lose ON temp_lose.id = temp_data.previous_stage_id
                          INNER JOIN getListStage temp_stage
                                     ON temp_stage.sequence <= CASE
                                                                   WHEN stage.code != 'khongthanhcong'
                                                                       THEN stage.sequence
                                                                   ELSE temp_lose.sequence END
                          LEFT JOIN prepareListOpportunityContract_2 temp_cont
                                    ON temp_cont.opportunity_id = temp_data.opportunity_id
                 WHERE temp_data.order_row = 1
                 GROUP BY temp_stage.id
             )
        SELECT json_build_object(
                       'element', 'opportunity_contract_value_accumulate',
                       'icon', 'fa-bar-chart',
                       'title', 'Thống kê giá trị hợp đồng theo cơ hội',
                       'type', 'column',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category',
                       (SELECT ARRAY_AGG(temp_stage.name ORDER BY temp_stage.sequence)
                        FROM getListStage temp_stage),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Tổng dự toán',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.estimated_budget > 0
                                                                  THEN round(temp_data.estimated_budget::DECIMAL, 2) END
                                                          ))
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Giá trị hợp đồng',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(
                                                CASE
                                                    WHEN temp_stage.sequence < _sequence_nghiemthu THEN NULL
                                                    WHEN temp_data.contract_value > 0
                                                        THEN round(temp_data.contract_value::DECIMAL, 2)
                                                    END
                                                ))
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Giá trị VNPT được hưởng',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_stage.sequence < _sequence_nghiemthu THEN NULL
                                                              WHEN temp_data.vnpt_get_value > 0
                                                                  THEN round(temp_data.vnpt_get_value::DECIMAL, 2) END
                                                          ))
                           || ']')::JSON
                        FROM getListStage temp_stage
                                 LEFT JOIN getListOpportunityAccumulatedCurrent_2 temp_data
                                           ON temp_data.stage_id = temp_stage.id
                        WHERE temp_stage.name != 'Không thành công'));

END
$$ LANGUAGE PLPGSQL;