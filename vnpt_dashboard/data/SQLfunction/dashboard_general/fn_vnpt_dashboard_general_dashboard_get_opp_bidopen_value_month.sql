-- Giá trị cơ hội mở thầu phát sinh theo tháng
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_get_opp_bidopen_value_month
(VARCHAR, VARCHAR);
CREATE OR REPLACE FUNCTION  fn_vnpt_dashboard_general_dashboard_get_opp_bidopen_value_month(
_input_from_date      VARCHAR,
_input_to_date        VARCHAR
)
    returns TABLE(output_data json)
    language plpgsql
as
$$
DECLARE
    _from_date                      DATE = to_char(date_trunc('year', CURRENT_DATE + INTERVAL '-1 year'), 'yyyy-mm-dd')::DATE;
    _to_date                        DATE = CURRENT_DATE;
    _year_of_from_date              INT;
    _year_of_end_date               INT;
    _year_next                      INT := 0;
    _month_of_from_date             INT;
    _month_of_end_date              INT;
    _month_next                     INT := 0;

    _stage_hinhthanhcohoikhathi                     INT;
    _stage_dauthau                                  INT;
BEGIN
    -- Note:
    -- Tổng giá trị các cơ hội đang ở giai đoạn Đấu thầu và được cập nhật kết quả thầu là
    -- “Trượt thầu” trong tháng tương ứng.
    -- Giá trị lấy theo thông tin “Giá trị dự toán gói thầu” trong Tab Đấu thầu

    IF _input_from_date IS NOT NULL AND _input_from_date != '' THEN
        _from_date = to_date(_input_from_date, 'dd/mm/yyyy');
    END IF;

    IF _input_to_date IS NOT NULL AND _input_to_date != '' THEN
        _to_date = to_date(_input_to_date, 'dd/mm/yyyy');
    END IF;

    _year_of_from_date      = (SELECT DATE_PART('YEAR', _from_date::date));
    _year_of_end_date       = (SELECT DATE_PART('YEAR', _to_date::date));
    _year_next              = _year_of_from_date;

    _month_of_from_date     = (SELECT DATE_PART('MONTH', _from_date::date));
    _month_of_end_date      = (SELECT DATE_PART('MONTH', _to_date::date));
    _month_next             = _month_of_from_date;

    _stage_hinhthanhcohoikhathi             = (SELECT id FROM vnpt_crm_stage WHERE code = 'danhgia' LIMIT 1);
    _stage_dauthau                          = (SELECT id FROM vnpt_crm_stage WHERE code = 'dauthau' LIMIT 1);

    CREATE TEMP TABLE list_month_opportunity (name VARCHAR(255), start_date DATE, end_date DATE);
    WHILE _year_next <= _year_of_end_date LOOP
        IF _year_next < _year_of_end_date THEN
            WHILE _month_next <= 12 LOOP
                INSERT INTO list_month_opportunity
                    SELECT '' || _month_next || '/' || _year_next,
                           to_date('01/'||_month_next || '/' || _year_next, 'dd/mm/yyyy'),
                           (
                                CASE WHEN _month_next = 12 THEN
                                    (
                                        to_date('01/01/'|| (_year_next + 1), 'dd/mm/yyyy')
                                        + INTERVAL '-1 day'
                                    )
                                ELSE
                                    (
                                        to_date('01/'||(_month_next +1) || '/' || _year_next, 'dd/mm/yyyy')
                                        + INTERVAL '-1 day'
                                    )
                                END
                            )
                           ;
                _month_next = _month_next + 1;
            END LOOP;

            _month_next = 1;
        ELSE
            WHILE _month_next <= _month_of_end_date LOOP
                INSERT INTO list_month_opportunity
                    SELECT '' || _month_next || '/' || _year_next,
                           to_date('01/'||_month_next || '/' || _year_next, 'dd/mm/yyyy'),
                           (
                                CASE WHEN _month_next = 12 THEN
                                    (
                                        to_date('01/01/'|| (_year_next + 1), 'dd/mm/yyyy')
                                        + INTERVAL '-1 day'
                                    )
                                ELSE
                                    (
                                        to_date('01/'||(_month_next +1) || '/' || _year_next, 'dd/mm/yyyy')
                                        + INTERVAL '-1 day'
                                    )
                                END
                            )
                           ;
                _month_next = _month_next + 1;
            END LOOP;
        END IF;
        _year_next = _year_next + 1;
    END LOOP;

    RETURN QUERY
    WITH
    table_dauthau AS
    (
        -- Bảng lấy log từ Hình thành cơ hội khả thi, Chuyển sang Đấu thầu
        SELECT log_stage.opportunity_id,
               log_stage.create_date,
               trungthau.trangthai trungthau_trangthai,
               COALESCE(trungthau.giagoithau, 0) trungthau_estimate_value,
               trungthau.date_bid_success,
               trungthau.date_bid_fail,
               opportunity_city.city_id
        FROM vnpt_crm_list_opportunity_list_log_stage log_stage
        INNER JOIN temp_dashboard_vnpt_crm_list_opportunity_ver2 opportunity_by_user ON opportunity_by_user.opportunity_id = log_stage.opportunity_id
        LEFT JOIN bid_goithau_dacoketqua_dauthau trungthau ON trungthau.opportunity_id = opportunity_by_user.opportunity_id
        LEFT JOIN rel_list_opportunity_city opportunity_city -- xử lý địa bàn
                                            ON opportunity_city.opportunity_id = opportunity_by_user.opportunity_id
        WHERE
        log_stage.stage_id = _stage_dauthau
        AND log_stage.previous_stage_id = _stage_hinhthanhcohoikhathi
        AND log_stage.state = 'confirm'
        AND log_stage.is_reject_log IS NOT TRUE
        GROUP BY log_stage.opportunity_id, log_stage.create_date,
               trungthau.trangthai ,
               trungthau.giagoithau,
               trungthau.date_bid_success,
               trungthau.date_bid_fail,
               opportunity_city.city_id
    ),
    table_thanhpho AS
    (
        SELECT id, CAST(('00'::varchar || code) AS VARCHAR(255)) AS code, name FROM bid_tinh_thanhpho WHERE code = 'TW'
        UNION ALL
        SELECT id, code, name FROM bid_tinh_thanhpho
        WHERE code != 'TW'
        ORDER BY code ASC
    )
    SELECT json_build_object(
        'element', 'opportunity_bid_open_value_month_chart',
                       'icon', 'fa-bar-chart',
                       'title', 'Giá trị cơ hội mở thầu phát sinh theo tháng',
                       'type', 'column',
                       'other_option', json_build_object(
                               'legend', true,
                           'dataLabels', true,
                           'stacking', 'normal'
                           ),
        'category',
        (SELECT ARRAY_AGG(table_thanhpho.name) FROM table_thanhpho),
        'value',
        ARRAY_AGG(
                    json_build_object('name', month_opportunity.name,
                        'data',
                        (
                            (SELECT ARRAY_AGG(
                                (
                                    COALESCE ((SELECT SUM(COALESCE(dauthau.trungthau_estimate_value, 0))

                                    FROM table_dauthau dauthau
                                    WHERE  dauthau.city_id = thanhpho.id
                                    --AND dauthau.trungthau_trangthai NOT IN ('TRUNG', 'TRUOT')
                                    AND dauthau.create_date BETWEEN
                                                    month_opportunity.start_date AND month_opportunity.end_date
                                    AND (dauthau.date_bid_fail IS NULL OR dauthau.date_bid_fail NOT BETWEEN
                                                    month_opportunity.start_date AND month_opportunity.end_date)
                                    AND (dauthau.date_bid_success IS NULL OR dauthau.date_bid_success NOT BETWEEN
                                                    month_opportunity.start_date AND month_opportunity.end_date)
                                    ), 0)

                                )
                            ) FROM table_thanhpho thanhpho)
                        )
                    )
                )

    ) FROM list_month_opportunity month_opportunity;

    -- Xóa bảng temp
    DROP TABLE list_month_opportunity;
END
$$;
