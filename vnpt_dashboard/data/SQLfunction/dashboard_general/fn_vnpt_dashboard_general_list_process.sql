-- hungvdh - [Dashboard chung] Tiến trình cơ hội dạng ống
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_list_process(INT, INT, INT, INT, DATE, DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_list_process(_dong_to_million INT,
                                                                  _stage_kyket_sequence INT,
                                                                  _stage_saunghiemthu_sequence INT,
                                                                  _stage_lose_sequence_id INT,
                                                                  _to_date DATE,
                                                                  _first_day_of_month DATE,
                                                                  _previous_month_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN


    RETURN QUERY
        WITH
            -- Lấy thông tin giá trị hợp đồng hiện tại
            lcpPrepareListOpportunityBid AS (
                SELECT SUM(contract.contract_value) / _dong_to_million AS contract_value
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp_opp
                         JOIN bid_hopdong_tuongtu contract
                              ON contract.opportunity_id = temp_opp.opportunity_id
                WHERE temp_opp.stage_sequence >= _stage_kyket_sequence
                  AND temp_opp.stage_sequence != _stage_lose_sequence_id
            ),
            -- Lấy thông tin giá trị hợp đồng tháng trước
            lcpPrepareListOpportunityBidPrevious AS (
                SELECT SUM(contract.contract_value) / _dong_to_million AS contract_value
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp_opp
                         JOIN bid_hopdong_tuongtu contract
                              ON contract.opportunity_id = temp_opp.opportunity_id
                WHERE temp_opp.previous_month_stage_sequence >= _stage_kyket_sequence
                  AND temp_opp.previous_month_stage_sequence != _stage_lose_sequence_id
            ),
            -- Là tổng dự toán của tất cả cơ hội không gồm trạng thái “Không thành công”
            lcpGetListOpportunitySuccessLose AS (
                SELECT SUM(CASE
                               WHEN temp_opp.stage_code != 'khongthanhcong'
                                   THEN temp_opp.estimated_budget
                               ELSE 0 END)                                                     AS estimated_budget,
                       SUM(1)                                                                  AS opportunity_number,
                       SUM(CASE
                               WHEN temp_opp.previous_month_log_date < _first_day_of_month
                                   THEN 1
                               ELSE 0 END)                                                     AS opportunity_number_previous,
                       SUM(CASE
                               WHEN temp_opp.stage_sequence BETWEEN _stage_kyket_sequence AND _stage_saunghiemthu_sequence
                                   THEN 1
                               ELSE 0 END)                                                     AS opportunity_number_success,
                       SUM(CASE WHEN temp_opp.stage_code = 'khongthanhcong' THEN 1 ELSE 0 END) AS opportunity_number_lose
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 temp_opp
            ),
            -- Thông tin Dữ liệu tổng hợp
            lcpGetListFluctuationsMonthCurrent AS (
                SELECT temp_bid.contract_value                       AS current_accumulated_value,
                       temp_bid_previous.contract_value              AS previous_accumulated_value,
                       temp_success_lose.estimated_budget / _dong_to_million           AS process_current_estimates_value,
                       temp_success_lose.opportunity_number          AS process_current_number,
                       temp_success_lose.opportunity_number_previous AS process_previous_number,
                       temp_success_lose.opportunity_number_success  AS process_success_accumulated_number,
                       temp_success_lose.opportunity_number_lose     AS process_lose_accumulated_number
                FROM unnest(ARRAY [to_char(_to_date, 'mm/yyyy')]) temp_current(month_name)
                         LEFT JOIN lcpPrepareListOpportunityBid temp_bid ON TRUE
                         LEFT JOIN lcpPrepareListOpportunityBidPrevious temp_bid_previous ON TRUE
                         LEFT JOIN lcpGetListOpportunitySuccessLose temp_success_lose ON TRUE
            )
        SELECT json_build_object(
                       'process_current_month',
                       to_char(_to_date, 'mm/yyyy'),
                       'process_previous_month',
                       to_char(_previous_month_date, 'mm/yyyy'),
                       'process_current_accumulated_value',
                       fn_vnpt_format_vietnamese_dong(
                               COALESCE(temp_data.current_accumulated_value, 0)),
                       'process_current_accumulated_percentage',
                       CASE
                           WHEN temp_data.previous_accumulated_value IS NOT NULL
                               AND
                                temp_data.previous_accumulated_value > 0
                               THEN
                               CONCAT(
                                       ((COALESCE(temp_data.current_accumulated_value, 0) * 100) /
                                        temp_data.previous_accumulated_value)::INT,
                                       '')
                           ELSE '' END,
                       'process_current_estimates_value',
                       fn_vnpt_format_vietnamese_dong(
                               COALESCE(temp_data.process_current_estimates_value, 0)),
                       'process_current_number',
                       COALESCE(temp_data.process_current_number, 0),
                       'process_previous_number',
                       COALESCE(temp_data.process_previous_number, 0),
                       'process_success_accumulated_number',
                       COALESCE(temp_data.process_success_accumulated_number, 0),
                       'process_lose_accumulated_number',
                       COALESCE(temp_data.process_lose_accumulated_number, 0),
                       'process_change_proportion',
                       CASE
                           WHEN temp_data.process_current_number IS NOT NULL
                               AND
                                temp_data.process_current_number > 0
                               THEN
                               CONCAT(
                                       ((COALESCE(temp_data.process_success_accumulated_number, 0) * 100) /
                                        temp_data.process_current_number)::INT,
                                       '')
                           ELSE '' END
                   )
        FROM lcpGetListFluctuationsMonthCurrent temp_data;

END
$$ LANGUAGE PLPGSQL;




