-- hungvdh - [Dashboard chung] Thống kê tình hình bán hàng theo tháng
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_thongke_banhang_theothang(INT, DATE, DATE, DATE, INT, INt);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_thongke_banhang_theothang(_dong_to_million INT,
                                                                                         _from_date DATE, _to_date DATE,
                                                                                         _first_day_of_month DATE,
                                                                                         _stage_kyket_sequence INT,
                                                                                         _stage_saunghiemthu_sequence INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
    DECLARE  _stage_kyket      INT;
        _stage_saunghiemthu      INT;
BEGIN
    --_first_day_of_month = _first_day_of_month + INTERVAL '1 month';
    _stage_kyket = (SELECT id FROM vnpt_crm_stage WHERE code = 'kyket' LIMIT  1);
    _stage_saunghiemthu = (SELECT id FROM vnpt_crm_stage WHERE code = 'quyettoan' LIMIT  1);
    RETURN QUERY
        WITH getListMonth_9 AS (
            SELECT to_char(a.month_name, 'mm/yyyy') AS month_name,
            a.sequence

            FROM generate_series(
                         date_trunc('month',
                                    to_char(date_trunc('year', _from_date), 'yyyy-mm-dd')::DATE),
                         _to_date, '1 month'
                     )
                     WITH ORDINALITY a(month_name, sequence)
        ),
            temp_banhang AS (
                SELECT list_opportunity_ver2.opportunity_id,
                        list_opportunity_ver2.stage_sequence,
                        ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                            ORDER BY temp_log.create_date DESC)   AS order_row
                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                    ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
                    AND temp_log.create_date BETWEEN _from_date AND _to_date
                    AND temp_log.stage_id = _stage_saunghiemthu
                    AND temp_log.previous_stage_id = _stage_kyket
                    AND temp_log.state = 'confirm'
                    AND temp_log.is_reject_log IS NOT TRUE
            ),
             -- Lấy thông tin hợp đồng
             prepareListOpportunityContract_9 AS (
                 SELECT to_char(cont.ngaybatdauhd, 'mm/yyyy')           AS contract_date,
                        SUM(cont.contract_value) / _dong_to_million     AS contract_value,
                        SUM(CASE
                                WHEN cont.ngaybatdauhd < _first_day_of_month
                                    THEN cont.contract_value
                                ELSE 0 END) /
                        _dong_to_million                                AS previous_contract_value,
                        SUM(cont.giatrivnptthuchien) / _dong_to_million AS vnpt_get_value
                 FROM temp_banhang temp_opp
                          JOIN bid_hopdong_tuongtu cont
                               ON cont.opportunity_id = temp_opp.opportunity_id
                 WHERE temp_opp.order_row = 1
--                        temp_opp.stage_sequence BETWEEN _stage_kyket_sequence AND _stage_saunghiemthu_sequence
--                    AND cont.ngaybatdauhd BETWEEN _from_date AND _to_date
                 GROUP BY to_char(cont.ngaybatdauhd, 'mm/yyyy')
             ),
             getListFluctuationsMonthSell_9 AS (
                 SELECT temp_month.month_name                                               AS month_name,
                        temp_data.contract_value,
                        SUM(temp_data.contract_value) OVER (ORDER BY temp_month.sequence) AS accumulated_value
                 FROM getListMonth_9 temp_month
                          LEFT JOIN prepareListOpportunityContract_9 temp_data
                                    ON temp_data.contract_date = temp_month.month_name
             )
        SELECT json_build_object(
                       'element',
                       'fluctuations_month_sell',
                       'icon', 'fa-line-chart',
                       'title',
                       'Thống kê tình hình bán hàng theo tháng',
                       'type', 'xy',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_month.month_name ORDER BY temp_month.sequence)
                           FROM getListMonth_9 temp_month
                       ),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Giá trị hợp đồng',
                                        'type', 'column',
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.contract_value > 0
                                                                  THEN temp_data.contract_value
                                                              ELSE 0 END
                                                          ORDER BY temp_month.sequence))
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Giá trị lũy kế',
                                        'type', 'spline',
                                        'yAxis', 1,
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data', ARRAY_AGG(CASE
                                                              WHEN temp_data.accumulated_value > 0
                                                                  THEN temp_data.accumulated_value
                                                              ELSE 0 END
                                                          ORDER BY temp_month.sequence))
                           || ']')::JSON
                        FROM getListMonth_9 temp_month
                                 LEFT JOIN getListFluctuationsMonthSell_9 temp_data
                                           ON temp_data.month_name = temp_month.month_name));

END
$$ LANGUAGE PLPGSQL;