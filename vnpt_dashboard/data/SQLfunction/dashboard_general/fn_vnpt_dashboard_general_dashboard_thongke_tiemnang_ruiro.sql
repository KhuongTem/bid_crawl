-- hungvdh - [Dashboard chung] Thống kê theo phân loại tiềm năng rủi ro và quy mô dự toán
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_thongke_tiemnang_ruiro(DATE, DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_thongke_tiemnang_ruiro(_from_date DATE,
                                                                                        _to_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH getListStage_8 AS (
            SELECT stage.id, stage.code, stage.name, stage.sequence
            FROM vnpt_crm_stage stage
        ),
             getListRiskAssessment_8 AS (
                 SELECT CONCAT('RR ', temp_selec.name) AS name,
                        temp_selec.value,
                        temp_selec.sequence
                 FROM ir_model_fields temp_field
                          JOIN ir_model_fields_selection temp_selec
                               ON temp_selec.field_id = temp_field.id
                 WHERE temp_field.model = 'vnpt_crm_list_opportunity'
                   AND temp_field.name = 'risk_assessment'
             ),
             getListRiskPotential_8 AS (
                 SELECT CONCAT('TN ', temp_selec.name)   AS potential_name,
                        temp_risk.name                   AS risk_name,
                        temp_selec.value                 AS potential_level,
                        temp_risk.value                  AS risk_assessment,
                        ROW_NUMBER() OVER
                            (ORDER BY temp_selec.sequence,
                                temp_risk.sequence DESC) AS sequence
                 FROM ir_model_fields temp_poten
                          JOIN ir_model_fields_selection temp_selec
                               ON temp_selec.field_id = temp_poten.id
                          JOIN getListRiskAssessment_8 temp_risk ON TRUE
                 WHERE temp_poten.model = 'vnpt_crm_list_opportunity'
                   AND temp_poten.name = 'potential_level'
             ),
             -- Thời gian thống kê: so sánh với tháng đánh giá của cơ hội
             temp_list_opportunity AS (
                 SELECT
                        list_opportunity_ver2.opportunity_id,
                        list_opportunity_ver2.stage_id,
                        list_opportunity_ver2.potential_level,
                        list_opportunity_ver2.risk_assessment
                        FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                 JOIN vnpt_crm_list_opportunity_list_evaluate list_evaluate
                     ON list_evaluate.opportunity_id = list_opportunity_ver2.opportunity_id
                    AND (
                        CASE WHEN list_evaluate.evaluate_year > (DATE_PART('year', _from_date)::INT) THEN True
                            WHEN list_evaluate.evaluate_year = (DATE_PART('year', _from_date)::INT)
                                AND list_evaluate.evaluate_month >= (DATE_PART('month', _from_date)::INT) THEN True
                            ELSE False
                        END
                        )
                    AND (
                        CASE WHEN list_evaluate.evaluate_year < (DATE_PART('year', _to_date)::INT) THEN True
                            WHEN list_evaluate.evaluate_year = (DATE_PART('year', _to_date)::INT)
                                     AND list_evaluate.evaluate_month <= (DATE_PART('month', _to_date)::INT) THEN True
                            ELSE False
                        END)
                 GROUP BY list_opportunity_ver2.opportunity_id,
                        list_opportunity_ver2.stage_id,
                        list_opportunity_ver2.potential_level,
                        list_opportunity_ver2.risk_assessment
             ),
             prepareistRiskPotentialData_8 AS (
                 SELECT temp_opp.stage_id,
                        CONCAT(temp_rp.potential_name, ' - ', temp_rp.risk_name) AS risk_potential,
                        COUNT(1)                                                 AS opportunity_number,
                        temp_rp.sequence
                 FROM getListRiskPotential_8 temp_rp
                          LEFT JOIN temp_list_opportunity temp_opp
                                    ON temp_opp.potential_level = temp_rp.potential_level
                                        AND
                                       temp_opp.risk_assessment = temp_rp.risk_assessment
                 GROUP BY temp_opp.stage_id,
                          CONCAT(temp_rp.potential_name, ' - ', temp_rp.risk_name),
                          temp_rp.sequence
             ),
             getListRiskPotentialLevelCategory_8 AS (
                 SELECT temp_data.risk_potential,
                        temp_data.sequence
                 FROM prepareistRiskPotentialData_8 temp_data
                 GROUP BY temp_data.risk_potential,
                          temp_data.sequence
             ),
             getListRiskPotentialData_8 AS (
                 SELECT temp_stage.id,
                        ARRAY_AGG(temp_data.opportunity_number ORDER BY temp_cate.sequence) AS opportunity_number
                 FROM getListStage_8 temp_stage
                          INNER JOIN getListRiskPotentialLevelCategory_8 temp_cate ON true
                          LEFT JOIN prepareistRiskPotentialData_8 temp_data
                                    ON temp_data.stage_id = temp_stage.id
                                        AND
                                       temp_data.risk_potential = temp_cate.risk_potential
                 GROUP BY temp_stage.id
             )
        SELECT json_build_object(
                       'element', 'opportunity_risk_potential_level',
                       'icon', 'fa-bar-chart',
                       'title',
                       'Thống kê theo phân loại tiềm năng rủi ro và quy mô dự toán',
                       'type', 'column',
                       'other_option', json_build_object(
                               'legend', true,
                               'dataLabels', true,
                               'stacking', 'normal',
                               'rotation', -60
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_service.risk_potential ORDER BY temp_service.sequence)
                           FROM getListRiskPotentialLevelCategory_8 temp_service
                       ),
                       'value',
                       (SELECT json_agg(json_build_object(
                                                'name', temp_stage.name,
                                                'data', temp_data.opportunity_number
                                            ) ORDER BY temp_stage.sequence)
                        FROM getListStage_8 temp_stage
                                 LEFT JOIN getListRiskPotentialData_8 temp_data ON temp_data.id = temp_stage.id));

END
$$ LANGUAGE PLPGSQL;