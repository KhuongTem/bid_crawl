-- hungvdh - [Dashboard chung] Thống kê theo sản phẩm dịch vụ
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_thongke_spdv(DATE);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_thongke_spdv(_to_date DATE)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
    DECLARE
        _stage_moi_id      INT;
BEGIN
    _stage_moi_id = (SELECT id FROM vnpt_crm_stage WHERE code = 'moi' LIMIT  1);

    RETURN QUERY
        WITH
             -- So sánh với thời điểm cơ hội thao tác nằm trong thời điểm < “Thời gian cuối tìm kiếm trong Filter”
             -- (Loại bỏ các cơ hội ở giai đoạn “Mới” và đếm theo quy tắc thống kê của từng tiêu chí
             temp_to_date AS (
                SELECT
                    list_opportunity_ver2.opportunity_id,
                    COALESCE(list_opportunity_ver2.estimated_budget, 0) estimated_budget,
                    ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                     ORDER BY temp_log.create_date DESC) AS order_row

                FROM temp_dashboard_vnpt_crm_list_opportunity_ver2 list_opportunity_ver2
                JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                    ON temp_log.opportunity_id = list_opportunity_ver2.opportunity_id
                WHERE temp_log.state = 'confirm'
                AND temp_log.stage_id != _stage_moi_id
                AND temp_log.create_date < _to_date
                AND temp_log.is_reject_log IS NOT TRUE
             ),

            getListServiceData_4 AS (
                SELECT service.id,
                       CASE
                           WHEN COALESCE(service.tenviettat, '') != ''
                               THEN service.tenviettat
                           ELSE service.tennoibo END   AS name,
                       SUM(1)                          AS opportunity_number,
                       SUM(temp_data.estimated_budget) / 1000000 AS estimated_budget
                FROM temp_to_date temp_data
                         JOIN vnpt_crm_list_opportunity_list_service opp_service
                              ON opp_service.opportunity_id = temp_data.opportunity_id
                         JOIN bid_sanpham_dv service ON service.id = opp_service.service_id
                WHERE temp_data.order_row = 1
                GROUP BY service.id,
                         CASE
                             WHEN COALESCE(service.tenviettat, '') != ''
                                 THEN service.tenviettat
                             ELSE service.tennoibo END
            )
        SELECT json_build_object(
                       'element', 'opportunity_service_value',
                       'icon', 'fa-line-chart',
                       'title', 'Thống kê theo sản phẩm dịch vụ',
                       'type', 'xy',
                       'other_option', json_build_object(
                               'legend', true
                           ),
                       'category', (
                           SELECT ARRAY_AGG(temp_service.name ORDER BY temp_service.id)
                           FROM getListServiceData_4 temp_service
                       ),
                       'value',
                       (SELECT ('[' ||
                                json_build_object(
                                        'name', 'Số lượng yêu cầu',
                                        'type', 'column',
                                        'dataLabels', json_build_object(
                                                'enabled', true
                                            ),
                                        'data',
                                        ARRAY_AGG(temp_service.opportunity_number
                                                  ORDER BY temp_service.id))
                                    || ', ' ||
                                json_build_object(
                                        'name', 'Tổng dự toán',
                                        'type', 'spline',
                                        'dataLabels', json_build_object(
                                                'enabled', true,
                                                'inside', true
                                            ),
                                        'yAxis', 1,
                                        'tooltip',
                                        json_build_object('valueSuffix', ' Triệu đồng'),
                                        'data',
                                        ARRAY_AGG(
                                                round(COALESCE(temp_service.estimated_budget, 0)::DECIMAL, 2)
                                                ORDER BY temp_service.id))
                           || ']')::JSON
                        FROM getListServiceData_4 temp_service));

END
$$ LANGUAGE PLPGSQL;