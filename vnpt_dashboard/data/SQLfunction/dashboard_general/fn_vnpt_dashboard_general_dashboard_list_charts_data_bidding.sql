
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_general_dashboard_list_charts_data_bidding(INT, INT, INT, VARCHAR, INT, INT, INT, VARCHAR, VARCHAR, INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_general_dashboard_list_charts_data_bidding(_user_id            INT,
                                                                                    _user_company_id    INT,
                                                                                    _company_id         INT,
                                                                                    _service_id         VARCHAR,
                                                                                    _location_id        INT,
                                                                                    _level              INT,
                                                                                    _nhom_sanpham_dv    INT,
                                                                                    _input_from_date    VARCHAR,
                                                                                    _input_to_date      VARCHAR,
                                                                                    _type               INT,
                                                                                    _is_change_level               INT,
                                                                                    _is_change_nhom_sp               INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _from_date                   DATE = to_char(date_trunc('year', CURRENT_DATE), 'yyyy-mm-dd')::DATE;
    _to_date                     DATE = CURRENT_DATE;
    _previous_month_date         DATE;
    _dong_to_million             INT  = 1000000;
    _stage_kyket_sequence        INT;
    _stage_saunghiemthu_sequence INT;
    _stage_dexuat_sequence       INT;
    _stage_dexuat_id             INT;
    _stage_lose_id               INT;
    _first_day_of_month          DATE;
BEGIN

    IF _input_from_date IS NOT NULL AND _input_from_date != '' THEN
        _from_date = to_date(_input_from_date, 'dd/mm/yyyy');
    END IF;

    IF _input_to_date IS NOT NULL AND _input_to_date != '' THEN
        _to_date = to_date(_input_to_date, 'dd/mm/yyyy');
    END IF;

    _previous_month_date = ((date_trunc('month', _to_date::DATE) - interval '1 month')::DATE);
    _first_day_of_month = to_char(_to_date, 'YYYY-mm-01')::DATE;

    -- Lấy thông tin giai đoạn cơ hội
    WITH getListStageId AS (
        SELECT jsonb_object_agg(stage.code, stage.id)       json_ids,
               jsonb_object_agg(stage.code, stage.sequence) json_sequences
        FROM vnpt_crm_stage stage
        WHERE stage.code IN ('dexuat', 'kyket', 'quyettoan', 'khongthanhcong')
    )
    SELECT COALESCE((temp_stage.json_ids -> 'dexuat')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'dexuat')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'kyket')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'quyettoan')::INT, 0),
           COALESCE((temp_stage.json_sequences -> 'khongthanhcong')::INT, 0)
    INTO _stage_dexuat_id,
        _stage_dexuat_sequence,
        _stage_kyket_sequence,
        _stage_saunghiemthu_sequence,
        _stage_lose_id
    FROM getListStageId temp_stage;

    -- Tạo temp validate
    CREATE TEMP TABLE IF NOT EXISTS temp_dashboard_vnpt_crm_list_opportunity_ver2
    (
        opportunity_id                INT,     -- [Cơ hội kinh doanh] Id
        company_id                    INT,     -- [Cơ hội kinh doanh] Đơn vị
        proof_form                    VARCHAR, -- [Cơ hội kinh doanh] Hình thức chứng minh
        risk_assessment               VARCHAR, -- [Cơ hội kinh doanh] Mức độ rủi ro
        potential_level               VARCHAR, -- [Cơ hội kinh doanh] Mức độ tiềm năng
        estimated_budget              FLOAT,   -- [Cơ hội kinh doanh] Ngân sách dự toán (VNĐ)
--         previous_stage_id             INT,     -- [Cơ hội kinh doanh] Trạng thái cũ trước khi chuyển
--         log_date                      DATE,    -- [Cơ hội kinh doanh] Ngày tạo log chuyển trạng thái
        create_date                     DATE,  -- [Cơ hội kinh doanh] Ngày tạo cơ hội
        stage_id                      INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Id
        stage_code                    VARCHAR, -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Mã
        stage_sequence                INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Thứ tự
        previous_month_log_date       DATE,    -- [Cơ hội kinh doanh] Ngày tạo log chuyển trạng thái tháng trước
        previous_month_stage_id       INT,     -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Id tháng trước
        previous_month_stage_code     VARCHAR, -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Mã tháng trước
        previous_month_stage_sequence INT      -- [Cơ hội kinh doanh] [Trạng thái hiện tại] Thứ tự tháng trước
    );
    TRUNCATE TABLE temp_dashboard_vnpt_crm_list_opportunity_ver2;

    -- Lấy dữ liệu Cơ hội kinh doanh hiện tại
    WITH
         table_service_multi AS (
             SELECT (CASE WHEN value_service_id <> '' THEN value_service_id::INT ELSE 0 END) value_service_id
              FROM UNNEST(STRING_TO_ARRAY(_service_id::text,';'::text)) AS value_service_id
         ),
         getListOpportunityUser AS (
             -- Lấy ra dữ liệu Cơ hội kinh doanh theo phân quyền
        SELECT temp_opp.opportunity_id,
               (opp.estimated_budget) AS estimated_budget,
               opp.company_id,
               opp.risk_assessment,
               opp.potential_level,
               opp.proof_form,
               opp.create_date, -- Ngày tạo
               opp.stage_id, -- Trạng thái hiện tại
--                temp_log.create_date::DATE                AS log_date,
--                temp_log.previous_stage_id,
--                temp_log.stage_id,
              ROW_NUMBER() OVER (PARTITION BY temp_opp.opportunity_id
                   ORDER BY opp.create_date DESC)   AS order_row
        FROM vnpt_crm_list_opportunity opp
                  -- KienCT tạm sửa sang hàm mới, lấy theo cả Company truyền vào
                JOIN fn_vnpt_get_list_opportunity_by_user_company(_user_id, _company_id,
                                                           null, null) temp_opp
                      ON temp_opp.opportunity_id = opp.id
--                 JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
--                       ON temp_log.opportunity_id = temp_opp.opportunity_id
                LEFT JOIN vnpt_crm_list_opportunity_list_service list_service
                                                ON list_service.opportunity_id = opp.id
                LEFT JOIN bid_sanpham_dv sanpham_dv ON sanpham_dv.id = list_service.service_id
                LEFT JOIN table_service_multi service_multi ON service_multi.value_service_id = sanpham_dv.id
                LEFT JOIN bid_nhom_sp nhom_sp ON nhom_sp.id = sanpham_dv.nhomspid
                LEFT JOIN rel_list_opportunity_city opportunity_city -- xử lý địa bàn
                                            ON opportunity_city.opportunity_id = opp.id
        WHERE
--               temp_log.create_date::DATE BETWEEN _from_date AND _to_date
--         AND temp_log.state = 'confirm'
          -- Tiêu chí tìm kiếm theo SPDV
--         AND
            (_service_id IS NULL OR _service_id = '0' OR (sanpham_dv.id IS NOT NULL AND list_service.id IS NOT NULL))
        AND (_nhom_sanpham_dv IS NULL OR _nhom_sanpham_dv = 0 OR (sanpham_dv.nhomspid = _nhom_sanpham_dv AND list_service.id IS NOT NULL))
        AND (_location_id IS NULL OR _location_id = 0 OR (opportunity_city.city_id = _location_id))
        AND (_level IS NULL OR _level = 0
                OR (
                    nhom_sp.id IS NOT NULL AND nhom_sp.level = _level
                )
            )
    ),
         -- Lấy trạng thái cơ hội tháng trước
         getListPreviousMonthOpportunity AS (
             SELECT temp_opp.opportunity_id,
                    temp_log.stage_id,
                    temp_log.create_date::DATE              AS log_date,
                    ROW_NUMBER() OVER (PARTITION BY temp_log.opportunity_id
                        ORDER BY temp_log.create_date DESC) AS order_row
             FROM getListOpportunityUser temp_opp
                      JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                          ON temp_log.opportunity_id = temp_opp.opportunity_id
             WHERE temp_opp.order_row = 1
               AND temp_log.create_date <= _first_day_of_month
               AND temp_log.state = 'confirm'
               AND temp_log.is_reject_log IS NOT TRUE
         )
    INSERT
    INTO temp_dashboard_vnpt_crm_list_opportunity_ver2
    (opportunity_id, company_id, proof_form, risk_assessment, potential_level, estimated_budget,
--      previous_stage_id, log_date,
     create_date,
     stage_id, stage_code, stage_sequence,
     previous_month_log_date, previous_month_stage_id, previous_month_stage_code, previous_month_stage_sequence)
    SELECT temp_opp.opportunity_id,
           temp_opp.company_id,
           temp_opp.proof_form,
           temp_opp.risk_assessment,
           temp_opp.potential_level,
           temp_opp.estimated_budget,
--            temp_opp.previous_stage_id,
--            temp_opp.log_date,
           temp_opp.create_date,        -- Ngày tạo Cơ hội
           stage.id                   AS stage_id,
           stage.code                 AS stage_code,
           stage.sequence             AS stage_sequence,
           temp_opp_previous.log_date AS previous_month_log_date,
           stage_previous.id          AS previous_month_stage_id,
           stage_previous.code        AS previous_month_stage_code,
           stage_previous.sequence    AS previous_month_stage_sequence
    FROM getListOpportunityUser temp_opp
             INNER JOIN vnpt_crm_stage stage ON stage.id = temp_opp.stage_id -- trạng thái hiện tại
             LEFT JOIN getListPreviousMonthOpportunity temp_opp_previous
                       ON temp_opp_previous.opportunity_id = temp_opp.opportunity_id
                           AND temp_opp_previous.order_row = 1
             LEFT JOIN vnpt_crm_stage stage_previous ON stage_previous.id = temp_opp_previous.stage_id
    WHERE temp_opp.order_row = 1;

    RETURN QUERY
        SELECT json_build_object(
                       'current_status', TRUE,
                       'current_message', 'Thành công',
                       'current_data', json_build_object(
                               'list_div',
                               ('[' ||
                                -- Giá trị cơ hội trúng thầu phát sinh theo tháng
                                json_build_object(
                                    'start_div',
                                    '<div class="col-sm-12 col-dashboard-custom">',
                                    'end_div',
                                    '</div>',
                                    'data',
                                    (
                                        -- Đã call sang bảng temp
                                        SELECT *
                                        FROM fn_vnpt_dashboard_get_opportunity_bid_win_new_by_month(
                                            _user_id,
                                            _user_company_id,
                                            0,
                                            _service_id,
                                            _input_from_date,
                                            _input_to_date,
                                            _type,
                                            _company_id,
                                            _location_id,
                                            _level,
                                            _nhom_sanpham_dv)
                                    )
                                )
                                || ', ' ||
                                -- Giá trị cơ hội trượt thầu phát sinh theo tháng
                                json_build_object(
                                    'start_div',
                                    '<div class="col-sm-12 col-dashboard-custom">',
                                    'end_div',
                                    '</div>',
                                    'data',
                                    (
                                    -- Đã call sang bảng temp
                                    SELECT *
                                    FROM fn_vnpt_dashboard_get_opportunity_bid_lose_new_by_month(
                                        _user_id,
                                        _user_company_id,
                                        0,
                                        _service_id,
                                        _input_from_date,
                                        _input_to_date,
                                        _type,
                                        _company_id,
                                        _location_id,
                                        _level,
                                        _nhom_sanpham_dv)
                                    )
                                )
                                || ', ' ||
                                -- Giá trị cơ hội mở thầu phát sinh theo tháng (bổ sung)
                                json_build_object(
                                    'start_div',
                                    '<div class="col-sm-12 col-dashboard-custom">',
                                    'end_div',
                                    '</div>',
                                    'data',
                                    (
                                        -- Đã call sang bảng temp
                                        SELECT *
                                        FROM fn_vnpt_dashboard_general_dashboard_get_opp_bidopen_value_month
                                        (
                                        _input_from_date,
                                        _input_to_date)
                                    )
                                )
                                || ', ' ||
                                -- 	Biểu đồ phân loại lý do trượt thầu (bổ sung)
                                json_build_object(
                                    'start_div',
                                    '<div class="col-sm-12 col-lg-6 col-dashboard-custom">',
                                    'end_div', '</div>',
                                    'data',
                                    (
                                    -- Đã call sang bảng temp
                                    SELECT *
                                    FROM fn_vnpt_dashboard_general_dashboard_get_opplose_cate_reason
                                    (_from_date, _to_date)
                                    )
                                )

                               ---------------
                               -- Kết thúc mảng
                               || ']'
                                )::JSON)
                            ,

                          'list_nhom_sp',
                          (
                              CASE
                                  WHEN _is_change_level = 1 THEN
                                      (
                                          SELECT ARRAY_AGG(json_build_object('id', id, 'name', tenhomsp))
                                          FROM bid_nhom_sp
                                          WHERE (_level IS NULL OR _level = 0 OR level = _level)
                                      )
                                  ELSE
                                      '{}'
                                  END
                          ),
                          'list_sanpham_dv',
                          (
                              CASE
                                  WHEN _is_change_level = 1 OR _is_change_nhom_sp = 1
                                      THEN
                                      (
                                          SELECT ARRAY_AGG(
                                                         json_build_object(
                                                                 'id',
                                                                 bid_sanpham_dv.id,
                                                                 'name',
                                                                 bid_sanpham_dv.tennoibo))
                                          FROM bid_sanpham_dv
                                                   INNER JOIN bid_nhom_sp ON bid_nhom_sp.id = bid_sanpham_dv.nhomspid
                                          WHERE (_is_change_level = 1 AND (_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level) )
                                             OR (_is_change_nhom_sp = 1 AND
                                                 ((_level IS NULL OR _level = 0 OR bid_nhom_sp.level = _level)
                                                     AND (_nhom_sanpham_dv IS NULL
                                                              OR _nhom_sanpham_dv = 0
                                                              OR bid_nhom_sp.id = _nhom_sanpham_dv)
                                                     )

                                                 )
                                      )
                                  ELSE
                                      '{}'
                                  END
                          )

                    );

    DROP TABLE temp_dashboard_vnpt_crm_list_opportunity_ver2;
END
$$ LANGUAGE PLPGSQL;

