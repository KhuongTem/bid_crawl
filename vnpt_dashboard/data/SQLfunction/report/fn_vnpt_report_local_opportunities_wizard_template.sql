-- hungvdh - Báo cáo dự án tiềm năng, cơ hội kinh doanh địa bàn
DROP FUNCTION IF EXISTS fn_vnpt_report_local_opportunities_wizard_template(INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_report_local_opportunities_wizard_template(_input_user_id INT, _input_user_company INT, _input_company_id INT)
    RETURNS TABLE
            (
                output_data json
            )
AS
$$
BEGIN

    RETURN QUERY
        WITH
            -- Danh sách dự án
            getListProject AS (
                SELECT pro.id      AS project_id,
                       pro.type_id AS project_type_id,
                       pro.stage_name
                FROM vnpt_project pro
                         JOIN fn_vnpt_get_list_project_by_user(_input_user_id, _input_company_id,
                                                               NULL, NULL) temp_pro
                              ON temp_pro.project_id = pro.id
            ),
            -- Thông tin doanh số dự án, phân kỳ vốn năm
            getCategoryInfo AS (
                SELECT cate.project_id,
                       ''                                   AS group_name,
                       string_agg(cate.category_id, '; ') AS service_name,
                       SUM(cate.capital_divergence_1)       AS capital_divergence_1,
                       SUM(cate.capital_divergence_2)       AS capital_divergence_2,
                       SUM(cate.capital_divergence_3)       AS capital_divergence_3,
                       SUM(cate.capital_divergence_4)       AS capital_divergence_4,
                       SUM(cate.capital_divergence_5)       AS capital_divergence_5,
                       SUM(cate.total_estimate)             AS total_estimate
                FROM vnpt_project_list_category cate
                         JOIN getListProject temp_pro ON temp_pro.project_id = cate.project_id
                GROUP BY cate.project_id
            ),
            -- Thông tin cơ hội kinh doanh
            getListOpportunity AS (
                SELECT opp.project_id,
                       opp.potential_level,
                       ROW_NUMBER()
                       OVER (PARTITION BY opp.project_id ORDER BY opp.date_start DESC, opp.id DESC) AS order_row
                FROM vnpt_crm_list_opportunity opp
                         JOIN getListProject temp_pro ON temp_pro.project_id = opp.project_id
            ),
            -- Thông tin hợp đồng
            getListContract AS (
                SELECT temp_pro.project_id,
                       cont.ngaybatdauhd,
                       cont.thoidiemnghiemthutongthe,
                       ROW_NUMBER()
                       OVER (PARTITION BY temp_pro.project_id ORDER BY cont.ngaybatdauhd DESC, cont.id DESC) AS order_row
                FROM getListProject temp_pro
                         JOIN bid_goithau_dacoketqua_dauthau bid ON bid.project_id = temp_pro.project_id
                         JOIN bid_hopdong_tuongtu cont ON cont.goithauid = bid.id
            ),
            -- Thông tin gói thầu
            getListBid AS (
                SELECT bid.project_id,
                       SUM(bid.giatrungthau) AS contract_value
                FROM bid_goithau_dacoketqua_dauthau bid
                         JOIN getListProject temp_pro ON temp_pro.project_id = bid.project_id
                GROUP BY bid.project_id
            ),
            -- Thông tin doanh thu IT
            getListRevenueRecognition AS (
                SELECT reve.project_id,
                       SUM(reve.value) AS value
                FROM vnpt_project_list_provisional_revenue_recognition reve
                         JOIN getListProject temp_pro ON temp_pro.project_id = reve.project_id
                         JOIN res_company com ON com.id = reve.company_id
                WHERE com.type = 'DV'
                  AND com.code LIKE 'IT%'
                GROUP BY reve.project_id
            )
        SELECT TO_JSON(pre)
        FROM (
                 SELECT 'Báo cáo danh sách dự án' AS file_name,
                        'Sheet 1'                 AS sheet_name,
                        (SELECT JSON_AGG(json_build_object(
                                'diaban', COALESCE(loc.name, ''),
                                'ten_duan', pro.name,
                                'mo_ta', '',
                                'ten_spdv', COALESCE(temp_cate.service_name, ''),
                                'trangthai_sanpham', CASE
                                                         WHEN pro.sent_bid_form = 'a' THEN 'Dự án nhóm A'
                                                         WHEN pro.sent_bid_form = 'b' THEN 'Dự án nhóm B'
                                                         WHEN pro.sent_bid_form = 'c' THEN 'Dự án nhóm C'
                                                         WHEN pro.sent_bid_form = 'all' THEN 'Không phân loại'
                                                         ELSE '' END,
                                'linh_vuc', COALESCE(temp_cate.group_name, ''),
                                'ten_khachhang', '',
                                'hinh_thuc', pro_type.tennguonvon,
                                'trangthai_trienkhai', COALESCE(pro.stage_name, ''),
                                'trangthai_phaply', COALESCE(task.name, ''),
                                'tai_lieu', COALESCE(pro_task.category_content, ''),
                                'tinhhinh_botrivon', CONCAT(
                                        fn_vnpt_format_vietnamese_dong(temp_cate.total_estimate),
                                        '/ ',
                                        fn_vnpt_format_vietnamese_dong(temp_bid.contract_value)
                                    ),
                                'gtda_tong', fn_vnpt_format_vietnamese_dong(temp_cate.total_estimate),
                                'gtda_it_thuchien', fn_vnpt_format_vietnamese_dong(temp_rene.value),
                                'gtda_ve_it', '',
                                'gtda_nam_1', fn_vnpt_format_vietnamese_dong(temp_cate.capital_divergence_1),
                                'gtda_nam_2', fn_vnpt_format_vietnamese_dong(temp_cate.capital_divergence_2),
                                'gtda_nam_3', fn_vnpt_format_vietnamese_dong(temp_cate.capital_divergence_3),
                                'gtda_nam_4', fn_vnpt_format_vietnamese_dong(temp_cate.capital_divergence_4),
                                'gtda_nam_5', fn_vnpt_format_vietnamese_dong(temp_cate.capital_divergence_5),
                                'kh_ungdung_cntt', '',
                                'thoihan_thuchien', '',
                                'thoidiem_dukien_ky', COALESCE(to_char(temp_cont.ngaybatdauhd, 'dd/mm/yyyy'), ''),
                                'thoidiem_nghiemthu',
                                COALESCE(to_char(temp_cont.thoidiemnghiemthutongthe, 'dd/mm/yyyy'), ''),
                                'mucdo_khathi', fn_vnpt_get_name_of_potential_level(temp_opp.potential_level),
                                'doithu_canhtranh', '',
                                'khokhan_vuongmac', COALESCE(pro_task.problem_content, '')
                            ))
                         FROM getListProject temp_pro
                                  INNER JOIN vnpt_project pro ON pro.id = temp_pro.project_id
                                  INNER JOIN bid_nguonvon pro_type ON pro_type.id = pro.type_id
                                  LEFT JOIN bid_tinh_thanhpho loc ON loc.id = pro.deployment_location_id
                                  LEFT JOIN vnpt_project_list_task pro_task ON pro_task.id = pro.task_id
                                  LEFT JOIN vnpt_project_type_stage_task task ON task.id = pro_task.task_id
                                  LEFT JOIN getCategoryInfo temp_cate ON temp_cate.project_id = pro.id
                                  LEFT JOIN getListOpportunity temp_opp ON temp_opp.project_id = pro.id
                             AND temp_opp.order_row = 1
                                  LEFT JOIN getListContract temp_cont ON temp_cont.project_id = pro.id
                             AND temp_cont.order_row = 1
                                  LEFT JOIN getListRevenueRecognition temp_rene ON temp_rene.project_id = pro.id
                                  LEFT JOIN getListBid temp_bid ON temp_bid.project_id = pro.id
                        )                         AS list_data
             ) pre;
END
$$ LANGUAGE PLPGSQL;