-- hungvdh - [Báo cáo] Báo cáo xếp hạng đơn vị theo dự án sản phẩm trọng điểm
DROP FUNCTION IF EXISTS fn_vnpt_report_unit_ranking_project_wizard_template(INT, INT, VARCHAR, VARCHAR);
CREATE OR REPLACE FUNCTION fn_vnpt_report_unit_ranking_project_wizard_template(_input_user_id INT, _user_company_id INT,
                                                                               _input_date_start VARCHAR,
                                                                               _input_date_end VARCHAR)
    RETURNS TABLE
            (
                output_data json
            )
AS
$$
DECLARE
    _total_service_focus INT;
    _stage_damphan_id    INT;
    _stage_trienkhai_id  INT;
    _service_view_id     INT;
    _project_view_id     INT;
    _report_date_start   DATE;
    _report_date_end     DATE;
BEGIN

    _total_service_focus = (SELECT COUNT(1) FROM bid_sanpham_dv WHERE is_focus IS TRUE);
    _report_date_start = TO_CHAR(CASE WHEN _input_date_start != '' THEN _input_date_start::DATE ELSE CURRENT_DATE END,
                                 'yyyy-01-01')::DATE;
    _report_date_end = CASE WHEN _input_date_end != '' THEN _input_date_end::DATE ELSE CURRENT_DATE END;

    -- Lấy thông tin giai đoạn cơ hội
    WITH getListStageId AS (
        SELECT jsonb_object_agg(stage.code, stage.id) json_ids
        FROM vnpt_crm_stage stage
        WHERE stage.code IN ('damphan', 'trienkhai')
    )
    SELECT COALESCE((temp_stage.json_ids -> 'damphan')::INT, 0),
           COALESCE((temp_stage.json_ids -> 'trienkhai')::INT, 0)
    INTO _stage_damphan_id,
        _stage_trienkhai_id
    FROM getListStageId temp_stage;

    -- Lấy thông tin view id
    WITH getListViewId AS (
        SELECT jsonb_object_agg(CONCAT(view_data.module, '.', view_data.name), view_data.res_id) json_ids
        FROM ir_model_data view_data
        WHERE model = 'ir.ui.view'
          AND (
                (module = 'vnpt_category' AND name = 'vnpt_products_services_tree')
                OR (module = 'vnpt_project' AND name = 'vnpt_project_tree'))
    )
    SELECT COALESCE((temp_view.json_ids -> 'vnpt_category.vnpt_products_services_tree')::INT, 0),
           COALESCE((temp_view.json_ids -> 'vnpt_project.vnpt_project_tree')::INT, 0)
    INTO _service_view_id,
        _project_view_id
    FROM getListViewId temp_view;

    DROP TABLE IF EXISTS temp_report_unit_ranking_project;
    CREATE TEMP TABLE IF NOT EXISTS temp_report_unit_ranking_project
    (
        opportunity_id   INT,   -- [Cơ hội kinh doanh] Id
        project_id       INT,   -- [Cơ hội kinh doanh] [Dự án] Id
        project_value    FLOAT, -- [Dự án] Tổng giá trị VNPT thực hiện trong hợp đồng gắn với dự án
        project_number_a INT,
        project_number_b INT,
        project_number_c INT,
        project_number_d INT,
        has_data         BOOLEAN
    );
    TRUNCATE TABLE temp_report_unit_ranking_project;

    -- Lấy dữ liệu Cơ hội kinh doanh hiện tại
    INSERT INTO temp_report_unit_ranking_project
        (opportunity_id, project_id, has_data)
    SELECT opp.id,
           opp.project_id,
           FALSE
    FROM vnpt_crm_list_opportunity opp
             JOIN fn_vnpt_get_list_opportunity_by_user(_input_user_id, _user_company_id,
                                                       NULL, NULL) temp_opp
                  ON temp_opp.opportunity_id = opp.id;

    -- Số sản phẩm trọng điểm có dự án của từng tỉnh (gọi là A)
    -- Số sản phẩm trọng điểm có dự án của từng tỉnh (gọi là B)
    WITH getListOpportunityLog AS (
        SELECT temp_opp.opportunity_id,
               temp_log.stage_id,
               opp_service.service_id,
               temp_opp.project_id,
               ROW_NUMBER() OVER (PARTITION BY temp_opp.opportunity_id ORDER BY temp_opp.opportunity_id) AS order_row
        FROM temp_report_unit_ranking_project temp_opp
                 JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                      ON temp_log.opportunity_id = temp_opp.opportunity_id
                 JOIN vnpt_crm_list_opportunity_list_service opp_service
                      ON opp_service.opportunity_id = temp_opp.opportunity_id
                 JOIN bid_sanpham_dv service ON service.id = opp_service.service_id
        WHERE temp_log.stage_id IN (_stage_damphan_id, _stage_trienkhai_id)
          AND temp_log.create_date::DATE BETWEEN _report_date_start AND _report_date_end
          AND temp_log.state = 'confirm'
          AND temp_log.is_reject_log IS NOT TRUE
          AND service.is_focus IS TRUE
    ),
         -- Số sản phẩm trọng điểm có dự án của từng tỉnh (gọi là A)
         -- Số sản phẩm trọng điểm có dự án của từng tỉnh (gọi là B)
         createServiceData AS (
             INSERT
                 INTO temp_report_unit_ranking_project
                     (opportunity_id, has_data, project_number_a, project_number_b)
                     SELECT temp_opp.opportunity_id,
                            TRUE,
                            SUM(CASE WHEN temp_opp.stage_id = _stage_damphan_id THEN 1 ELSE 0 END),
                            SUM(CASE WHEN temp_opp.stage_id = _stage_trienkhai_id THEN 1 ELSE 0 END)
                     FROM getListOpportunityLog temp_opp
                     GROUP BY temp_opp.opportunity_id
         )
         -- Số dự án có sản phẩm trọng điểm của từng tỉnh (gọi là C)
         -- Số dự án có sản phẩm trọng điểm của từng tỉnh (gọi là D)
    INSERT
    INTO temp_report_unit_ranking_project
        (opportunity_id, has_data, project_number_c, project_number_d)
    SELECT temp_opp.opportunity_id,
           TRUE,
           SUM(CASE WHEN temp_opp.stage_id = _stage_damphan_id THEN 1 ELSE 0 END),
           SUM(CASE WHEN temp_opp.stage_id = _stage_trienkhai_id THEN 1 ELSE 0 END)
    FROM getListOpportunityLog temp_opp
             JOIN vnpt_project pro ON pro.id = temp_opp.project_id
    WHERE temp_opp.order_row = 1
    GROUP BY temp_opp.opportunity_id;

    -- Lấy ra cơ hội duy nhất để tính giá trị => cập nhật vào dữ liệu
    WITH getListProject AS (
        SELECT temp_opp.opportunity_id,
               SUM(cont.giatrivnptthuchien) AS project_value
        FROM temp_report_unit_ranking_project temp_opp
                 JOIN bid_goithau_dacoketqua_dauthau bid ON bid.project_id = temp_opp.project_id
                 JOIN bid_hopdong_tuongtu cont ON cont.goithauid = bid.id
        WHERE temp_opp.project_id IS NOT NULL
          AND temp_opp.has_data IS FALSE
        GROUP BY temp_opp.opportunity_id
    )
    UPDATE temp_report_unit_ranking_project temp_up
    SET project_value = temp_pro.project_value
    FROM getListProject temp_pro
    WHERE temp_up.opportunity_id = temp_pro.opportunity_id
      AND temp_up.has_data IS TRUE;

    RETURN QUERY
        WITH getListData AS (
            SELECT city.id                         AS city_id,
                   city.name                       AS city_name,
                   SUM(temp_data.project_number_a) AS project_number_a,
                   SUM(temp_data.project_number_b) AS project_number_b,
                   SUM(temp_data.project_number_c) AS project_number_c,
                   SUM(temp_data.project_number_d) AS project_number_d,
                   CASE
                       WHEN _total_service_focus > 0 AND
                            (SUM(temp_data.project_number_a) > 0 OR SUM(temp_data.project_number_b) > 0)
                           THEN
                           ROUND(((((SUM(temp_data.project_number_a) + (2 * SUM(temp_data.project_number_b)))) /
                                   _total_service_focus::FLOAT) * 100)::DECIMAL, 2)
                       ELSE 0 END                  AS promotion_rate,
                   SUM(temp_data.project_value)    AS project_value
            FROM temp_report_unit_ranking_project temp_data
                     JOIN rel_list_opportunity_city rel_city ON rel_city.opportunity_id = temp_data.opportunity_id
                     JOIN bid_tinh_thanhpho city ON city.id = rel_city.city_id
            WHERE temp_data.has_data IS TRUE
            GROUP BY city.id, city.name
        ),
             prepareData AS (
                 SELECT temp_data.city_id,
                        temp_data.city_name,
                        temp_data.promotion_rate,
                        temp_data.project_number_a,
                        temp_data.project_number_b,
                        temp_data.project_number_c,
                        temp_data.project_number_d,
                        ROW_NUMBER()
                        OVER (ORDER BY temp_data.promotion_rate DESC, temp_data.project_value DESC, temp_data.project_number_c DESC) AS order_row
                 FROM getListData temp_data
             )
        SELECT json_build_object(
                       'action_service', json_build_object(
                        'type', 'ir.actions.act_window',
                        'name', 'Danh sách sản phẩm',
                        'view_mode', 'tree',
                        'view_type', 'list',
                        'res_model', 'bid_sanpham_dv',
                        'view_id', _service_view_id,
                        'views', CONCAT(':array[[', _service_view_id, ', ''list'']]:array'),
                        'target', 'new',
                        'context', json_build_object(
                                'filter_project_number', '1',
                                'filter_date_start', _report_date_start::VARCHAR,
                                'filter_date_end', _report_date_end::VARCHAR,
                                'create', 0
                            )
                    ),
                       'action_project', json_build_object(
                               'type', 'ir.actions.act_window',
                               'name', 'Danh sách sự án',
                               'view_mode', 'tree',
                               'view_type', 'list',
                               'res_model', 'vnpt_project',
                               'view_id', _project_view_id,
                               'views', CONCAT(':array[[', _project_view_id, ', ''list'']]:array'),
                               'target', 'new',
                               'context', json_build_object(
                                       'filter_project_number', '1',
                                       'filter_date_start', _report_date_start::VARCHAR,
                                       'filter_date_end', _report_date_end::VARCHAR,
                                       'create', 0
                                   )
                           ),
                       'list_data',
                       (SELECT JSON_AGG(json_build_object(
                                                'tinh', COALESCE(temp_data.city_name, ''),
                                                'xephang', temp_data.order_row,
                                                'tyle_xuctien', temp_data.promotion_rate,
                                                'soluong_duan_list', ARRAY ['a', 'b', 'c', 'd'],
                                                'soluong_duan_a', CASE
                                                                      WHEN temp_data.project_number_a = 0 THEN ''
                                                                      ELSE temp_data.project_number_a::VARCHAR END,
                                                'soluong_duan_b', CASE
                                                                      WHEN temp_data.project_number_b = 0 THEN ''
                                                                      ELSE temp_data.project_number_b::VARCHAR END,
                                                'soluong_duan_c', CASE
                                                                      WHEN temp_data.project_number_c = 0 THEN ''
                                                                      ELSE temp_data.project_number_c::VARCHAR END,
                                                'soluong_duan_d', CASE
                                                                      WHEN temp_data.project_number_d = 0 THEN ''
                                                                      ELSE temp_data.project_number_d::VARCHAR END,
                                                'tongso_trongdiem', CASE
                                                                        WHEN COALESCE(_total_service_focus, 0) = 0
                                                                            THEN ''
                                                                        ELSE _total_service_focus::VARCHAR END,
                                                'soluong_duan_parent_a', 'action_service',
                                                'soluong_duan_parent_b', 'action_service',
                                                'soluong_duan_parent_c', 'action_project',
                                                'soluong_duan_parent_d', 'action_project',
                                                'soluong_duan_context_a', json_build_object(
                                                        'filter_city_id', temp_data.city_id,
                                                        'filter_stage_id', _stage_damphan_id
                                                    ),
                                                'soluong_duan_context_b', json_build_object(
                                                        'filter_city_id', temp_data.city_id,
                                                        'filter_stage_id', _stage_trienkhai_id
                                                    ),
                                                'soluong_duan_context_c', json_build_object(
                                                        'filter_city_id', temp_data.city_id,
                                                        'filter_stage_id', _stage_damphan_id
                                                    ),
                                                'soluong_duan_context_d', json_build_object(
                                                        'filter_city_id', temp_data.city_id,
                                                        'filter_stage_id', _stage_trienkhai_id
                                                    )
                                            ) ORDER BY temp_data.order_row)
                        FROM prepareData temp_data)
                   );

    DROP TABLE temp_report_unit_ranking_project;

END
$$ LANGUAGE PLPGSQL;