-- hungvdh - [Tổng hợp hoạt động cơ hội trong tháng] Lấy dữ liệu filter
DROP FUNCTION IF EXISTS fn_vnpt_report_dulieu_dauthau_filter(INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_vnpt_report_dulieu_dauthau_filter(_user_id INT,
                                                                        _user_company_id INT,
                                                                        _record_id INT)
    RETURNS TABLE
            (
                order_row   BIGINT,
                output_data JSON
            )
AS
$$
DECLARE
    _filter_loainhandien        VARCHAR;
    _filter_phanloai_spdv      BOOLEAN;
    _filter_benmoithau_ids VARCHAR[];
    _filter_chudautu_ids    VARCHAR[];
BEGIN

    -- Lấy loại nhận diện
    SELECT loainhandien
    INTO _filter_loainhandien
    FROM vnpt_report_dulieu_dauthau
    WHERE id = _record_id;

    -- Lấy loại nhận diện
    SELECT phanloai_spdv
    INTO _filter_phanloai_spdv
    FROM vnpt_report_dulieu_dauthau
    WHERE id = _record_id;

    -- Lấy bên mời thầu
    SELECT ARRAY_AGG(bmt.madinhdanh)
    INTO _filter_benmoithau_ids
    FROM rel_vnpt_report_dulieu_dauthau_benmoithau rel
    join bid_muasamcong_danhsach_benmoithau bmt on rel.benmoithau_id = bmt.id
    WHERE rel.report_id = _record_id;

    -- Lấy chủ đầu tư
    SELECT ARRAY_AGG(cdt.madinhdanh)
    INTO _filter_chudautu_ids
    FROM rel_vnpt_report_dulieu_dauthau_chudautu rel
    join bid_muasamcong_danhsach_chudautu_duocpheduyet cdt on rel.chudautu_id = cdt.id
    WHERE report_id = _record_id;

    IF _filter_loainhandien = 'CHUONGV' then
        RETURN QUERY
            WITH getListGoiThau AS (
                select gt.matbmt,
                       gt.tengoithau,
                       cdt.tendaydu tenchudautu,
                       gt.linhvuc,
                       gt.giagoithau,
                       gt.hinhthucduthau,
                       gt.thoidiemdongthau,
                       gt.thoidiemmothau,
                       '',
                       ''
                 from bid_muasamcong_danhsach_goithau gt
                left join bid_muasamcong_danhsach_chudautu_duocpheduyet cdt on gt.tenchudautu = cdt.madinhdanh

            )
            SELECT ROW_NUMBER() OVER (ORDER BY gt.giagoithau) AS order_row,
                   json_build_object(
                           '11_tt', ROW_NUMBER() OVER (ORDER BY gt.giagoithau),
                           '12_so_tbmt', COALESCE(gt.matbmt, ''),
                           '13_ten_goithau', COALESCE(gt.tengoithau, ''),
                           '14_ten_chudautu', COALESCE(gt.tenchudautu, ''),
                           '15_linhvuc', COALESCE(gt.linhvuc, ''),
                           '16_giagoithau', COALESCE(gt.giagoithau, 0),
                           '17_hinhthuc_duthau', COALESCE(gt.hinhthucduthau, ''),
                           '18_thoidiem_dongthau', COALESCE(gt.thoidiemdongthau, null),
                           '19_thoidiem_mothau', COALESCE(gt.thoidiemmothau, null),
                           '20_spdv', '',
                           '21_tilenhandien', ''
                       )   AS list_data
            FROM getListGoiThau gt;
    END IF;

    IF _filter_loainhandien = 'TENGOITHAU' then
    RETURN QUERY
        WITH getListGoiThau AS (
            select gt.matbmt,
                   gt.tengoithau,
                   cdt.tendaydu tenchudautu,
                   gt.linhvuc,
                   gt.giagoithau,
                   gt.hinhthucduthau,
                   gt.thoidiemdongthau,
                   gt.thoidiemmothau,
                   STRING_AGG (sp.sanpham, ' | ') list_sp,
                   ''
            from bid_muasamcong_danhsach_goithau gt
            left join bid_muasamcong_danhsach_chudautu_duocpheduyet cdt on gt.tenchudautu = cdt.madinhdanh
            left join bid_muasamcong_danhsach_sanpham sp on gt.goithauid = sp.goithauid
            group by gt.goithauid,gt.matbmt, gt.tengoithau,
                     cdt.tendaydu, gt.linhvuc, gt.giagoithau,
                     gt.hinhthucduthau, gt.thoidiemdongthau,
                     gt.thoidiemmothau

        )
        SELECT ROW_NUMBER() OVER (ORDER BY gt.giagoithau) AS order_row,
               json_build_object(
                       '11_tt', ROW_NUMBER() OVER (ORDER BY gt.giagoithau),
                       '12_so_tbmt', COALESCE(gt.matbmt, ''),
                       '13_ten_goithau', COALESCE(gt.tengoithau, ''),
                       '14_ten_chudautu', COALESCE(gt.tenchudautu, ''),
                       '15_linhvuc', COALESCE(gt.linhvuc, ''),
                       '16_giagoithau', COALESCE(gt.giagoithau, 0),
                       '17_hinhthuc_duthau', COALESCE(gt.hinhthucduthau, ''),
                       '18_thoidiem_dongthau', COALESCE(gt.thoidiemdongthau, null),
                       '19_thoidiem_mothau', COALESCE(gt.thoidiemmothau, null),
                       '20_spdv', COALESCE(gt.list_sp, null),
                       '21_tilenhandien', ''
                   )   AS list_data
        FROM getListGoiThau gt;
    END IF;

    IF _filter_loainhandien = 'TENCHUDAUTU' then
    RETURN QUERY
        WITH getListGoiThau AS (
            select gt.matbmt,
                   gt.tengoithau,
                   cdt.tendaydu tenchudautu,
                   gt.linhvuc,
                   gt.giagoithau,
                   gt.hinhthucduthau,
                   gt.thoidiemdongthau,
                   gt.thoidiemmothau,
                   STRING_AGG (sp.sanpham, ' | ') list_sp,
                   ''
            from bid_muasamcong_danhsach_goithau gt
            left join bid_muasamcong_danhsach_chudautu_duocpheduyet cdt on gt.tenchudautu = cdt.madinhdanh
            left join bid_muasamcong_danhsach_sanpham sp on gt.goithauid = sp.goithauid
            WHERE (_filter_chudautu_ids IS NULL OR gt.tenchudautu = ANY (_filter_chudautu_ids))
            group by gt.goithauid,gt.matbmt, gt.tengoithau,
                     cdt.tendaydu, gt.linhvuc, gt.giagoithau,
                     gt.hinhthucduthau, gt.thoidiemdongthau,
                     gt.thoidiemmothau

        )
        SELECT ROW_NUMBER() OVER (ORDER BY gt.giagoithau) AS order_row,
               json_build_object(
                       '11_tt', ROW_NUMBER() OVER (ORDER BY gt.giagoithau),
                       '12_so_tbmt', COALESCE(gt.matbmt, ''),
                       '13_ten_goithau', COALESCE(gt.tengoithau, ''),
                       '14_ten_chudautu', COALESCE(gt.tenchudautu, ''),
                       '15_linhvuc', COALESCE(gt.linhvuc, ''),
                       '16_giagoithau', COALESCE(gt.giagoithau, 0),
                       '17_hinhthuc_duthau', COALESCE(gt.hinhthucduthau, ''),
                       '18_thoidiem_dongthau', COALESCE(gt.thoidiemdongthau, null),
                       '19_thoidiem_mothau', COALESCE(gt.thoidiemmothau, null),
                       '20_spdv', COALESCE(gt.list_sp, null),
                       '21_tilenhandien', ''
                   )   AS list_data
        FROM getListGoiThau gt;
    END IF;

--     Bên mời thầu
    IF _filter_loainhandien = 'BENMOITHAU' then
    RETURN QUERY
        WITH getListGoiThau AS (
            select gt.matbmt,
                   gt.tengoithau,
                   cdt.tendaydu tenchudautu,
                   gt.linhvuc,
                   gt.giagoithau,
                   gt.hinhthucduthau,
                   gt.thoidiemdongthau,
                   gt.thoidiemmothau,
                   STRING_AGG (sp.sanpham, ' | ') list_sp,
                   ''
            from bid_muasamcong_danhsach_goithau gt
            left join bid_muasamcong_danhsach_chudautu_duocpheduyet cdt on gt.tenchudautu = cdt.madinhdanh
            left join bid_muasamcong_danhsach_sanpham sp on gt.goithauid = sp.goithauid
            WHERE (_filter_benmoithau_ids IS NULL OR gt.mabenmoithau = ANY (_filter_benmoithau_ids))
            group by gt.goithauid,gt.matbmt, gt.tengoithau,
                     cdt.tendaydu, gt.linhvuc, gt.giagoithau,
                     gt.hinhthucduthau, gt.thoidiemdongthau,
                     gt.thoidiemmothau

        )
        SELECT ROW_NUMBER() OVER (ORDER BY gt.giagoithau) AS order_row,
               json_build_object(
                       '11_tt', ROW_NUMBER() OVER (ORDER BY gt.giagoithau),
                       '12_so_tbmt', COALESCE(gt.matbmt, ''),
                       '13_ten_goithau', COALESCE(gt.tengoithau, ''),
                       '14_ten_chudautu', COALESCE(gt.tenchudautu, ''),
                       '15_linhvuc', COALESCE(gt.linhvuc, ''),
                       '16_giagoithau', COALESCE(gt.giagoithau, 0),
                       '17_hinhthuc_duthau', COALESCE(gt.hinhthucduthau, ''),
                       '18_thoidiem_dongthau', COALESCE(gt.thoidiemdongthau, null),
                       '19_thoidiem_mothau', COALESCE(gt.thoidiemmothau, null),
                       '20_spdv', COALESCE(gt.list_sp, null),
                       '21_tilenhandien', ''
                   )   AS list_data
        FROM getListGoiThau gt;
    END IF;

END
$$ LANGUAGE PLPGSQL;