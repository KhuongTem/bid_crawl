-- hungvdh - [Tổng hợp hoạt động cơ hội trong tháng] Lấy dữ liệu data
DROP FUNCTION IF EXISTS fn_widget_vnpt_report_dulieu_dauthau_data(INT, INT, INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_widget_vnpt_report_dulieu_dauthau_data(_user_id INT, _user_company_id INT,
                                                                             _record_id INT, _sheet_id INT,
                                                                             _page_index INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
DECLARE
    _page_size   INT = 40;
    _page_number INT = (_page_index - 1) * _page_size;
BEGIN

    RETURN QUERY
        WITH getListOpportunityUser AS (
            SELECT * FROM fn_vnpt_report_dulieu_dauthau_filter(_user_id, _user_company_id, _record_id)
        ),
             prepareListVariant AS (
                 SELECT JSON_AGG(temp_opp.output_data ORDER BY temp_opp.order_row) AS list_data
                 FROM getListOpportunityUser temp_opp
                 WHERE temp_opp.order_row BETWEEN ((_page_index - 1) * _page_size + 1)
                           AND _page_index * _page_size
             ),
             prepareListTotal AS (
                 SELECT 'sheet_1' AS sheet_code,
                        COUNT(1)  AS total_record
                 FROM getListOpportunityUser temp_data
             )
        SELECT json_build_object(
                       'list_data', (
                    SELECT json_build_object(
                                   'sheet_1', (SELECT * FROM prepareListVariant)
                               )
                ),
                       'list_total', (
                           SELECT jsonb_object_agg(
                                          temp_data.sheet_code, json_build_object(
                                           'total_record', temp_data.total_record,
                                           'from_record', (_page_number + 1),
                                           'to_record', CASE
                                                            WHEN (_page_number + _page_size) > temp_data.total_record
                                                                THEN temp_data.total_record
                                                            ELSE _page_number + _page_size END,
                                           'total_page', CEIL(temp_data.total_record / _page_size::float)
                                       )
                                      )
                           FROM prepareListTotal temp_data
                       )
                   );
END
$$ LANGUAGE PLPGSQL;
