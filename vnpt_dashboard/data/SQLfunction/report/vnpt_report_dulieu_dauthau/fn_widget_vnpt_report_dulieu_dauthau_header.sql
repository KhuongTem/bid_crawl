-- hungvdh - [Tổng hợp hoạt động cơ hội trong tháng] Lấy dữ liệu header
DROP FUNCTION IF EXISTS fn_widget_vnpt_report_dulieu_dauthau_header(INT, INT, INT);
CREATE OR REPLACE FUNCTION fn_widget_vnpt_report_dulieu_dauthau_header(_user_id INT, _user_company_id INT, _record_id INT)
    RETURNS TABLE
            (
                output_data JSON
            )
AS
$$
BEGIN

    RETURN QUERY
        SELECT json_build_object(
                       'list_sheet', (
                    SELECT json_agg(json_build_object(
                            'sheet_id', '1',
                            'sheet_code', '1',
                            'sheet_name', 'Kết quả',
                            'list_variant',
                            (SELECT '[
                              {"variant_code": "", "variant_name": "TT", "variant_note": "", "variant_width": 50, "variant_order": 1},
                              {"variant_code": "", "variant_name": "Số TBMT", "variant_note": "", "variant_width": 200, "variant_order": 2},
                              {"variant_code": "", "variant_name": "Tên gói thầu", "variant_note": "", "variant_width": 200, "variant_order": 3},
                              {"variant_code": "", "variant_name": "Tên chủ đầu tư", "variant_note": "", "variant_width": 130, "variant_order": 4, "class": "text-center"},
                              {"variant_code": "", "variant_name": "Lĩnh vực", "variant_note": "", "variant_width": 130, "variant_order": 5},
                              {"variant_code": "", "variant_name": "Giá gói thầu", "variant_note": "", "variant_width": 130, "variant_order": 6},
                              {"variant_code": "", "variant_name": "Hình thức dự thầu", "variant_note": "", "variant_width": 230, "variant_order": 7},
                              {"variant_code": "", "variant_name": "Thời điểm mở thầu", "variant_note": "", "variant_width": 260, "variant_order": 8},
                              {"variant_code": "", "variant_name": "Thời điểm đóng thầu", "variant_note": "", "variant_width": 260, "variant_order": 9},
                              {"variant_code": "", "variant_name": "SPDV", "variant_note": "", "variant_width": 130, "variant_order": 10},
                              {"variant_code": "", "variant_name": "Tỉ lệ nhận diện gói thầu", "variant_note": "", "variant_width": 200, "variant_order": 11}
                            ]'::JSON)
                        ))
                )
                   );
END
$$ LANGUAGE PLPGSQL;