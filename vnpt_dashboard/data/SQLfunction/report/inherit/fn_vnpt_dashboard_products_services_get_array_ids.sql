-- hungvdh - Số sản phẩm trọng điểm có dự án đang hoạt động trên hệ thống
DROP FUNCTION IF EXISTS fn_vnpt_dashboard_products_services_get_array_ids(INT, INT, INT, INT, VARCHAR, VARCHAR);
CREATE OR REPLACE FUNCTION fn_vnpt_dashboard_products_services_get_array_ids(_user_id INT, _user_company_id INT, _city_id INT,
                                                                   _stage_id INT, _report_date_start VARCHAR,
                                                                   _report_date_end VARCHAR)
    RETURNS INT[] AS
$func$
WITH getListData AS (
    SELECT opp_service.service_id
    FROM fn_vnpt_get_list_opportunity_by_user(_user_id, _user_company_id,
                                              NULL, NULL) temp_opp
             JOIN vnpt_crm_list_opportunity_list_log_stage temp_log
                  ON temp_log.opportunity_id = temp_opp.opportunity_id
             JOIN vnpt_crm_list_opportunity_list_service opp_service
                  ON opp_service.opportunity_id = temp_opp.opportunity_id
             JOIN bid_sanpham_dv service ON service.id = opp_service.service_id
             JOIN rel_list_opportunity_city rel_city ON rel_city.opportunity_id = temp_opp.opportunity_id
    WHERE rel_city.city_id = _city_id
      AND temp_log.stage_id = _stage_id
      AND temp_log.create_date::DATE BETWEEN _report_date_start::DATE AND _report_date_end::DATE
      AND temp_log.state = 'confirm'
      AND temp_log.is_reject_log IS NOT TRUE
      AND service.is_focus IS TRUE
    GROUP BY opp_service.service_id
)
SELECT array_agg(temp_data.service_id)
FROM getListData temp_data;
$func$
    LANGUAGE sql;