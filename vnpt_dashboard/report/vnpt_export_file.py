# -*- coding:utf-8 -*-
from odoo import fields, models
import logging

_logger = logging.getLogger(__name__)


class VNPTExportFile(models.TransientModel):
    _name = 'vnpt_export_file'
    _description = u'Danh sách tập tin'
    _order = 'id ASC'

    code_file = fields.Char(u'Mã file', required=False)
    download_file = fields.Binary(u'Tập tin tải xuống', required=False, filters="*.xls,*.xlsx")
    download_filename = fields.Char(u'Tên tập tin tải xuống', required=False)
