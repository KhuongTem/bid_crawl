# -*- coding: utf-8 -*-
__author__ = 'VNPT-IT'
import base64
from pdfminer.high_level import extract_text
from odoo.exceptions import ValidationError
from datetime import date, datetime, timedelta
import os


def get_current_sequence(self, input_sequence, input_code=''):
    identity = self.env['ir.sequence'].next_by_code('.' + input_sequence) or '/'
    if identity == '/':
        self._cr.execute("""SELECT fn_vnpt_security_create_sequence(%s, %s, %s)""",
                         (self.env.user.id, input_sequence, input_code,))
        identity = self.env['ir.sequence'].next_by_code('.' + input_sequence) or '/'

    return identity


def get_current_sequence_company(self, input_sequence):
    identity = self.env['ir.sequence'].next_by_code(self.env.user.company_id.code + '.' + input_sequence) or '/'
    if identity == '/':
        self._cr.execute("""SELECT fn_vnpt_security_create_sequence_company(%s, %s)""",
                         (self.env.user.id, input_sequence,))
        identity = self.env['ir.sequence'].next_by_code(self.env.user.company_id.code + '.' + input_sequence) or '/'

    return identity


def get_current_sequence_code(self, input_sequence, input_code):
    identity = self.env['ir.sequence'].next_by_code('AUTO.' + input_sequence) or ''
    if identity == '':
        self._cr.execute("""SELECT fn_vnpt_security_create_sequence_code(%s, %s, %s)""",
                         (self.env.user.id, input_sequence, input_code,))
        identity = self.env['ir.sequence'].next_by_code('AUTO.' + input_sequence) or ''

    return identity


def check_user_has_group(self, list_group_code):
    self._cr.execute("""SELECT fn_vnpt_security_check_user_has_group(%s, %s)""",
                     (self.env.user.id, list_group_code,))
    return self._cr.fetchone()[0]


def get_file_extension(file_name):
    ext = ''
    if file_name and file_name != '':
        tmp = file_name.split('.')
        if len(tmp) > 1:
            ext = tmp[len(tmp) - 1]
    return ext


def check_file_extension(self, field_name, file_name, list_extension):
    if file_name and file_name != '':
        tmp = file_name.split('.')
        if len(tmp) > 1:
            ext = tmp[len(tmp) - 1]
            if ext.lower() not in list_extension:
                raise ValidationError(u'"%s" phải là định dạng "%s". Vui lòng xem lại!' %
                                      (self._fields[field_name].string,
                                       str(list_extension).replace('[', '').replace(']', ''),))


# Check file pdf hợp lệ hay không
def is_pdf(filename):
    try:
        dir_name = os.path.dirname(__file__)
        # uuidOne = str(uuid.uuid1())
        # path_file =  dir_name+'/'+uuidOne+".pdf"
        #
        # my_canvas = canvas.Canvas(path_file)
        # my_canvas.showPage()
        # my_canvas.save()

        pdf_file = open(dir_name + '/' + "input" + ".pdf", "wb")
        pdf_file.write((base64.b64decode(filename)))
        # extract_text('/'+uuidOne+".pdf")
        extract_text(dir_name + '/' + "input" + ".pdf")
        pdf_file.close()
        
        # if os.path.exists(path_file):
        #     os.remove(path_file)
        return True
    except Exception as e:
        # if os.path.exists(path_file):
        #     os.remove(path_file)
        # raise ValidationError('%s' % (e))
        # raise ValidationError(u'File pdf không hợp lệ! ' + filename)
        return False


def create_tracking(self, list_message):
    custom_message = '<ul class="o_Message_trackingValues">'
    for msg in list_message:
        if len(msg) == 3:
            custom_message += """<li>
                        <div class="o_Message_trackingValue">
                            <div class="o_Message_trackingValueFieldName o_Message_trackingValueItem">%s:</div>
                            <div class="o_Message_trackingValueOldValue o_Message_trackingValueItem">%s</div>
                            <div title="Đã thay đổi" role="img" class="o_Message_trackingValueSeparator o_Message_trackingValueItem fa fa-long-arrow-right"></div>
                            <div class="o_Message_trackingValueNewValue o_Message_trackingValueItem">%s</div>
                        </div>
                    </li>""" % (msg[0], msg[1], msg[2],)
        else:
            custom_message += """<li>
                <div class="o_Message_trackingValue">
                    <div class="o_Message_trackingValueFieldName o_Message_trackingValueItem">%s:</div>
                    <div class="o_Message_trackingValueNewValue o_Message_trackingValueItem">%s</div>
                </div>
            </li>""" % (msg[0], msg[1],)

    custom_message += '</ul>'

    self.message_post(body=custom_message)


def get_month():
    return str(datetime.now().month)


def get_year():
    return str(datetime.now().year)


def validate_date_without_day(date_text):
    try:
        new_date = datetime.strptime(date_text, '%m/%Y')
        return new_date.strftime('%m/%Y')
    except ValueError:
        return False


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)
    return (next_month - timedelta(days=next_month.day)).date()


def create_notify_return(i_success='success', i_title='Thông báo', i_message='Cập nhật thành công', i_next=None):
    notification = {
        'type': 'ir.actions.client',
        'tag': 'display_notification',
        'params': {
            'title': i_title,
            'message': i_message,
            'type': i_success
        },
    }
    if i_next is not None:
        notification['params']['next'] = i_next

    return notification


def format_data_false(data):
    return data if data else ''


def format_data_money_false(data):
    return format_if_money(data.replace(',', '')) if data else ''


def format_if_money(data):
    try:
        return float(data)
    except Exception as e:
        return data


def format_excel_as_text(cell, is_date=False):
    cell_value = cell.value
    # Là loại integer và float
    if cell.ctype in (2, 3) and int(cell_value) == cell_value:
        cell_value = int(cell_value)
        # Nếu muốn format date
        if is_date:
            d = date.fromordinal(cell_value + 693594)
            date_value = d.isoformat()
            cell_value = datetime.strptime(date_value, '%Y-%m-%d').strftime('%d/%m/%Y')

    return str(cell_value).strip()


def check_numeric(input_string):
    is_number = True
    try:
        int(input_string)
    except Exception as e:
        is_number = False

    return is_number


def get_style_by_class(variant_class):
    row_style = ''
    if variant_class == 'text-left':
        row_style = 'style_9_left_top_border_text'
    elif variant_class == 'text-center':
        row_style = 'style_9_center_top_border_text'
    elif variant_class == 'text-right':
        row_style = 'style_9_right_top_border_number'
    elif variant_class == 'text-link':
        row_style = 'style_9_left_top_border_link'
    return row_style
