# -*- coding: utf-8 -*-
import xlsxwriter
import datetime
import base64
from odoo import fields, models, api
from odoo.exceptions import ValidationError
from io import BytesIO
# from ...vnpt_security.model.tools import xslxwriter_style
# from ...vnpt_security.model.tools import VNPTAssetFunction as cFun

from ...vnpt_dashboard.report import xslxwriter_style
from ...vnpt_dashboard.report import VNPTAssetFunction as cFun


class VNPTReportDuLieuDauThau(models.Model):
    _name = "vnpt_report_dulieu_dauthau"
    _description = "Thu thập và phân loại dữ liệu thầu từ thị trường"
    _rec_name = 'name'

    LOAINHANDIENS = [
        ('CHUONGV', u'Chương V'),
        ('TENGOITHAU', u'Tên gói thầu'),
        ('TENDUANMUASAM', u'Tên dự án mua sắm'),
        ('TENNHATHAU', u'Tên nhà thầu'),
        ('TENCHUDAUTU', u'Tên chủ đầu tư'),
        ('BENMOITHAU', u'Bên mời thầu')
    ]

    loainhandien = fields.Selection(LOAINHANDIENS, string=u'Nhận diện gói thầu Viễn thông - CNTT theo', default='CHUONGV',
                                    required=False)
    phanloai_spdv = fields.Boolean(string=u'Phân loại gói thầu theo SPDV hay không ?')
    name = fields.Char(string=u'Tên', default=u'Thu thập và phân loại dữ liệu thầu từ thị trường')
    # month = fields.Integer(string=u'Tháng', size=2, digits=(2, 0), default=fields.Date.today().month)
    # year = fields.Integer(string=u'Năm', size=4, digits=(2, 0), default=fields.Date.today().year)
    benmoithau_ids = fields.Many2many('bid_muasamcong_danhsach_benmoithau', 'rel_vnpt_report_dulieu_dauthau_benmoithau',
                                    'report_id', 'benmoithau_id', string=u'Bên mời thầu')
    chudautu_ids = fields.Many2many('bid_muasamcong_danhsach_chudautu_duocpheduyet', 'rel_vnpt_report_dulieu_dauthau_chudautu',
                                'report_id', 'chudautu_id', string=u'Chủ đầu tư')
    html_data = fields.Char('HTML data')
    show_html_data = fields.Boolean(string=u'Tìm theo thuộc tính')

    # @api.constrains('month', 'year')
    # def _check_field_year(self):
    #     filter_year = self.year or 0
    #     filter_month = self.month or 0
    #     if filter_year != 0 and (int(filter_year) < 1000 or int(filter_year) > 9999):
    #         raise ValidationError(u'Năm không đúng định dạng. Vui lòng xem lại!')
    #     if filter_month != 0 and (int(filter_month) < 0 or int(filter_month) > 12):
    #         raise ValidationError(u'Tháng không đúng định dạng. Vui lòng xem lại!')
    #     now = datetime.datetime.now() + datetime.timedelta(hours=7)
    #     filter_date = ''
    #     if filter_year > 0:
    #         filter_date += str(filter_year) + '-'
    #         filter_date += (str(filter_month) if filter_month > 0 else now.strftime('%m')) + '-'
    #         filter_date += '01'
    #
    #     if filter_date != '' and datetime.datetime.strptime(filter_date, '%Y-%m-%d') > now:
    #         raise ValidationError(u'Tháng đánh giá không được lớn hơn hiện tại. Vui lòng xem lại!')

    def act_report_show(self):
        self.show_html_data = True

    def act_report_download(self):
        self.show_html_data = False
        file_data = BytesIO()
        workbook = xlsxwriter.Workbook(file_data, {})
        worksheet = workbook.add_worksheet('Sheet1')
        worksheet.fit_to_pages(1, 1)
        worksheet.set_landscape()
        worksheet.set_paper(8)
        style_excel = xslxwriter_style.get_style(workbook)
        worksheet.set_column(0, 0, 4)
        worksheet.set_column(1, 1, 30)
        worksheet.set_column(2, 2, 30)
        worksheet.set_column(3, 3, 9)
        worksheet.set_column(4, 4, 9)
        worksheet.set_column(5, 5, 9)
        worksheet.set_column(6, 6, 20)
        worksheet.set_column(7, 7, 20)
        worksheet.set_column(8, 8, 20)
        worksheet.set_column(9, 9, 20)
        worksheet.set_column(10, 10, 20)
        worksheet.set_row(0, 20)
        worksheet.set_row(1, 20)
        worksheet.set_row(3, 20)

        # Header
        worksheet.merge_range(1, 1, 1, 13, u'THU THẬP VÀ PHÂN LOẠI DỮ LIỆU THẦU TỪ THỊ TRƯỜNG',
                              style_excel['style_13_bold_center'])
        # filter_year = self.year or 0
        # filter_month = self.month or 0
        # filter_date = ''
        # if filter_month > 0:
        #     filter_date += str(filter_month)
        # if filter_year > 0:
        #     filter_date += ('/' if filter_date != '' else '') + str(filter_year)
        # worksheet.merge_range(2, 1, 2, 13, u'Tháng đánh giá: %s' % filter_date, style_excel['style_11_center_italic'])

        # Thead
        worksheet.write(4, 0, u'TT', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 1, u'Số TBMT', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 2, u'Tên gói thầu', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 3, u'Tên chủ đầu tư', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 4, u'Lĩnh vực', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 5, u'Giá gói thầu', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 6, u'Hình thức dự thầu', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 7, u'Thời điểm đóng thầu', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 8, u'Thời điểm mở thầu', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 9, u'SPDV', style_excel['style_9_bold_center_border'])
        worksheet.write(4, 10, u'Tỉ lệ nhận diện gói thầu', style_excel['style_9_bold_center_border'])

        self._cr.execute("""SELECT * FROM fn_vnpt_report_dulieu_dauthau_filter(%s, %s, %s);""",
                         (self.env.uid, self.env.user.company_id.id, self.id,))
        sql_data = self._cr.dictfetchall()
        if sql_data and len(sql_data) > 0:
            current_row = 5
            for _item in sql_data:
                _current_data = _item['output_data']
                worksheet.write(current_row, 0, _current_data['11_tt'], style_excel['style_9_center_top_border_text'])
                worksheet.write(current_row, 1, cFun.format_data_false(_current_data['12_so_tbmt']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 2, cFun.format_data_false(_current_data['13_ten_goithau']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 3, cFun.format_data_false(_current_data['14_ten_chudautu']),
                                style_excel['style_9_center_top_border_text'])
                worksheet.write(current_row, 4, cFun.format_data_false(_current_data['15_linhvuc']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 5, cFun.format_data_false(_current_data['16_giagoithau']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 6, cFun.format_data_false(_current_data['17_hinhthuc_duthau']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 7, cFun.format_data_false(_current_data['18_thoidiem_dongthau']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 8, cFun.format_data_false(_current_data['19_thoidiem_mothau']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 9, cFun.format_data_false(_current_data['20_spdv']),
                                style_excel['style_9_left_top_border_text'])
                worksheet.write(current_row, 10, cFun.format_data_false(_current_data['21_tilenhandien']),
                                style_excel['style_9_left_top_border_text'])

                worksheet.set_row(current_row, 11)
                current_row += 1

        workbook.close()
        file_data.seek(0)
        out = base64.b64encode(file_data.getvalue())
        file_data.close()
        file_name = f'{self._description}.xlsx'
        file_temp = self.env['vnpt_export_file'].create({
            'code_file': datetime.datetime.now().strftime("%Y%m%d%H%M%S") + '_vnpt_report_dulieu_dauthau',
            'download_file': out,
            'download_filename': file_name}
        )
        file_id = file_temp.id or 0

        return {
            'type': 'ir.actions.act_url',
            'url': '/web/content/?model=vnpt_export_file&id=' + str(file_id)
                   + '&filename_field=download_filename&field=download_file&download=true&filename=' + str(file_name),
            'target': 'new',
        }

    @api.model
    def widget_get_report_header(self, record_id=None):
        record_id = int(record_id)
        current_user = self.env.user
        self._cr.execute("""SELECT * FROM fn_widget_vnpt_report_dulieu_dauthau_header(%s, %s, %s);""",
                         (current_user.id, current_user.company_id.id, record_id,))
        list_data = self._cr.dictfetchall()

        return {
            'current_error': 200,
            'current_message': 'Thành công',
            'current_data': list_data[0]['output_data']
        }

    @api.model
    def widget_get_report_data(self, record_id=None, sheet_id=None, page_index=None):
        record_id = int(record_id)
        sheet_id = int(sheet_id)
        page_index = int(page_index)
        current_user = self.env.user
        self._cr.execute("""SELECT * FROM fn_widget_vnpt_report_dulieu_dauthau_data(%s, %s, %s, %s, %s);""",
                         (current_user.id, current_user.company_id.id, record_id, sheet_id, page_index,))
        list_data = self._cr.dictfetchall()

        return {
            'current_error': 200,
            'current_message': 'Thành công',
            'current_data': list_data[0]['output_data']
        }
