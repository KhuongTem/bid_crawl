# coding=utf-8
def get_style(wb):
    style_14_bold_center = wb.add_format(
        {'bold': True, 'font_size': '14', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': False})

    style_14_bold_left = wb.add_format(
        {'bold': True, 'font_size': '14', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': False})

    style_12_bold_center = wb.add_format(
        {'bold': True, 'font_size': '12', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': False})

    style_12_bold_left = wb.add_format(
        {'bold': True, 'font_size': '12', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': False})
    style_12_bold_left_border = wb.add_format(
        {'bold': True, 'font_size': '12', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': True})

    style_12_left = wb.add_format(
        {'bold': False, 'font_size': '12', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': False})

    style_12_bold_center_border = wb.add_format(
        {'bold': True, 'font_size': '12', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_center_border = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_center_bold_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_bold_center_underline = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True, 'underline': True})

    style_11_bold_center_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_left_border = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_left = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': False})

    style_11_left_wrap_border = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_bold_left_wrap_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_middle_wrap_border = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'middle', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_left_bold = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'bottom': True, 'top': True})

    style_11_left_bold_no_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         })

    style_11_left_bold_number = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'num_format': '#,##0.00',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         })

    style_11_left_border_right = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'right': True, 'bottom': True, 'top': True,
         'text_wrap': True,
         })

    style_11_right_border_number = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'num_format': '#,##0.00', 'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_center_border_number_1digit = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'num_format': '#,##0.0',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_right_bold_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_right_border = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': True})

    style_11_left_bold_border = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': True})

    style_hidden = wb.add_format(
        {'hidden': True, 'font_size': '11', 'font_color': 'black', 'align': 'left', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': False,
         'border': False})

    style_11_right_border_number_no_digit = wb.add_format(
        {'bold': False, 'font_size': '11', 'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'num_format': '#,##0', 'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_11_right_bold_border_number_no_digit = wb.add_format(
        {'bold': True, 'font_size': '11', 'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'num_format': '#,##0', 'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_number_1digit_right_11 = wb.add_format(
        {'bold': False, 'font_name': 'Times New Roman', 'num_format': '#,##0.0', 'font_size': '11',
         'font_color': 'black', 'align': 'right', 'valign': 'vcenter',
         'text_wrap': True,
         'border': True})

    style_header_content_noborder_center_italic = wb.add_format(
        {'bold': False, 'font_name': 'Times New Roman', 'italic': True, 'font_size': '10', 'font_color': 'black',
         'align': 'center', 'valign': 'vcenter',
         'text_wrap': True,
         'border': False})

    style_20_bold_center = wb.add_format(
        {
            'bold': True,
            'font_size': '20',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_12_left_italic_bold = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True,
            'bold': True,
        }
    )

    style_12_center_italic_bold = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True,
            'bold': True,
        }
    )

    style_12_left_italic = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True
        }
    )

    style_12_center_italic_border = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True,
            'border': True
        }
    )

    style_9_bold_center_border = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_bold_center_border_bg_color = wb.add_format(
        {
            'bg_color': '#b8c4e4',
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_12_center_italic = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True
        }
    )

    style_12_right_italic = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True
        }
    )

    style_12_center_border_wrap = wb.add_format(
        {'bold': False, 'font_size': '12', 'font_color': 'black', 'align': 'center', 'valign': 'vcenter',
         'font_name': 'Times New Roman',
         'text_wrap': True,
         'border': True})

    style_9_bold_right_border = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_bold_right_border_italic = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True,
            'border': True
        }
    )

    style_9_bold_left_border = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_bold_left_border_red = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'red',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_right_border = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_left_border = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_center_border = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_14_center = wb.add_format(
        {
            'bold': False,
            'font_size': '14',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_12_bold = wb.add_format(
        {
            'bold': True,
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_12_center = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_12_center_border = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_12_right_border = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_12_right_number_no_zero_border = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '0;-0;;@'
        }
    )

    style_12_center_border_number = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '#,###'
        }
    )

    style_12_center_border_italic = wb.add_format(
        {
            'font_size': '12',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True,
            'border': True
        }
    )

    style_12_left_border = wb.add_format(
        {
            'bold': False,
            'font_size': '12',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_right_border_number = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '#,###'
        }
    )

    style_9_right_top_border_number = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '#,###'
        }
    )

    style_9_bold_center_border_rotation = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'rotation': -90,
        }
    )

    style_9_center_border_rotation = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'rotation': 90,
        }
    )

    style_9_center_border_italic = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'italic': True
        }
    )

    style_9_left_border_italic = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'italic': True
        }
    )

    style_11_center_italic = wb.add_format(
        {
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True
        }
    )

    style_11_center = wb.add_format(
        {
            'bold': False,
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_9_bold_left_top_border_diag = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'diag_type': 2
        }
    )

    style_9_bold_center_border_italic = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'italic': True
        }
    )

    style_9_bold_center = wb.add_format(
        {
            'bold': True,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True
        }
    )

    style_9_left = wb.add_format(
        {
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True
        }
    )

    style_11_left_italic = wb.add_format(
        {
            'font_size': '11',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'italic': True
        }
    )

    style_11_bold_center = wb.add_format(
        {
            'bold': True,
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True
        }
    )

    style_12_bold_right = wb.add_format(
        {
            'bold': True,
            'font_size': '12',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': False,
            'border': False
        }
    )

    style_13_center = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_13_left = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_13_bold_center = wb.add_format(
        {
            'bold': True,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_13_bold_center_border = wb.add_format(
        {
            'bold': True,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_13_bold_center_border_dot = wb.add_format(
        {
            'bold': True,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
        }
    )

    style_13_bold_left_border_dot = wb.add_format(
        {
            'bold': True,
            'font_size': '13',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
        }
    )

    style_13_center_border_dot = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
        }
    )

    style_13_left_border_dot = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
        }
    )

    style_13_right_border_dot = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
        }
    )

    style_13_right_border_dot_accounting = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False,
            'num_format': '#.##'
        }
    )

    style_13_right_border_no_top = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'top': False
        }
    )

    style_18_bold_center = wb.add_format(
        {
            'bold': True,
            'font_size': '18',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False
        }
    )

    style_11_bold_center_border_italic = wb.add_format(
        {
            'bold': True,
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'italic': True
        }
    )

    style_13_center_italic = wb.add_format(
        {
            'bold': False,
            'font_size': '13',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': False,
            'italic': True
        }
    )

    style_11_center_border_dot = wb.add_format(
        {
            'bold': False,
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False
        }
    )

    style_11_left_border_dot = wb.add_format(
        {
            'bold': False,
            'font_size': '11',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'bottom': 4,
            'top': False
        }
    )

    style_11_center_bold_border_number_no_digit = wb.add_format(
        {
            'bold': True,
            'font_size': '11',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'num_format': '#,##0',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True
        }
    )

    style_9_left_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_left_top_border_link = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'align': 'left',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'font_color': 'blue',
            'underline': True,
        }
    )

    style_9_left_top_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'left',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_center_top_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_right_top_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'top',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_right_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'right',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_center_border_text = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '@',
        }
    )

    style_9_center_border_text_percentage_1digit = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '0.0%',
        }
    )

    style_9_center_border_text_percentage_2digit = wb.add_format(
        {
            'bold': False,
            'font_size': '9',
            'font_color': 'black',
            'align': 'center',
            'valign': 'vcenter',
            'font_name': 'Times New Roman',
            'text_wrap': True,
            'border': True,
            'num_format': '0.00%',
        }
    )

    return {
        'style_18_bold_center': style_18_bold_center,
        'style_14_bold_center': style_14_bold_center,
        'style_14_bold_left': style_14_bold_left,
        'style_13_center': style_13_center,
        'style_13_bold_center': style_13_bold_center,
        'style_13_bold_center_border': style_13_bold_center_border,
        'style_13_bold_center_border_dot': style_13_bold_center_border_dot,
        'style_13_bold_left_border_dot': style_13_bold_left_border_dot,
        'style_13_center_border_dot': style_13_center_border_dot,
        'style_13_left': style_13_left,
        'style_13_left_border_dot': style_13_left_border_dot,
        'style_13_right_border_dot': style_13_right_border_dot,
        'style_13_center_italic': style_13_center_italic,
        'style_13_right_border_dot_accounting': style_13_right_border_dot_accounting,
        'style_13_right_border_no_top': style_13_right_border_no_top,
        'style_12_bold_center': style_12_bold_center,
        'style_12_bold_left': style_12_bold_left,
        'style_12_bold_right': style_12_bold_right,
        'style_12_bold_left_border': style_12_bold_left_border,
        'style_12_bold_center_border': style_12_bold_center_border,
        'style_11_center_border': style_11_center_border,
        'style_11_left_border': style_11_left_border,
        'style_11_right_border_number': style_11_right_border_number,
        'style_11_bold_center_border': style_11_bold_center_border,
        'style_11_bold_center_underline': style_11_bold_center_underline,
        'style_11_left_bold': style_11_left_bold,
        'style_11_left_border_right': style_11_left_border_right,
        'style_11_left_bold_border': style_11_left_bold_border,
        'style_11_right_border': style_11_right_border,
        'style_11_right_bold_border': style_11_right_bold_border,
        'style_11_center_border_dot': style_11_center_border_dot,
        'style_11_left_border_dot': style_11_left_border_dot,
        'style_hidden': style_hidden,
        'style_11_center_boder': style_11_center_border,
        'style_11_center_bold_border_number_no_digit': style_11_center_bold_border_number_no_digit,
        'style_11_left_wrap_border': style_11_left_wrap_border,
        'style_11_left_bold_number': style_11_left_bold_number,
        'style_11_left_bold_no_border': style_11_left_bold_no_border,
        'style_11_bold_left_wrap_border': style_11_bold_left_wrap_border,
        'style_11_middle_wrap_border': style_11_middle_wrap_border,
        'style_number_1digit_right_11': style_number_1digit_right_11,
        'style_12_left': style_12_left,
        'style_11_center_bold_border': style_11_center_bold_border,
        'style_11_center_border_number_1digit': style_11_center_border_number_1digit,
        'style_11_right_border_number_no_digit': style_11_right_border_number_no_digit,
        'style_11_right_bold_border_number_no_digit': style_11_right_bold_border_number_no_digit,
        'style_11_center_italic': style_11_center_italic,
        'style_11_center': style_11_center,
        'style_11_left_italic': style_11_left_italic,
        'style_11_bold_center': style_11_bold_center,
        'style_11_left': style_11_left,
        'style_11_bold_center_border_italic': style_11_bold_center_border_italic,
        'style_header_content_noborder_center_italic': style_header_content_noborder_center_italic,
        'style_12_center_italic': style_12_center_italic,
        'style_12_center_border': style_12_center_border,
        'style_12_center_border_wrap': style_12_center_border_wrap,
        'style_12_center_border_italic': style_12_center_border_italic,
        'style_20_bold_center': style_20_bold_center,
        'style_9_bold_center_border': style_9_bold_center_border,
        'style_9_bold_right_border': style_9_bold_right_border,
        'style_9_bold_left_border': style_9_bold_left_border,
        'style_9_bold_center_border_rotation': style_9_bold_center_border_rotation,
        'style_9_right_border': style_9_right_border,
        'style_9_left_border': style_9_left_border,
        'style_9_center_border': style_9_center_border,
        'style_9_left_border_text': style_9_left_border_text,
        'style_9_right_border_text': style_9_right_border_text,
        'style_9_center_border_text': style_9_center_border_text,
        'style_9_center_border_italic': style_9_center_border_italic,
        'style_9_bold_left_top_border_diag': style_9_bold_left_top_border_diag,
        'style_9_bold_center_border_italic': style_9_bold_center_border_italic,
        'style_9_bold_right_border_italic': style_9_bold_right_border_italic,
        'style_9_left_border_italic': style_9_left_border_italic,
        'style_9_left': style_9_left,
        'style_9_bold_center': style_9_bold_center,
        'style_9_center_border_rotation': style_9_center_border_rotation,
        'style_9_bold_left_border_red': style_9_bold_left_border_red,
        'style_9_right_border_number': style_9_right_border_number,
        'style_9_right_top_border_number': style_9_right_top_border_number,
        'style_9_center_border_text_percentage_1digit': style_9_center_border_text_percentage_1digit,
        'style_9_center_border_text_percentage_2digit': style_9_center_border_text_percentage_2digit,
        'style_9_left_top_border_link': style_9_left_top_border_link,
        'style_9_left_top_border_text': style_9_left_top_border_text,
        'style_9_center_top_border_text': style_9_center_top_border_text,
        'style_9_right_top_border_text': style_9_right_top_border_text,
        'style_14_center': style_14_center,
        'style_12_center': style_12_center,
        'style_12_center_border_number': style_12_center_border_number,
        'style_12_left_border': style_12_left_border,
        'style_12_right_border': style_12_right_border,
        'style_12_center_italic_border': style_12_center_italic_border,
        'style_12_left_italic': style_12_left_italic,
        'style_12_right_italic': style_12_right_italic,
        'style_12_left_italic_bold': style_12_left_italic_bold,
        'style_12_center_italic_bold': style_12_center_italic_bold,
        'style_12_bold': style_12_bold,
        'style_12_right_number_no_zero_border': style_12_right_number_no_zero_border,
        'style_9_bold_center_border_bg_color': style_9_bold_center_border_bg_color,
    }
