{
    'name': 'VNPT DashBoard',
    'version': '1.1.1',
    'summary': 'Customize Dashboard - Report',
    'category': 'VNPT PTPM',
    'author': 'VNPT-IT ERP',
    'description': """
    Module quản lý dashboard, báo cáo
    """,
    # 'depends': ['vnpt_bid'],
    "depends": ['mail'],
    'data': [
        'report/vnpt_report_dulieu_dauthau.xml',
        'security/ir.model.access.csv',
        'menuitem.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'vnpt_dashboard/static/src/css/vnpt_dashboard.scss',
            'vnpt_dashboard/static/src/css/vnpt_custom_report_html.scss',
            'vnpt_dashboard/static/bower_components/highcharts/js/highcharts.js',
            'vnpt_dashboard/static/bower_components/highcharts/js/funnel.js',
            'vnpt_dashboard/static/bower_components/feather/css/feather.css',
            'vnpt_dashboard/static/bower_components/select2/v4/js/select2.min.js',
            'vnpt_dashboard/static/bower_components/select2/v3/js/select2.js',
            'vnpt_dashboard/static/bower_components/select2/v4/css/select2.min.css',
            'vnpt_dashboard/static/bower_components/datepicker/css/datepicker.scss',
        ],
        'web.assets_backend_prod_only': [
            'vnpt_dashboard/static/bower_components/font-awesome/css/font-awesome.css',
            'vnpt_dashboard/static/bower_components/bootstrap_v3/css/bootstrap.min.scss',
            'vnpt_dashboard/static/bower_components/datepicker/js/bootstrap-datepicker.js',
            'vnpt_dashboard/static/src/js/*.js',
        ],
        'web.assets_qweb': [
            'vnpt_dashboard/static/src/xml/*.xml',
        ],
        'web.report_assets_common': [
            'vnpt_dashboard/static/src/js/vnpt_custom_report.js',
        ],
    },
    'installable': True,
    'qweb': [],
    'application': False
}
